#[macro_use]
extern crate log;

mod fs;

use std::{
	ffi::{
		OsStr,
		OsString,
	},
	path::{
		Path,
		PathBuf,
	},
	sync::Arc,
};

use anyhow::Context;
use arcnet::{
	cfg_loader::NodeTreeLoader,
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	interface::{
		GetFileOptions,
		StoreInterface,
	},
	metadata::Metadata,
	nodes::{
		FinishedFile,
		Ref,
		StoredID,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
};
use clap::Parser;
use is_terminal::IsTerminal;
use log::LevelFilter;
use serde::Serialize;
use thiserror::Error;
use time::OffsetDateTime;
use tokio::io::{
	AsyncReadExt,
	AsyncWriteExt,
};
use tokio_stream::StreamExt;

/// Command-line interface to ArcNet.
#[derive(Parser)]
struct Args {
	/// Log less. Can be specified multiple times.
	#[clap(short = 'q', long, parse(from_occurrences))]
	quiet: i32,
	/// Log more. Can be specified multiple times.
	#[clap(short = 'v', long, parse(from_occurrences))]
	verbose: i32,

	/// If this arg starts with "/" or "./", interprets it as a path to a config file to load locally.
	/// Otherwise its parsed as a hostname:port to connect via QUIC.
	#[clap(parse(from_os_str))]
	host_or_cfg: OsString,

	/// Subcommand to run
	#[clap(subcommand)]
	command: Command,
}

#[derive(Parser)]
enum Command {
	/// Lists metadata about a file or reference
	Meta {
		item: ID,

		/// Put file into a specific backend instead of the default one
		#[clap(short = 'p', long)]
		node_path: Option<NodePathBuf>,
	},
	/// Gets a file and writes it to stdout.
	Get {
		file: ID,

		/// Backend to get from
		#[clap(short = 'p', long)]
		node_path: Option<NodePathBuf>,

		/// Write data to stdout even if we think the data is not textual and stdout is not a terminal
		#[clap(short = 'B', long)]
		force_tty: bool,
	},
	/// Puts a file into the store
	Put {
		#[clap(parse(from_os_str))]
		file_to_store: PathBuf,

		/// Other items to link to. May be specified more than once to link multiple IDs.
		#[clap(short = 'l', long, parse(try_from_str))]
		link: Vec<ID>,

		/// Put file into a specific backend instead of the default one
		#[clap(short = 'p', long)]
		node_path: Option<NodePathBuf>,
	},
	/// Lists all files and references in the store
	List {
		#[clap(short = 'p', long)]
		node_path: Option<NodePathBuf>,
	},
	/// Updates a reference
	#[clap(alias = "update-ref")]
	UpdateReference {
		/// ID of the reference to update
		ref_id: RefID,

		/// File ID that the reference should point to
		target: FileID,

		/// If set, only update the reference if it is currently set to this ID. The check and replace is atomic.
		#[clap(short = 'e', long)]
		if_matches: Option<FileID>,

		/// Write (and read) reference to a specific backend
		#[clap(short = 'p', long)]
		node_path: Option<NodePathBuf>,
	},

	/// Puts a directory of files into the store. Prints the resulting File ID
	/// of the tree object.
	DirPut(fs::DirPut),
	DirList(fs::DirList),
}

#[tokio::main]
async fn main() {
	if let Err(err) = real_main().await {
		if let Some(ec) = err.root_cause().downcast_ref::<ExitCode>() {
			std::process::exit(ec.0);
		}

		eprintln!("{:#}", err);
		std::process::exit(1);
	}
}

async fn real_main() -> anyhow::Result<()> {
	let args = Args::parse();

	setup_logging(args.quiet, args.verbose);

	let interface = if os_str_starts_with(&args.host_or_cfg, "/")
		|| os_str_starts_with(&args.host_or_cfg, "./")
	{
		let path = Path::new(&args.host_or_cfg);
		debug!("Loading node tree from local config file {:?}", path);
		NodeTreeLoader::load_file(path).await?
	} else {
		let addr = args
			.host_or_cfg
			.to_str()
			.ok_or_else(|| anyhow::anyhow!("Provided address was not UTF-8"))?
			.parse()?;
		debug!("Loading node tree for remote {:?}", &addr);
		let node = arcnet::nodes::quic_client::QuicClientNode::connect(
			NodePathBuf::root(),
			addr,
			None,
			None,
		)
		.await?;
		StoreInterface::new(Arc::pin(node))
	};
	debug!("Node tree loaded");

	match args.command {
		Command::Meta { item, node_path } => {
			match item {
				ID::File(id) => {
					let opt = interface
						.get_file(
							&id,
							GetFileOptions::default().node_path(node_path.as_deref()),
						)
						.await
						.context("While getting file")?;
					match opt {
						Some(file) => {
							let obj = serde_json::to_string_pretty(&FileFullMeta {
								size: file.size(),
								node_path: file.node_path(),
								meta: file.metadata(),
							})
							.unwrap();
							println!("{}", obj);
						}
						None => {
							println!("null");
						}
					};
				}
				ID::Ref(re) => {
					let opt = interface
						.get_ref(&re, node_path.as_deref())
						.await
						.context("While getting reference")?;
					match opt {
						Some(Ref { target, node_path }) => {
							let obj = serde_json::to_string_pretty(&RefFullMeta {
								node_path: &*node_path,
								target: &target,
							})
							.unwrap();
							println!("{}", obj);
						}
						None => {
							println!("null")
						}
					};
				}
			}
			Ok(())
		}
		Command::Put {
			file_to_store,
			link,
			node_path,
		} => {
			let mut src = tokio::fs::File::open(&file_to_store)
				.await
				.with_context(|| format!("While opening file {:?}", file_to_store))?;

			let md = Metadata::new(link.into_iter().collect());

			let mut dest = interface
				.push_file(md, node_path.as_deref())
				.await
				.context("While setting up destination")?;
			tokio::io::copy(&mut src, &mut dest)
				.await
				.context("While copying contents")?;
			let FinishedFile { id, .. } = dest.finish().await.context("While committing file")?;

			println!("{}", id);
			Ok(())
		}
		Command::Get {
			file,
			node_path,
			force_tty,
		} => {
			let file = interface
				.get(
					&file,
					GetFileOptions::default().node_path(node_path.as_deref()),
				)
				.await?;

			let mut file = match file {
				Some(v) => v,
				None => {
					eprintln!("Not found");
					return Err(ExitCode(2).into());
				}
			};

			let mut stdout = tokio::io::stdout();
			if !force_tty && std::io::stdout().is_terminal() {
				let mut detect_buf = vec![0u8; 1024 * 4];
				let mut offset = 0;
				while offset < detect_buf.len() {
					let amnt = file
						.read(&mut detect_buf[offset..])
						.await
						.context("While reading from store")?;
					if amnt == 0 {
						detect_buf.truncate(offset);
						break;
					}
					offset += amnt;
				}

				// TODO: this validity checking can probably be improved.
				let valid = match std::str::from_utf8(&detect_buf) {
					Ok(s) => s,
					Err(e) if e.error_len().is_none() => {
						std::str::from_utf8(&detect_buf[0..e.valid_up_to()]).unwrap()
					}
					Err(_) => {
						eprintln!("File appears to be binary; not printing to terminal");
						return Err(ExitCode(2).into());
					}
				};

				if valid.contains(|ch| ch == '\0') {
					eprintln!("File appears to be binary; not printing to terminal");
					return Err(ExitCode(2).into());
				}

				stdout
					.write_all(&detect_buf)
					.await
					.context("While writing file")?;
			}

			tokio::io::copy(&mut file, &mut stdout)
				.await
				.context("While writing file")?;

			Ok(())
		}
		Command::List { node_path } => {
			let mut it = interface
				.stored_ids(node_path.as_deref())
				.await
				.context("While getting iterator")?;
			while let Some(id) = it.next().await {
				match id {
					Ok(StoredID { id, node_path }) => println!("{} @ {}", id, node_path),
					Err(e) => return Err(anyhow::anyhow!("Could not iterate: {}", e)),
				}
			}
			Ok(())
		}
		Command::UpdateReference {
			ref_id,
			target,
			if_matches,
			node_path,
		} => {
			match interface
				.set_ref(&ref_id, &target, node_path.as_deref(), if_matches.as_ref())
				.await
			{
				Ok(_) => Ok(()),
				Err(SetReferenceError::DidNotMatch { existing }) => {
					eprintln!(
						"Expected existing id mismatch.\nCurrently points to: {:?}",
						existing
					);
					std::mem::drop(interface);
					std::process::exit(2);
				}
				Err(SetReferenceError::Error(e)) => Err(e),
			}
		}
		Command::DirPut(opts) => fs::dir_put(&interface, opts).await,
		Command::DirList(opts) => fs::dir_list(&interface, opts).await,
	}
}

fn setup_logging(q: i32, v: i32) {
	let level = match v - q {
		n if n <= -1 => LevelFilter::Error,
		-1 => LevelFilter::Warn,
		0 => LevelFilter::Info,
		1 => LevelFilter::Debug,
		_ => LevelFilter::Trace,
	};

	fern::Dispatch::new()
		.format(|out, message, record| {
			out.finish(format_args!(
				"[{}][{}][{}] {}",
				OffsetDateTime::now_local()
					.ok()
					.unwrap_or_else(OffsetDateTime::now_utc)
					.format(&time::format_description::well_known::Rfc3339)
					.unwrap(),
				record.target(),
				record.level(),
				message
			))
		})
		.level(level)
		.level_for("quinn", LevelFilter::Warn)
		.chain(std::io::stderr())
		.apply()
		.unwrap();
}

#[derive(Debug, Clone, Error)]
#[error("YOU SHOULD NOT SEE THIS")]
struct ExitCode(i32);

#[derive(Serialize)]
struct FileFullMeta<'a> {
	size: u64,
	node_path: &'a NodePath,
	#[serde(flatten)]
	meta: &'a Metadata,
}

#[derive(Serialize)]
struct RefFullMeta<'a> {
	node_path: &'a NodePath,
	target: &'a FileID,
}

#[cfg(target_family = "windows")]
fn os_str_starts_with(s: &OsStr, prefix: &str) -> bool {
	use std::os::windows::ffi::OsStrExt;
	let mut s_it = s.encode_wide().map(|ch| ch as u32);
	let mut prefix_it = prefix.chars().map(|ch| ch as u32);
	loop {
		match (s_it.next(), prefix_it.next()) {
			(Some(a), Some(b)) => {
				if a != b {
					return false;
				}
			}
			(_, None) => return true,
			(None, Some(_)) => return false,
		}
	}
}
#[cfg(target_family = "unix")]
fn os_str_starts_with(s: &OsStr, prefix: &str) -> bool {
	use std::os::unix::ffi::OsStrExt;
	s.as_bytes().starts_with(prefix.as_bytes())
}
