use std::path::PathBuf;

use anyhow::Context;
use arcnet::{
	identifiers::{
		RefID,
		ID,
	},
	interface::{
		GetFileOptions,
		StoreInterface,
	},
	metadata::Metadata,
	nodes::FinishedFile,
	path::NodePathBuf,
	structures::fs::{
		FileList,
		FileListEntry,
	},
};

#[derive(clap::Parser)]
pub struct DirPut {
	/// Tree of files to push
	#[clap(parse(from_os_str))]
	tree: PathBuf,

	/// Update a reference with the resulting file ID.
	reference: Option<RefID>,

	/// Ignore the execute bit when adding files. Use `-x` to manually mark executable.
	/// Useful for non-posix filesystems like FAT. Always set on Windows.
	#[clap(short = 'X', long)]
	ignore_execute: bool,

	/// Mark specified file(s) as executable. Supports globs. Can be specified multiple times.
	#[clap(short = 'x')]
	executable: Vec<globset::Glob>,

	/// Node to put files into
	#[clap(long)]
	file_node_path: Option<NodePathBuf>,

	/// Node to put the archive index and reference into
	#[clap(long)]
	meta_node_path: Option<NodePathBuf>,
}

#[derive(clap::Parser)]
pub struct DirList {
	id: ID,
	#[clap(long)]
	meta_node_path: Option<NodePathBuf>,
}

#[derive(Debug, Clone)]
struct GlobFromStr(globset::Glob);
impl std::str::FromStr for GlobFromStr {
	type Err = String;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let glob = s.parse::<globset::Glob>().map_err(|e| e.to_string())?;
		Ok(Self(glob))
	}
}

pub async fn dir_put(interface: &StoreInterface, opts: DirPut) -> anyhow::Result<()> {
	let mut executables_glob = globset::GlobSetBuilder::new();
	opts.executable.into_iter().for_each(|g| {
		executables_glob.add(g);
	});
	let executables_glob = executables_glob
		.build()
		.map_err(|e| anyhow::anyhow!("Could not create executables glob set: {}", e))?;

	let mut entries: FileList = Default::default();
	let mut stack = vec![(opts.tree, String::new())];
	let mut match_indices = vec![];

	while let Some((path, arc_path)) = stack.pop() {
		let mut it = tokio::fs::read_dir(&path)
			.await
			.with_context(|| format!("While reading directory {:?}", &path))?;
		while let Some(entry) = it
			.next_entry()
			.await
			.with_context(|| format!("While reading directory {:?}", path))?
		{
			let entry_path = entry.path();
			let entry_filename = entry
				.file_name()
				.to_str()
				.map(String::from)
				.ok_or_else(|| anyhow::anyhow!("Filename {:?} is not UTF-8", entry_path))?;
			let entry_arc_path = if arc_path.is_empty() {
				entry_filename.into()
			} else {
				format!("{}/{}", arc_path, entry_filename)
			};
			let entry_meta = tokio::fs::metadata(&entry_path)
				.await
				.with_context(|| format!("While getting metadata for {:?}", entry_path))?;

			if entry_meta.file_type().is_file() {
				let f = tokio::fs::File::open(&entry_path)
					.await
					.with_context(|| format!("While opening file {:?}", entry_path))?;

				let executable;
				executables_glob.matches_into(&entry_path, &mut match_indices);
				if !match_indices.is_empty() {
					executable = true;
				} else if !opts.ignore_execute {
					executable = is_executable(&f)
						.await
						.with_context(|| format!("While getting metadata for  {:?}", entry_path))?;
				} else {
					executable = false;
				}

				let FinishedFile { id, .. } = interface
					.push_file_from_read(Metadata::new_leaf(), opts.file_node_path.as_deref(), f)
					.await
					.with_context(|| format!("While pushing file {:?}", entry_path))?;

				entries.insert(
					entry_arc_path.clone(),
					FileListEntry::File {
						id: id.into(),
						executable,
					},
				);
			} else if entry_meta.file_type().is_dir() {
				entries.insert(entry_arc_path.clone(), FileListEntry::Directory {});
				stack.push((entry_path, entry_arc_path));
			} else {
				anyhow::bail!(
					"Cannot store file type {:?} at path {:?}",
					entry_meta.file_type(),
					entry_path
				)
			}
		}
	}

	let FinishedFile { id, .. } = interface
		.push_msgpack(opts.meta_node_path.as_deref(), &entries)
		.await
		.context("While pushing files index")?;
	println!("{}", id);

	if let Some(refid) = opts.reference {
		interface
			.set_ref(&refid, &id, opts.meta_node_path.as_deref(), None)
			.await
			.context("While setting reference")?;
	}

	Ok(())
}

pub async fn dir_list(interface: &StoreInterface, opts: DirList) -> anyhow::Result<()> {
	let list = match interface
		.get_msgpack::<FileList, _>(
			&opts.id,
			GetFileOptions::default().node_path(opts.meta_node_path.as_deref()),
		)
		.await?
	{
		Some(v) => v,
		None => {
			eprintln!("Not found");
			return Err(super::ExitCode(2).into());
		}
	};

	for (name, e) in list {
		print!("{}: ", name);
		match e {
			FileListEntry::File { id, executable } => {
				print!("{}", id);
				if executable {
					print!(" +x");
				}
				println!();
			}
			FileListEntry::Directory {} => println!("Dir"),
		}
	}

	Ok(())
}

#[cfg(unix)]
async fn is_executable(f: &tokio::fs::File) -> anyhow::Result<bool> {
	use std::os::unix::fs::PermissionsExt as _;
	let md = f.metadata().await?;
	let mode = md.permissions().mode();
	Ok((mode & 0o111) != 0)
}

#[cfg(not(unix))]
async fn is_executable(f: &tokio::fs::File) -> anyhow::Result<bool> {
	let _ = f;
	Ok(false)
}
