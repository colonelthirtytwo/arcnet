#!/bin/sh
set -euo pipefail

# Generate key.
openssl genpkey -out ./arcnet.key -outform DER -algorithm RSA -pkeyopt rsa_keygen_bits:4096
# Generate certificate. Modify the CN if you wish.
openssl req -new -key ./arcnet.key -keyform DER -x509 -sha256 -nodes -subj "/CN=client" -outform DER -out ./arcnet.crt
