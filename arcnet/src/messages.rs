//! Common (de)serializable messages for communicating with remote servers.
//!
//! If implementing a custom server, consider using these types. If not, you probably
//! don't need this module.

use serde::{
	Deserialize,
	Serialize,
};
use thiserror::Error;

use crate::{
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::GetRange,
	path::NodePathBuf,
	utils::match_downcast::match_downcast,
};

/// Request message. Most of these have a corresponding `*Response` type.
#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
	/// Gets a file.
	GetFile {
		id: FileID,
		node_path: NodePathBuf,
		range: GetRange,
	},
	/// Push a file.
	///
	/// QUIC: request followed by data until send stream finished.
	PushFile {
		meta: Metadata,
		node_path: NodePathBuf,
	},
	/// Gets a reference.
	GetRef { id: RefID, node_path: NodePathBuf },
	/// Sets a reference.
	SetRef {
		id: RefID,
		target: FileID,
		expected_existing: Option<FileID>,
		node_path: NodePathBuf,
	},
	/// Deletes a file or reference.
	Delete { id: ID, node_path: NodePathBuf },
	/// Resolves a reference, getting both the reference and the target file.
	GetResolvedRef {
		id: RefID,
		node_path: NodePathBuf,
		range: GetRange,
	},
	/// Lists a node's stored IDs
	ListStoredIDs { node_path: NodePathBuf },
	/// List a node's subnodes
	ListSubNodes { node_path: NodePathBuf },
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GetFileResponseInner {
	pub id: FileID,
	pub node_path: NodePathBuf,
	pub metadata: Metadata,
	pub size: u64,
	pub range: std::ops::Range<u64>,
}
/// QUIC: If not `None`, response is followed by the file content.
pub type GetFileResponse = Option<GetFileResponseInner>;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PushFileResponse {
	pub id: FileID,
	pub node_path: NodePathBuf,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GetRefResponseInner {
	pub target: FileID,
	pub node_path: NodePathBuf,
}
pub type GetRefResponse = Option<GetRefResponseInner>;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SetRefResponse {
	pub target: FileID,
	pub node_path: NodePathBuf,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DeleteResponse;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum GetResolvedRefResponse {
	NoRef,
	DanglingRef {
		target: FileID,
		ref_node_path: NodePathBuf,
	},
	/// QUIC: followed by file content
	ResolvedRef {
		target: FileID,
		ref_node_path: NodePathBuf,
		file: GetFileResponseInner,
	},
}

/// QUIC: Result<(), Error>, then if OK, stream of `\n` separated list of IDs.
pub type ListStoredIDsResponse = ();
/// QUIC: Result<(), Error>, then if OK, stream of `/` separated list of subnodes.
pub type ListSubnodesResponse = ();

#[derive(Serialize, Deserialize, Debug, Clone, Error)]
pub enum Error {
	#[error("{0}")]
	Misc(String),
	#[error(transparent)]
	Unsupported(
		#[from]
		#[serde(default, skip_serializing, skip_deserializing)]
		crate::errors::Unsupported,
	),
	#[error(transparent)]
	Unauthorized(
		#[from]
		#[serde(default, skip_serializing, skip_deserializing)]
		crate::errors::Unauthorized,
	),
	#[error(transparent)]
	NoSuchNode(#[from] crate::errors::NoSuchNode),
	#[error("Reference did not match expected value")]
	SetReferenceDoesNotMatch(Option<crate::nodes::Ref>),
}
impl Error {
	pub fn into_set_reference_error(self) -> crate::errors::SetReferenceError {
		match self {
			Self::SetReferenceDoesNotMatch(existing) => {
				crate::errors::SetReferenceError::DidNotMatch { existing }
			}
			other => crate::errors::SetReferenceError::Error(other.into_anyhow()),
		}
	}

	/// Checks an [`anyhow::Error`] to see if it containes a supported error subtype and unwraps it.
	///
	/// Returns `Err` with the original error if the anyhow error does not contain a supported error.
	///
	/// Supported errors are instances of the types in [`crate::errors`] with a corresponding variant
	/// in this enum, except for  `SetReferenceDoesNotMatch` (because it's specific to one call)
	/// and `Misc` (because generic failures should not be ).
	pub fn from_anyhow(e: anyhow::Error) -> Result<Self, anyhow::Error> {
		match_downcast!(e, {
			e = crate::errors::Unsupported =>
				Ok(Self::Unsupported(e)),
			e = crate::errors::Unauthorized =>
				Ok(Self::Unauthorized(e)),
			e = crate::errors::NoSuchNode =>
				Ok(Self::NoSuchNode(e)),
			e => Err(e),
		})
	}

	/// Similar to `from_anyhow`, but takes a reference and clones the errors.
	pub fn from_anyhow_ref(e: &anyhow::Error) -> Option<Self> {
		match_downcast!(ref e, {
			e = crate::errors::Unsupported =>
				Some(Self::Unsupported(e.clone())),
			e = crate::errors::Unauthorized =>
				Some(Self::Unauthorized(e.clone())),
			e = crate::errors::NoSuchNode =>
				Some(Self::NoSuchNode(e.clone())),
			_ => None,
		})
	}

	pub fn into_anyhow(self) -> anyhow::Error {
		match self {
			Self::Misc(s) => anyhow::anyhow!(s),
			Self::Unsupported(inner) => inner.into(),
			Self::Unauthorized(inner) => inner.into(),
			Self::NoSuchNode(inner) => inner.into(),
			Self::SetReferenceDoesNotMatch { .. } => {
				anyhow::anyhow!("Reference does not match expected value")
			}
		}
	}
}
