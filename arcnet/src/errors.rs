//! Common error types.
//!
//! The arcnet library uses the [`anyhow`] crate for most of its error handling.
//! These errors are common root causes for errors.

use serde::{
	Deserialize,
	Serialize,
};
use thiserror::Error;

use crate::{
	identifiers::{
		FileID,
		RefID,
	},
	path::NodePathBuf,
};

/// Error returned from [`NodeTrait::set_ref`](crate::nodes::NodeTrait::set_ref).
#[derive(Debug, Error)]
pub enum SetReferenceError {
	/// `if_matches` did not match
	#[error("Reference did not match expected value")]
	DidNotMatch { existing: Option<crate::nodes::Ref> },
	/// Other error
	#[error(transparent)]
	Error(#[from] anyhow::Error),
}

/// Unsupported operation (ex. trying to update a read-only store)
#[derive(Debug, Clone, Copy, Error, Default, Serialize, Deserialize)]
#[error("Operation not supported")]
pub struct Unsupported;

/// Unauthorized operation (ex. trying to update a remote store as an unprivileged user)
#[derive(Debug, Clone, Copy, Error, Default, Serialize, Deserialize)]
#[error("Not authorized")]
pub struct Unauthorized;

/// Error caused by trying to explicitly access a node that does not exist.
#[derive(Error, Clone, Debug, Serialize, Deserialize)]
#[error("Attempted to use node {0:?} but that node does not exist")]
pub struct NoSuchNode(pub NodePathBuf);

/// Error caused by accessing a reference's target when the targeted file deos not exist.
///
/// Most functions consider such "dangling" references an error, since it generally should not
/// happen in usual use.
#[derive(Error, Debug, Clone, Serialize, Deserialize)]
#[error("Reference {ref_id} in node {ref_node_path:?} points to file {target_id} which is not available")]
pub struct DanglingRef {
	pub ref_id: RefID,
	pub ref_node_path: NodePathBuf,
	pub target_id: FileID,
}
