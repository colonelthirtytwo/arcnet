//! Node pathing and path manipulation.
//!
//! Leading and trailing slashes are stored in the representation but are otherwise ignored, including in the `ParialEq`, `Eq`, and `Hash` implementations.
//!
//! A path component can be any UTF-8 string that does not contain '/', except for the empty string.
//!

use std::{
	borrow::Borrow,
	hash::Hash,
};

/// Node path reference.
///
/// An unsized type, like `str` or `Path`. Pre-validated to be a valid node path.
#[derive(Debug)]
#[repr(transparent)]
pub struct NodePath(str);
impl NodePath {
	/// Constant containing the root node path "/".
	pub const ROOT: &'static NodePath = Self::raw_new("/");

	const fn raw_new<'a>(s: &'a str) -> &'a Self {
		// safety: repr transparent
		unsafe { std::mem::transmute::<&'a str, &'a NodePath>(s) }
	}

	/// Tries to parse a string as a node path.
	pub fn try_new<'a>(s: &'a str) -> Result<&'a Self, NodePathError> {
		if s == "" || s == "/" {
			return Ok(Self::raw_new(s));
		}

		let mut inner = s;
		if inner.starts_with('/') {
			inner = &inner[1..];
		}
		if inner.ends_with('/') {
			inner = &inner[..inner.len() - 1];
		}

		if inner.split('/').any(|c| c.is_empty()) {
			return Err(NodePathError::EmptyComponent);
		}

		Ok(Self::raw_new(s))
	}

	/// Iterates over each component
	pub fn components<'a>(&'a self) -> impl Iterator<Item = &'a str> {
		if &self.0 == "" || &self.0 == "/" {
			let mut fake = "".split('/');
			fake.next().unwrap();
			return fake;
		}
		let mut inner = &self.0;
		if inner.starts_with('/') {
			inner = &inner[1..];
		}
		if inner.ends_with('/') {
			inner = &inner[..inner.len() - 1];
		}
		inner.split('/')
	}

	/// Gets the string representation
	pub fn as_str(&self) -> &str {
		&self.0
	}
}
impl PartialEq<str> for NodePath {
	fn eq(&self, other: &str) -> bool {
		&self.0 == other
	}
}
impl PartialEq<NodePath> for NodePath {
	fn eq(&self, other: &NodePath) -> bool {
		self.components().eq(other.components())
	}
}
impl Eq for NodePath {}
impl Hash for NodePath {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		for comp in self.components() {
			comp.hash(state);
		}
	}
}
impl<'a> TryFrom<&'a str> for &'a NodePath {
	type Error = NodePathError;

	fn try_from(value: &'a str) -> Result<Self, Self::Error> {
		NodePath::try_new(value)
	}
}
impl<'a> From<&'a NodePath> for NodePathBuf {
	fn from(v: &'a NodePath) -> Self {
		Self(v.as_str().into())
	}
}
impl std::fmt::Display for NodePath {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(&self.0)
	}
}
impl std::borrow::ToOwned for NodePath {
	type Owned = NodePathBuf;

	fn to_owned(&self) -> Self::Owned {
		NodePathBuf::from(self)
	}
}
impl serde::Serialize for NodePath {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		self.0.serialize(serializer)
	}
}
impl<'de> serde::Deserialize<'de> for &'de NodePath {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error;
		let str = <&'de str>::deserialize(deserializer)?;
		NodePath::try_new(str).map_err(|e| D::Error::custom(e))
	}
}

/// Owned node path.
///
/// Pre-validated to be a valid node path.
#[derive(Debug, Clone)]
pub struct NodePathBuf(String);
impl NodePathBuf {
	fn as_node_path(&self) -> &NodePath {
		NodePath::raw_new(self.0.as_str())
	}

	pub fn root() -> Self {
		Self::from(NodePath::ROOT)
	}

	/// Tries to push a component.
	///
	/// Returns an error if the component name is invalid.
	pub fn try_push(&mut self, c: &str) -> Result<(), NodePathError> {
		if c == "" {
			return Err(NodePathError::EmptyComponent);
		}
		if c.contains('/') {
			return Err(NodePathError::SlashInComponent);
		}
		if !self.0.ends_with('/') {
			self.0.push('/');
		}
		self.0.push_str(c);
		Ok(())
	}

	/// Appends another node path onto this one.
	pub fn join(&mut self, other: &NodePath) {
		if !self.0.ends_with('/') {
			self.0.push('/');
		}
		for comp in other.components() {
			self.0.push_str(comp);
			self.0.push('/');
		}
	}

	pub fn joined(&self, other: &NodePath) -> NodePathBuf {
		let mut this = self.clone();
		this.join(other);
		this
	}
}
impl TryFrom<String> for NodePathBuf {
	type Error = NodePathError;

	fn try_from(value: String) -> Result<Self, Self::Error> {
		NodePath::try_new(value.as_str())?;
		Ok(Self(value))
	}
}
impl<'a> TryFrom<&'a str> for NodePathBuf {
	type Error = NodePathError;

	fn try_from(value: &'a str) -> Result<Self, Self::Error> {
		NodePath::try_new(value)?;
		Ok(Self(value.into()))
	}
}
impl std::str::FromStr for NodePathBuf {
	type Err = NodePathError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Self::try_from(s)
	}
}
impl std::ops::Deref for NodePathBuf {
	type Target = NodePath;

	fn deref<'a>(&'a self) -> &'a Self::Target {
		self.as_node_path()
	}
}
impl std::fmt::Display for NodePathBuf {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		(&**self).fmt(f)
	}
}
impl serde::Serialize for NodePathBuf {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		self.0.serialize(serializer)
	}
}
impl<'de> serde::Deserialize<'de> for NodePathBuf {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error;
		let str = String::deserialize(deserializer)?;
		Self::try_from(str).map_err(|e| D::Error::custom(e))
	}
}
impl PartialEq<NodePathBuf> for NodePathBuf {
	fn eq(&self, other: &NodePathBuf) -> bool {
		self.as_node_path() == other.as_node_path()
	}
}
impl Eq for NodePathBuf {}
impl Hash for NodePathBuf {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.as_node_path().hash(state)
	}
}
impl Borrow<NodePath> for NodePathBuf {
	fn borrow(&self) -> &NodePath {
		&**self
	}
}

/// Error parsing a node path or adding a component to an existing one.
#[derive(thiserror::Error, Debug, Clone, PartialEq, Eq)]
pub enum NodePathError {
	/// A component was empty (ex. "//").
	#[error("Component was the empty string")]
	EmptyComponent,
	/// Pushed a component containing a '/' character.
	#[error("Component contained '/'")]
	SlashInComponent,
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_paths() {
		assert_eq!(
			NodePath::try_new("")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec![] as Vec<&'static str>
		);
		assert_eq!(
			NodePath::try_new("/")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec![] as Vec<&'static str>
		);
		assert_eq!(
			NodePath::try_new("foo")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo"]
		);
		assert_eq!(
			NodePath::try_new("/foo")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo"]
		);
		assert_eq!(
			NodePath::try_new("/foo/")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo"]
		);
		assert_eq!(
			NodePath::try_new("foo/bar")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo", "bar"]
		);
		assert_eq!(
			NodePath::try_new("/foo/bar")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo", "bar"]
		);
		assert_eq!(
			NodePath::try_new("/foo/bar/")
				.unwrap()
				.components()
				.collect::<Vec<_>>(),
			vec!["foo", "bar"]
		);

		assert!(NodePath::try_new("//").is_err());
		assert!(NodePath::try_new("foo//").is_err());
		assert!(NodePath::try_new("/foo//").is_err());
		assert!(NodePath::try_new("//foo").is_err());
		assert!(NodePath::try_new("//foo/").is_err());
	}
}
