//! Connects to another arcnet server via the QUIC protocol.
//!
//! Requires the "node_quic_client" crate feature.

mod json_obj_stream;

use std::{
	net::SocketAddr,
	path::PathBuf,
	pin::Pin,
	sync::Arc,
	task::Poll,
};

use anyhow::Context;
use async_trait::async_trait;
use futures::{
	stream::BoxStream,
	StreamExt,
	TryFutureExt,
};
use serde::Deserialize;
use tokio::{
	io::{
		AsyncReadExt,
		AsyncWriteExt,
	},
	sync::RwLock,
};

use self::json_obj_stream::recv_json_stream;
use crate::{
	cfg_loader::{
		NodeLoader,
		NodeTreeLoader,
	},
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	messages,
	metadata::Metadata,
	nodes::{
		FinishedFile,
		GetRange,
		Node,
		NodeReadTrait,
		NodeReader,
		NodeTrait,
		NodeWriteTrait,
		NodeWriter,
		Ref,
		ResolvedRef,
		StoredID,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
	utils::match_downcast::match_downcast,
};

struct EndpointShared {
	// TODO: lazily instantiate only the sockets we need
	endpoint_v4: quinn::Endpoint,
	endpoint_v6: quinn::Endpoint,
}
impl EndpointShared {
	async fn new() -> anyhow::Result<Self> {
		let endpoint_v4 = quinn::Endpoint::client(std::net::SocketAddr::V4(
			std::net::SocketAddrV4::new(std::net::Ipv4Addr::UNSPECIFIED, 0),
		))
		.context("While creating QUIC IPv4 endpoint")?;
		let endpoint_v6 = quinn::Endpoint::client(std::net::SocketAddr::V6(
			std::net::SocketAddrV6::new(std::net::Ipv6Addr::UNSPECIFIED, 0, 0, 0),
		))
		.context("While creating QUIC IPv6 endpoint")?;
		Ok(EndpointShared {
			endpoint_v4,
			endpoint_v6,
		})
	}

	async fn connect(
		self: Arc<Self>,
		path: NodePathBuf,
		remote_addr: SocketAddr,
		client_certificate: Option<(rustls::Certificate, rustls::PrivateKey)>,
		expected_server_certificate: Option<rustls::Certificate>,
	) -> anyhow::Result<QuicClientNode> {
		let security = rustls::client::ClientConfig::builder()
			.with_safe_default_cipher_suites()
			.with_safe_default_kx_groups()
			.with_safe_default_protocol_versions()
			.with_context(|| format!("While setting up QUIC TLS for path {:?}", path))?
			.with_custom_certificate_verifier(Arc::new(CertVerifier {
				expected_server_cert: expected_server_certificate.map(|v| v.0),
			}));

		let security = match client_certificate {
			Some((cert, key)) => security
				.with_single_cert(vec![cert], key)
				.with_context(|| format!("While setting up client TLS key for path {:?}", path))?,
			None => security.with_no_client_auth(),
		};

		let security = Arc::new(security);

		let shared = Arc::new(RemoteShared {
			endpoints: self,
			remote_addr: remote_addr,
			security,
			connection: RwLock::new(None),
		});
		Ok(QuicClientNode {
			shared,
			full_path: path,
			relative_path: NodePathBuf::root(),
		})
	}
}

struct RemoteShared {
	endpoints: Arc<EndpointShared>,
	remote_addr: SocketAddr,
	security: Arc<rustls::client::ClientConfig>,

	connection: RwLock<Option<quinn::Connection>>,
}
impl RemoteShared {
	async fn get_connection<'a>(
		&'a self,
	) -> anyhow::Result<tokio::sync::RwLockReadGuard<'a, quinn::Connection>> {
		loop {
			let lock = self.connection.read().await;
			if lock.is_some() {
				return Ok(tokio::sync::RwLockReadGuard::map(lock, |v| {
					v.as_ref().unwrap()
				}));
			}
			std::mem::drop(lock);

			let mut lock = self.connection.write().await;
			if lock.is_some() {
				continue;
			}

			let ep = match self.remote_addr {
				SocketAddr::V4(_) => &self.endpoints.endpoint_v4,
				SocketAddr::V6(_) => &self.endpoints.endpoint_v6,
			};

			debug!("Connecting to {:?}", self.remote_addr);
			let conn = ep
				.connect_with(
					quinn::ClientConfig::new(self.security.clone()),
					self.remote_addr.clone(),
					"arcnet",
				)
				.with_context(|| format!("While connecting to {:?}", self.remote_addr))?;
			let connection = conn
				.await
				.with_context(|| format!("While connecting to {:?}", self.remote_addr))?;
			*lock = Some(connection);
		}
	}

	async fn check_clear_connection<T>(&self, e: anyhow::Error) -> Result<T, anyhow::Error> {
		if is_connection_lost(&e) {
			let mut lock = self.connection.write().await;
			debug!("Lost connection: {:#}", e);
			*lock = None;
		}
		Err(e)
	}

	async fn simple_request_response<T: serde::de::DeserializeOwned>(
		&self,
		req: &messages::Request,
	) -> Result<Result<T, messages::Error>, anyhow::Error> {
		let conn = self.get_connection().await?;

		let (mut send, mut recv) = conn
			.open_bi()
			.or_else(|e| self.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);

		write_message(&mut send, req)
			.or_else(|e| self.check_clear_connection(anyhow::anyhow!(e)))
			.await?;
		let _ = send
			.finish()
			.or_else(|e| self.check_clear_connection(anyhow::anyhow!(e)))
			.await?;

		let msg = read_message::<T, _>(&mut recv)
			.or_else(|e| self.check_clear_connection(anyhow::anyhow!(e)))
			.await?;

		Ok(msg)
	}
}

async fn read_message<T: serde::de::DeserializeOwned, R: tokio::io::AsyncRead + Unpin>(
	mut r: R,
) -> anyhow::Result<Result<T, messages::Error>> {
	let mut len_buf = [0u8; 4];
	r.read_exact(&mut len_buf)
		.await
		.context("While reading message")?;
	let mut buf = vec![0u8; u32::from_le_bytes(len_buf) as usize];
	r.read_exact(&mut buf)
		.await
		.context("While reading message")?;

	let v = rmp_serde::from_slice::<Result<T, messages::Error>>(&buf)
		.context("While deserializing message")?;
	Ok(v)
}

async fn write_message<T: serde::Serialize, W: tokio::io::AsyncWrite + Unpin>(
	mut w: W,
	msg: &T,
) -> anyhow::Result<()> {
	let msg = rmp_serde::to_vec_named(msg).context("While serializing message")?;
	let len: u32 = msg.len().try_into().context("Message too long")?;

	w.write_all(&len.to_le_bytes())
		.await
		.context("While writing message")?;
	w.write_all(&msg).await.context("While writing message")?;

	Ok(())
}

/// Node that connects to another arcnet server, using QUIC.
///
/// Supports efficient parallel requests and streaming data. Transfer is encrypted
/// with TLS, with optional server verification and client authentication.
///
/// Supports roaming - connection is delayed until the first request and the node will retry connecting
/// on the first operation after a failure.
///
/// `sub_node` will never return `None`. It will return a proxy object that forwards requests to the main
/// connection using the relative node path. If the server does not actually have the node path, all operations
/// will fail with a [`NoSuchNode`](crate::errors::NoSuchNode) error from the server.
///
pub struct QuicClientNode {
	shared: Arc<RemoteShared>,
	full_path: NodePathBuf,
	relative_path: NodePathBuf,
}
impl QuicClientNode {
	pub async fn connect(
		path: NodePathBuf,
		remote_addr: SocketAddr,
		client_certificate: Option<(rustls::Certificate, rustls::PrivateKey)>,
		expected_server_certificate: Option<rustls::Certificate>,
	) -> anyhow::Result<Self> {
		let endpoints = Arc::new(EndpointShared::new().await?);
		let node = endpoints
			.connect(
				path,
				remote_addr,
				client_certificate,
				expected_server_certificate,
			)
			.await?;

		Ok(node)
	}
}
#[async_trait]
impl NodeTrait for QuicClientNode {
	fn path(&self) -> &NodePath {
		&self.full_path
	}

	async fn sub_node(&self, name: &str) -> anyhow::Result<Option<Node>> {
		let mut full_path = self.full_path.clone();
		let mut relative_path = self.relative_path.clone();
		full_path.try_push(name)?;
		relative_path.try_push(name)?;
		Ok(Some(Arc::pin(Self {
			shared: self.shared.clone(),
			full_path,
			relative_path,
		})))
	}

	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>> {
		let this = Pin::into_inner(self);
		let conn = this.shared.get_connection().await?;

		let (mut send, mut recv) = conn
			.open_bi()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);
		write_message(
			&mut send,
			&messages::Request::GetFile {
				id: id.clone(),
				node_path: this.relative_path.clone(),
				range: range.clone(),
			},
		)
		.or_else(|e| this.shared.check_clear_connection(e))
		.await?;
		let _ = send
			.finish()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await?;
		let resp = read_message::<messages::GetFileResponse, _>(&mut recv)
			.or_else(|e| this.shared.check_clear_connection(e))
			.await?;
		match resp {
			Ok(Some(messages::GetFileResponseInner {
				id,
				node_path,
				metadata,
				size,
				range,
			})) => Ok(Some(Box::pin(QuicReader {
				node_path: this.full_path.joined(&node_path),
				id: id.clone(),
				stream: recv,
				metadata,
				size,
				range,
			}))),
			Ok(None) => Ok(None),
			Err(e) => Err(e.into_anyhow()),
		}
	}

	async fn push_file(self: Pin<Arc<Self>>, meta: Metadata) -> anyhow::Result<NodeWriter> {
		let conn = self.shared.get_connection().await?;

		let (mut send, recv) = conn
			.open_bi()
			.or_else(|e| self.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);
		write_message(
			&mut send,
			&messages::Request::PushFile {
				meta,
				node_path: self.relative_path.clone(),
			},
		)
		.or_else(|e| self.shared.check_clear_connection(e))
		.await?;

		Ok(Box::pin(QuicWriter {
			parent: self,
			send_stream: send,
			recv_stream: recv,
			finished: false,
		}))
	}

	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>> {
		let res = self
			.shared
			.simple_request_response::<messages::GetRefResponse>(&messages::Request::GetRef {
				id: id.clone(),
				node_path: self.relative_path.clone(),
			})
			.await?
			.map_err(|e| e.into_anyhow())?;

		Ok(res.map(|res| Ref {
			target: res.target,
			node_path: self.full_path.joined(&res.node_path),
		}))
	}

	async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		expected_existing: Option<&FileID>,
	) -> Result<Ref, SetReferenceError> {
		let res = self
			.shared
			.simple_request_response::<messages::SetRefResponse>(&messages::Request::SetRef {
				id: id.clone(),
				target: target.clone(),
				expected_existing: expected_existing.cloned(),
				node_path: self.relative_path.clone(),
			})
			.await?
			.map_err(|e| e.into_set_reference_error())?;

		Ok(Ref {
			target: res.target,
			node_path: self.full_path.joined(&res.node_path),
		})
	}

	async fn delete(&self, id: &ID) -> anyhow::Result<()> {
		let messages::DeleteResponse = self
			.shared
			.simple_request_response::<messages::DeleteResponse>(&messages::Request::Delete {
				id: id.clone(),
				node_path: self.relative_path.clone(),
			})
			.await?
			.map_err(|e| e.into_anyhow())?;
		Ok(())
	}

	async fn get_resolved_ref(
		self: Pin<Arc<Self>>,
		id: &RefID,
		range: &GetRange,
	) -> anyhow::Result<ResolvedRef> {
		let this = Pin::into_inner(self);
		let conn = this.shared.get_connection().await?;

		let (mut send, mut recv) = conn
			.open_bi()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);
		write_message(
			&mut send,
			&messages::Request::GetResolvedRef {
				id: id.clone(),
				node_path: this.relative_path.clone(),
				range: range.clone(),
			},
		)
		.or_else(|e| this.shared.check_clear_connection(e))
		.await?;
		let _ = send
			.finish()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await?;
		let resp = read_message::<messages::GetResolvedRefResponse, _>(&mut recv)
			.or_else(|e| this.shared.check_clear_connection(e))
			.await?;
		match resp {
			Ok(messages::GetResolvedRefResponse::ResolvedRef {
				target,
				ref_node_path,
				file:
					messages::GetFileResponseInner {
						id,
						node_path,
						metadata,
						size,
						range,
					},
			}) => {
				let f = Box::pin(QuicReader {
					node_path: this.full_path.joined(&node_path),
					id,
					stream: recv,
					metadata,
					size,
					range,
				});
				Ok(ResolvedRef::ResolvedRef {
					ref_: Ref {
						target,
						node_path: this.full_path.joined(&ref_node_path),
					},
					file: f,
				})
			}
			Ok(messages::GetResolvedRefResponse::DanglingRef {
				target,
				ref_node_path,
			}) => Ok(ResolvedRef::DanglingRef {
				ref_id: id.clone(),
				ref_: Ref {
					target,
					node_path: this.full_path.joined(&ref_node_path),
				},
			}),
			Ok(messages::GetResolvedRefResponse::NoRef) => Ok(ResolvedRef::NoRef),
			Err(e) => Err(e.into_anyhow()),
		}
	}

	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		let this = Pin::into_inner(self);
		let conn = this.shared.get_connection().await?;

		let (mut send, mut recv) = conn
			.open_bi()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);
		write_message(
			&mut send,
			&messages::Request::ListStoredIDs {
				node_path: this.relative_path.clone(),
			},
		)
		.or_else(|e| this.shared.check_clear_connection(e))
		.await?;
		let _ = send
			.finish()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await?;
		read_message::<(), _>(&mut recv)
			.or_else(|e| this.shared.check_clear_connection(e))
			.await?
			.map_err(|e| e.into_anyhow())?;

		let path = this.full_path.clone();
		let strm = recv_json_stream::<StoredID>(recv).map(move |res| {
			res.map(|mut id| {
				id.node_path = path.joined(&id.node_path);
				id
			})
		});
		Ok(Box::pin(strm))
	}

	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		let this = Pin::into_inner(self);
		let conn = this.shared.get_connection().await?;

		let (mut send, mut recv) = conn
			.open_bi()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await
			.context("While opening stream")?;
		std::mem::drop(conn);
		write_message(
			&mut send,
			&messages::Request::ListSubNodes {
				node_path: this.relative_path.clone(),
			},
		)
		.or_else(|e| this.shared.check_clear_connection(e))
		.await?;
		let _ = send
			.finish()
			.or_else(|e| this.shared.check_clear_connection(anyhow::anyhow!(e)))
			.await?;
		read_message::<(), _>(&mut recv)
			.or_else(|e| this.shared.check_clear_connection(e))
			.await?
			.map_err(|e| e.into_anyhow())?;

		let strm = recv_json_stream::<String>(recv);
		Ok(Box::pin(strm))
	}
}

struct QuicReader {
	node_path: NodePathBuf,
	id: FileID,
	stream: quinn::RecvStream,
	metadata: Metadata,
	size: u64,
	range: std::ops::Range<u64>,
}
impl NodeReadTrait for QuicReader {
	fn id(&self) -> &FileID {
		&self.id
	}

	fn metadata(&self) -> &Metadata {
		&self.metadata
	}

	fn size(&self) -> u64 {
		self.size
	}

	fn node_path(&self) -> &NodePath {
		&self.node_path
	}

	fn range(&self) -> std::ops::Range<u64> {
		self.range.clone()
	}
}
impl tokio::io::AsyncRead for QuicReader {
	fn poll_read(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		buf: &mut tokio::io::ReadBuf<'_>,
	) -> std::task::Poll<std::io::Result<()>> {
		// TODO: check for connection lost here, somehow
		Pin::new(&mut self.stream).poll_read(cx, buf)
	}
}

struct QuicWriter {
	parent: Pin<Arc<QuicClientNode>>,
	send_stream: quinn::SendStream,
	recv_stream: quinn::RecvStream,
	finished: bool,
}
#[async_trait]
impl NodeWriteTrait for QuicWriter {
	async fn finish(self: Pin<Box<Self>>) -> anyhow::Result<FinishedFile> {
		let mut this = Pin::into_inner(self);
		this.send_stream
			.finish()
			.or_else(|e| {
				this.parent
					.shared
					.check_clear_connection(anyhow::anyhow!(e))
			})
			.await
			.context("While finishing stream")?;
		let msg = read_message::<messages::PushFileResponse, _>(&mut this.recv_stream)
			.or_else(|e| {
				this.parent
					.shared
					.check_clear_connection(anyhow::anyhow!(e))
			})
			.await?
			.map_err(|e| e.into_anyhow())?;
		let _ = this.recv_stream.stop(0u32.into());
		this.finished = true;
		Ok(FinishedFile {
			id: msg.id,
			node_path: this.parent.full_path.joined(&msg.node_path),
		})
	}
}
impl tokio::io::AsyncWrite for QuicWriter {
	fn poll_write(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		buf: &[u8],
	) -> std::task::Poll<Result<usize, std::io::Error>> {
		Pin::new(&mut self.send_stream).poll_write(cx, buf)
	}

	fn poll_flush(
		self: Pin<&mut Self>,
		_: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), std::io::Error>> {
		// no point in this
		Poll::Ready(Ok(()))
	}

	fn poll_shutdown(
		self: Pin<&mut Self>,
		_: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), std::io::Error>> {
		// finish does the real work
		Poll::Ready(Ok(()))
	}
}
impl std::ops::Drop for QuicWriter {
	fn drop(&mut self) {
		if !self.finished {
			let _ = self.send_stream.reset(1u32.into());
			let _ = self.recv_stream.stop(1u32.into());
		}
	}
}

fn is_connection_lost(err: &anyhow::Error) -> bool {
	match_downcast!(ref err.root_cause(), {
		_ = quinn::ConnectionError => true,
		e = quinn::ReadError => matches!(e, quinn::ReadError::ConnectionLost(_)),
		e = quinn::WriteError => matches!(e, quinn::WriteError::ConnectionLost(_)),
		e = quinn::ReadExactError => matches!(e, quinn::ReadExactError::ReadError(quinn::ReadError::ConnectionLost(_))),
		e = quinn::ReadToEndError => matches!(e, quinn::ReadToEndError::Read(quinn::ReadError::ConnectionLost(_))),
		e = quinn::StoppedError => matches!(e, quinn::StoppedError::ConnectionLost(_)),
		e = quinn::SendDatagramError => matches!(e, quinn::SendDatagramError::ConnectionLost(_)),
		_ => false
	})
}

#[derive(Deserialize, Debug, Clone)]
pub struct QuicClientNodeCfg {
	/// Remote address to connect to.
	#[serde(deserialize_with = "QuicClientNodeCfg::deser_addr")]
	pub address: std::net::SocketAddr,

	/// Client certificate to authenticate with, in DER Format.
	///
	/// If provided, [`client_private_key`](QuicClientNodeCfg::client_private_key) must be
	/// provided as well. If omitted, the client will connect anonymously.
	///
	/// In a config file, this is a path to a file. The file will be read as part of
	/// deserialization.
	#[serde(deserialize_with = "QuicClientNodeCfg::deser_file")]
	pub client_certificate: Option<Vec<u8>>,
	/// Client private key to authenticate with, in DER format.
	///
	/// If provided, [`client_certificate`](QuicClientNodeCfg::client_certificate) must be
	/// provided as well. If omitted, the client will connect anonymously.
	///
	/// In a config file, this is a path to a file. The file will be read as part of
	/// deserialization.
	#[serde(deserialize_with = "QuicClientNodeCfg::deser_file")]
	pub client_private_key: Option<Vec<u8>>,

	/// Server certificate to check against, in DER format.
	///
	/// If provided, it is checked against the server's TLS certificate, and fails the connection if there
	/// is a mismatch. If omitted, the server's certificate will be accepted without any checks.
	///
	/// In a config file, this is a path to a file. The file will be read as part of
	/// deserialization.
	#[serde(deserialize_with = "QuicClientNodeCfg::deser_file")]
	pub expected_server_certificate: Option<Vec<u8>>,
}
impl QuicClientNodeCfg {
	fn deser_addr<'de, D: serde::Deserializer<'de>>(
		de: D,
	) -> Result<std::net::SocketAddr, D::Error> {
		use serde::de::Error;
		let addr = String::deserialize(de)?;
		let addr = std::net::ToSocketAddrs::to_socket_addrs(&addr)
			.map_err(D::Error::custom)?
			.into_iter()
			.next()
			.ok_or_else(|| {
				D::Error::custom(format!(
					"Parsing address string {:?} did not yield any addresses",
					addr
				))
			})?;
		Ok(addr)
	}

	fn deser_file<'de, D: serde::Deserializer<'de>>(de: D) -> Result<Option<Vec<u8>>, D::Error> {
		use serde::de::Error;
		match <Option<PathBuf>>::deserialize(de)? {
			Some(path) => {
				let content = std::fs::read(&path).map_err(D::Error::custom)?;
				Ok(Some(content))
			}
			None => Ok(None),
		}
	}
}

struct CertVerifier {
	expected_server_cert: Option<Vec<u8>>,
}
impl rustls::client::ServerCertVerifier for CertVerifier {
	fn verify_server_cert(
		&self,
		end_entity: &rustls::Certificate,
		intermediates: &[rustls::Certificate],
		_server_name: &rustls::ServerName,
		_scts: &mut dyn Iterator<Item = &[u8]>,
		_ocsp_response: &[u8],
		_now: std::time::SystemTime,
	) -> Result<rustls::client::ServerCertVerified, rustls::Error> {
		let expected_server_cert = match self.expected_server_cert {
			Some(ref c) => c,
			None => {
				// TODO: maybe check expiration
				return Ok(rustls::client::ServerCertVerified::assertion());
			}
		};

		if intermediates.len() != 0 {
			return Err(rustls::Error::General(
				"Not expecting any intermediate certificates".into(),
			));
		}

		// TODO: Is checking the contents of the serialized cert OK?
		if end_entity.0 == *expected_server_cert {
			Ok(rustls::client::ServerCertVerified::assertion())
		} else {
			Err(rustls::Error::General("Certificate mismatch".into()))
		}
	}
}

pub struct QuicClientNodeLoader {
	endpoints: Arc<EndpointShared>,
}
#[async_trait]
impl NodeLoader for QuicClientNodeLoader {
	async fn create(_: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized,
	{
		let endpoints = Arc::new(EndpointShared::new().await?);
		Ok(Self { endpoints })
	}

	fn name(&self) -> &str {
		"quic"
	}

	async fn load(
		&self,
		path: NodePathBuf,
		cfg: toml::value::Value,
		_tree_loader: &NodeTreeLoader,
	) -> anyhow::Result<Node> {
		let cfg = cfg
			.try_into::<QuicClientNodeCfg>()
			.with_context(|| format!("While parsing collection node config for path {:?}", path))?;

		let client_cert = match cfg.client_certificate {
			Some(client_cert) => {
				let client_key = cfg.client_private_key.ok_or_else(|| anyhow::anyhow!("`client_certificate` was specified but `client_private_key` was not, for path {:?}", path))?;
				Some((
					rustls::Certificate(client_cert),
					rustls::PrivateKey(client_key),
				))
			}
			None => {
				if cfg.client_private_key.is_some() {
					anyhow::bail!("`client_private_key` was specified but `client_certificate` was not, for path {:?}", path);
				}
				None
			}
		};

		let expected_server_cert = cfg
			.expected_server_certificate
			.map(|v| rustls::Certificate(v));

		let node = self
			.endpoints
			.clone()
			.connect(path, cfg.address, client_cert, expected_server_cert)
			.await?;
		Ok(Arc::pin(node))
	}
}
