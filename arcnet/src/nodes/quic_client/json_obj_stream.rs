use std::{
	collections::VecDeque,
	future::Future,
	pin::Pin,
	task::Poll,
};

use futures::StreamExt;

pub fn recv_json_stream<T: serde::de::DeserializeOwned>(
	stream: quinn::RecvStream,
) -> impl futures::stream::Stream<Item = anyhow::Result<T>> + Send {
	let bytes_stream = RecvChunkStream(stream, 4096);
	let item_stream = JsonRecvStream::<T, RecvChunkStream, _> {
		stream: bytes_stream,
		buffers: BytesBuffers::new(),
		kill: |s: &mut RecvChunkStream| {
			let _ = s.0.stop(1u32.into());
		},
		_ph: Default::default(),
	};
	item_stream
}

/// [`futures::stream::Stream`] adapter for [`quinn::RecvStream::read_chunk`]
struct RecvChunkStream(pub quinn::RecvStream, pub usize);
impl futures::stream::Stream for RecvChunkStream {
	type Item = Result<bytes::Bytes, quinn::ReadError>;

	fn poll_next(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> Poll<Option<Self::Item>> {
		let this = Pin::into_inner(self);
		let fut = this.0.read_chunk(this.1, true);
		futures::pin_mut!(fut);
		fut.poll(cx)
			.map(|v| v.map(|o| o.map(|ck| ck.bytes)).transpose())
	}
}

/// Queue of conceptually-concactenated `bytes::Bytes`
struct BytesBuffers {
	wip_buffers: VecDeque<bytes::Bytes>,
	wip_offset: usize,
}
impl BytesBuffers {
	pub fn new() -> Self {
		Self {
			wip_buffers: VecDeque::new(),
			wip_offset: 0,
		}
	}

	pub fn push(&mut self, b: bytes::Bytes) {
		self.wip_buffers.push_back(b)
	}

	pub fn consume(&mut self, amnt: usize) {
		self.wip_offset += amnt;
		loop {
			if self.wip_buffers.is_empty() {
				assert_eq!(self.wip_offset, 0);
				return;
			}

			if self.wip_offset >= self.wip_buffers[0].len() {
				let c = self.wip_buffers.pop_front().unwrap();
				self.wip_offset -= c.len();
			} else {
				break;
			}
		}
	}

	pub fn read_stored(&self) -> BytesBuffersReader {
		BytesBuffersReader {
			wip_buffers: &self.wip_buffers,
			arr_offset: 0,
			byte_offset: self.wip_offset,
		}
	}
}

/// [`std::io::Read`] reading from [`BytesBuffers`]
struct BytesBuffersReader<'a> {
	wip_buffers: &'a VecDeque<bytes::Bytes>,
	arr_offset: usize,
	byte_offset: usize,
}
impl<'a> std::io::Read for BytesBuffersReader<'a> {
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
		loop {
			// Check if at end of all chunks
			if self.arr_offset >= self.wip_buffers.len() {
				return Ok(0);
			}
			// Check if at end of this chunk
			let this_chunk = &self.wip_buffers[self.arr_offset];
			if self.byte_offset >= this_chunk.len() {
				self.arr_offset += 1;
				self.byte_offset = 0;
				continue;
			}

			let num_to_copy = buf.len().min(this_chunk.len() - self.byte_offset);
			buf[..num_to_copy]
				.copy_from_slice(&this_chunk[self.byte_offset..self.byte_offset + num_to_copy]);
			self.byte_offset += num_to_copy;
			return Ok(num_to_copy);
		}
	}
}

/// Async stream of delimited JSON objects.
struct JsonRecvStream<T, S, K> {
	stream: S,
	buffers: BytesBuffers,
	kill: K,
	_ph: std::marker::PhantomData<fn() -> T>,
}
impl<T, S, K> Unpin for JsonRecvStream<T, S, K> {}
impl<T, S, K> futures::stream::Stream for JsonRecvStream<T, S, K>
where
	S: futures::stream::Stream<Item = Result<bytes::Bytes, quinn::ReadError>> + Unpin,
	T: serde::de::DeserializeOwned,
	K: Fn(&mut S),
{
	type Item = anyhow::Result<T>;

	fn poll_next(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> Poll<Option<Self::Item>> {
		let this = Pin::into_inner(self);
		loop {
			// Try to read data we have.
			let mut eof_err = None;
			{
				let reader = this.buffers.read_stored();
				let mut deser = serde_json::de::Deserializer::from_reader(reader).into_iter();
				match deser.next() {
					Some(Ok(v)) => {
						let num_read = deser.byte_offset();
						this.buffers.consume(num_read);
						return Poll::Ready(Some(Ok(v)));
					}
					Some(Err(e)) if e.is_eof() => {
						eof_err = Some(e);
					}
					None => {}
					Some(Err(e)) => {
						(this.kill)(&mut this.stream);
						return Poll::Ready(Some(Err(anyhow::anyhow!(e))));
					}
				}
			}

			// Need more, grab a chunk
			let chunk = match this.stream.poll_next_unpin(cx) {
				Poll::Ready(Some(Ok(c))) => c,
				Poll::Ready(None) => {
					if let Some(eof_err) = eof_err {
						return Poll::Ready(Some(Err(anyhow::anyhow!(eof_err))));
					} else {
						return Poll::Ready(None);
					}
				}
				Poll::Ready(Some(Err(e))) => return Poll::Ready(Some(Err(anyhow::anyhow!(e)))),
				Poll::Pending => return Poll::Pending,
			};
			this.buffers.push(chunk);
		}
	}
}

#[cfg(test)]
mod tests {
	use bytes::Bytes;

	use super::*;

	fn read_all<R: std::io::Read>(mut r: R) -> Vec<u8> {
		let mut v = vec![];
		r.read_to_end(&mut v).unwrap();
		v
	}

	#[test]
	fn test_bytes_buffer() {
		let mut buffers = BytesBuffers::new();
		buffers.push(Bytes::from_static(b"deadbeef"));
		assert_eq!(read_all(buffers.read_stored()), b"deadbeef");
		buffers.push(Bytes::from_static(b"asdf"));
		assert_eq!(read_all(buffers.read_stored()), b"deadbeefasdf");
		buffers.push(Bytes::from_static(b"foobar"));
		assert_eq!(read_all(buffers.read_stored()), b"deadbeefasdffoobar");

		buffers.consume(4);
		assert_eq!(read_all(buffers.read_stored()), b"beefasdffoobar");
		buffers.consume(6);
		assert_eq!(read_all(buffers.read_stored()), b"dffoobar");
		buffers.consume(8);
		assert_eq!(read_all(buffers.read_stored()), b"");
	}
}
