//! Collection of nodes

use std::{
	pin::Pin,
	sync::Arc,
};

use anyhow::Context;
use async_trait::async_trait;
use futures::{
	stream::BoxStream,
	StreamExt,
};
use hashlink::LinkedHashMap;
use serde::Deserialize;

use crate::{
	cfg_loader::{
		NodeLoader,
		NodeTreeLoader,
	},
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::{
		GetRange,
		Node,
		NodeReader,
		NodeTrait,
		NodeWriter,
		Ref,
		StoredID,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
};

/// Collection of storage nodes.
///
/// Configured with an ordered map of subnodes, accessible via the `sub_node` method.
///
/// This node can be queried and altered directly. The semantics are documented on each method.
///
// TODO: cache which IDs are in what stores.
// TODO: use other stores if one errors.
pub struct Collection {
	path: NodePathBuf,
	subnodes: LinkedHashMap<String, Node>,
	default_write: String,
}
impl Collection {
	pub fn new(
		path: NodePathBuf,
		subnodes: LinkedHashMap<String, Node>,
		default_write: String,
	) -> Self {
		assert!(subnodes.contains_key(&default_write), "Tried to create `Collection` node with `default_write` node {:?} but that node is not a child", default_write);
		Self {
			path,
			subnodes,
			default_write,
		}
	}
}
#[async_trait]
impl NodeTrait for Collection {
	fn path(&self) -> &NodePath {
		&self.path
	}

	async fn sub_node(&self, name: &str) -> anyhow::Result<Option<Node>> {
		Ok(self.subnodes.get(name).cloned())
	}

	/// Iterates through each subnode in order, returning the first one that returns anything other than `Ok(None)`.
	///
	/// TODO: also skip/log errors
	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>> {
		for node in self.subnodes.values() {
			if let Some(f) = node.clone().get_file(id, range).await? {
				return Ok(Some(f));
			}
		}
		Ok(None)
	}

	/// Pushes a file to the `default_write` node.
	async fn push_file(self: Pin<Arc<Self>>, meta: Metadata) -> anyhow::Result<NodeWriter> {
		let node = self.subnodes.get(&self.default_write).unwrap().clone();
		node.push_file(meta).await
	}

	/// Iterates through each subnode in order, returning the first one that returns anything other than `Ok(None)`.
	///
	/// TODO: also skip/log errors
	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>> {
		for node in self.subnodes.values() {
			if let Some(f) = node.get_ref(id).await? {
				return Ok(Some(f));
			}
		}
		Ok(None)
	}

	/// First sees if the ref exists on any subnode, in order. If it exists, update it in the first subnode it exists in,
	/// otherwise writes the ref to the `default_write` node.
	///
	/// TODO: also skip/log errors
	async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		expected_existing: Option<&FileID>,
	) -> Result<Ref, SetReferenceError> {
		for node in self.subnodes.values() {
			if let Some(_) = node.get_ref(id).await? {
				return node.set_ref(id, target, expected_existing).await;
			}
		}
		let node = self.subnodes.get(&self.default_write).unwrap().clone();
		node.set_ref(id, target, expected_existing).await
	}

	/// Returns [`crate::errors::Unsupported`].
	async fn delete(&self, _: &ID) -> anyhow::Result<()> {
		return Err(anyhow::anyhow!(crate::errors::Unsupported)
			.context(format!("While deleting from {}", self.path)));
	}

	/// Concatenates the stored_ids streams from each subnode.
	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		let mut streams = vec![];
		for sub in self.subnodes.values() {
			streams.push(sub.clone().stored_ids().await?);
		}

		Ok(Box::pin(futures::stream::iter(streams).flatten()))
	}

	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		// TODO: just return the iterator
		Ok(Box::pin(futures::stream::iter(
			self.subnodes
				.keys()
				.map(|v| Ok(v.clone()))
				.collect::<Vec<_>>(),
		)))
	}
}

/// Node loader for [`Collection`].
pub struct CollectionNodeLoader;
#[async_trait]
impl NodeLoader for CollectionNodeLoader {
	async fn create(_: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self)
	}

	fn name(&self) -> &str {
		"collection"
	}

	async fn load(
		&self,
		path: NodePathBuf,
		cfg: toml::Value,
		tree_loader: &NodeTreeLoader,
	) -> anyhow::Result<Node> {
		let cfg = cfg
			.try_into::<CollectionNodeCfg>()
			.with_context(|| format!("While parsing collection node config for path {:?}", path))?;

		let mut subnodes = LinkedHashMap::with_capacity(cfg.subnodes.len());
		for (k, v) in cfg.subnodes {
			let mut sub_path = path.clone();
			sub_path
				.try_push(&k)
				.with_context(|| format!("While adding path component {:?}", k))?;
			let node = tree_loader.create_from_cfg_value(sub_path, v).await?;
			subnodes.insert(k, node);
		}
		Ok(Arc::pin(Collection::new(
			path,
			subnodes,
			cfg.default_write_node,
		)))
	}
}

/// Config for [`CollectionNodeLoader`]
#[derive(Deserialize, Clone, Debug)]
pub struct CollectionNodeCfg {
	subnodes: LinkedHashMap<String, toml::Value>,
	default_write_node: String,
}
