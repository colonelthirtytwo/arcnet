//! Utilities for `Node` implementors.

use tokio::io::{
	AsyncReadExt,
	AsyncSeekExt,
};

use crate::{
	identifiers::FileID,
	metadata::Metadata,
	nodes::{
		GetRange,
		NodeReadTrait,
	},
	path::NodePathBuf,
};

/// Basic implementation of [`NodeReadTrait`] where all of the metadata and request info is stored within the object.
#[pin_project::pin_project]
pub struct BasicNodeReader<R: tokio::io::AsyncRead + Send> {
	#[pin]
	pub reader: R,
	pub id: FileID,
	pub node_path: NodePathBuf,
	pub size: u64,
	pub metadata: Metadata,
	pub range: std::ops::Range<u64>,
}
impl<R: tokio::io::AsyncRead + Send> tokio::io::AsyncRead for BasicNodeReader<R> {
	fn poll_read(
		self: std::pin::Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		buf: &mut tokio::io::ReadBuf<'_>,
	) -> std::task::Poll<std::io::Result<()>> {
		let this = self.project();
		this.reader.poll_read(cx, buf)
	}
}
impl<R: tokio::io::AsyncRead + Send> NodeReadTrait for BasicNodeReader<R> {
	fn id(&self) -> &FileID {
		&self.id
	}

	fn metadata(&self) -> &Metadata {
		&self.metadata
	}

	fn size(&self) -> u64 {
		self.size
	}

	fn node_path(&self) -> &crate::path::NodePath {
		&self.node_path
	}

	fn range(&self) -> std::ops::Range<u64> {
		self.range.clone()
	}
}

impl<R: tokio::io::AsyncRead + tokio::io::AsyncSeek + Send + Unpin> BasicNodeReader<R> {
	/// Seeks and limits the contained reader to read from the specified [`GetRange`].
	///
	/// For this to work, `reader` and `size` must be filled out. `range` is overwritten. The rest of the fields
	/// are forwarded as-is.
	///
	/// Easy way to apply a `GetRange`. Just fill out all of the fields except `range`, then call this function with
	/// the requested `GetRange`.
	pub async fn apply_range(
		self,
		req_range: &GetRange,
	) -> std::io::Result<BasicNodeReader<tokio::io::Take<R>>> {
		let mut reader = self.reader;
		let range = req_range.clamped(self.size);
		reader.seek(std::io::SeekFrom::Start(range.start)).await?;
		let reader = reader.take(range.end.saturating_sub(range.start));
		Ok(BasicNodeReader {
			reader,
			id: self.id,
			node_path: self.node_path,
			size: self.size,
			metadata: self.metadata,
			range,
		})
	}
}
