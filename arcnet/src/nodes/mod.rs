//! Implementations of storage nodes.
//!
//! Some nodes are only available if their corresponding crate feature is enabled, to prevent
//! bloating applications that do not need them (for example, an arcnet client will likely not
//! need anything that stores data locally).

pub mod collection;
#[cfg(feature = "node_fs_indexer")]
pub mod fs_indexer;
pub mod mem;
#[cfg(feature = "node_quic_client")]
pub mod quic_client;
#[cfg(feature = "node_sqlite")]
pub mod sqlite;
pub mod util;

use std::{
	pin::Pin,
	sync::Arc,
};

use async_trait::async_trait;
use futures::stream::BoxStream;
use serde::{
	Deserialize,
	Serialize,
};
use tokio::io::{
	AsyncReadExt,
	AsyncSeekExt,
};

use crate::{
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	path::{
		NodePath,
		NodePathBuf,
	},
};

/// Pinned [`Arc`]'d node
pub type Node = Pin<Arc<dyn NodeTrait + Send + Sync>>;

/// Node in the storage tree.
///
/// Low level interface for accessing files and refs.
///
/// Methods that return boxed trait objects take a `Pin<Arc<Self>`, so the implementations of the
/// returned objects can store a reference. This necessitates a clone.
#[async_trait]
pub trait NodeTrait: std::any::Any + Send + Sync {
	/// Gets the full node path.
	fn path(&self) -> &NodePath;

	/// Gets an immediate child subnode.
	///
	/// Does not recurse - only gets immediate children.
	///
	/// This should not be relied upon for testing whether a node exists or not.
	/// For some nodes (ex remeote services), this may return a valid node object,
	/// even if the node technically does not exist, as verifying that with a request
	/// would be too expensive. Such a node should return the [`NoSuchNode`](crate::errors::NoSuchNode)
	/// error for all of its operations.
	///
	/// Use [`NodeTrait::sub_nodes`] to get a conclusive list of direct children nodes.
	///
	async fn sub_node(&self, name: &str) -> anyhow::Result<Option<Node>>;

	/// Gets a file, if stored.
	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>>;

	/// Stores a file.
	///
	/// Returns a `WriteFile` object that the client should write data to.
	/// If the WriteFile is dropped without calling `finalize`, the object
	/// should not be stored.
	async fn push_file(self: Pin<Arc<Self>>, meta: Metadata) -> anyhow::Result<NodeWriter>;

	/// Gets a reference, if exists in this store.
	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>>;

	/// Updates a reference.
	///
	/// Atomically set the reference target. If `expected_existing` is not `None`, then fail unless
	/// the reference is currently set to the `expected_existing` (opportunistic locking). Colloroy:
	/// `expected_existing` must be `None` to create a new reference.
	async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		expected_existing: Option<&FileID>,
	) -> Result<Ref, SetReferenceError>;

	/// Deletes a file or ref from a leaf storage node.
	///
	/// Does nothing if the specified file ID is not stored.
	///
	/// Composite nodes that don't directly manage their storage should not implement this,
	/// and return [`Unsupported`](crate::errors::Unsupported) instead.
	async fn delete(&self, id: &ID) -> anyhow::Result<()>;

	/// Gets a reference and reads its target file.
	///
	/// This is similar to passing the result of [`get_ref`](NodeTrait::get_ref) to [`get_file`](NodeTrait::get_file),
	/// and the default implementation does this, but remote nodes can save a round trip by doing the two operations in one go.
	async fn get_resolved_ref(
		self: Pin<Arc<Self>>,
		id: &RefID,
		range: &GetRange,
	) -> anyhow::Result<ResolvedRef> {
		let ref_ = match self.get_ref(id).await? {
			Some(v) => v,
			None => return Ok(ResolvedRef::NoRef),
		};

		let f = match self.get_file(&ref_.target, range).await? {
			Some(f) => f,
			None => {
				return Ok(ResolvedRef::DanglingRef {
					ref_id: id.clone(),
					ref_,
				})
			}
		};

		Ok(ResolvedRef::ResolvedRef { ref_, file: f })
	}

	/// Returns a stream over all IDs that this stores.
	///
	/// Iteration may produce dubious results if concurrently modified. No particular order is required.
	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>>;

	/// Returns a stream over all known nodes that may be retrevied via [`sub_node`](NodeTrait::sub_node)
	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>>;
}

#[cfg(test)]
const _ASSERT_NODE_TRAIT_DYN: Option<Box<dyn NodeTrait>> = None;

/// Returned ref information, from [`NodeTrait::get_ref`]
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Ref {
	/// File ID that the ref is pointing to
	pub target: FileID,
	/// Full node path of where the ref is actually stored.
	pub node_path: NodePathBuf,
}

/// Information about a pushed file, from [`NodeTrait::push_file`] and [`NodeWriteTrait::finish`].
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct FinishedFile {
	/// Computed file ID
	pub id: FileID,
	/// Full node path of where the file is actually stored.
	pub node_path: NodePathBuf,
}

/// Item from [`NodeTrait::stored_ids`]
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct StoredID {
	/// ID of the item
	pub id: ID,
	/// Full node path of where the file or ref is actually stored.
	pub node_path: NodePathBuf,
}

/// Range of a file to retreive, passed to [`NodeTrait::get_file`].
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum GetRange {
	/// Get the entire file
	Entire,
	/// Get the specified byte range.
	///
	/// The beginning index is inclusive and the ending index is exclusive.
	/// The range is clamped to the size of the file - that is, the file may not fill the range entirely.
	/// If the start index is beyond the end of the file, or is greater than or equal to the end index,
	/// an empty reader should be returned.
	///
	/// Fetching a file with a zero-sized range is valid, and useful for testing existence or getting metadata
	/// without fetching a possibly large file.
	Range(u64, u64),
}
impl GetRange {
	/// Clamps the range to fit within the passed in file size.
	pub fn clamped(&self, file_size: u64) -> std::ops::Range<u64> {
		match self {
			Self::Entire => 0..file_size,
			Self::Range(start, end) => file_size.min(*start)..file_size.min(*end),
		}
	}

	/// Clamps the range to fit within the passed in file size, given as a usize.
	pub fn clamped_usize(&self, file_size: usize) -> std::ops::Range<usize> {
		match self {
			Self::Entire => 0..file_size,
			Self::Range(start, end) => {
				let start = (file_size as u64).min(*start) as usize;
				let end = (file_size as u64).min(*end) as usize;
				start..end
			}
		}
	}

	/// Seeks and limits a reader as appropriate for this range.
	pub async fn slice_reader<R: tokio::io::AsyncRead + tokio::io::AsyncSeek + Unpin>(
		&self,
		mut reader: R,
		reader_len: u64,
	) -> std::io::Result<tokio::io::Take<R>> {
		let range = self.clamped(reader_len);
		reader.seek(std::io::SeekFrom::Start(range.start)).await?;
		let len = range.end.saturating_sub(range.start);
		Ok(reader.take(len))
	}
}
impl Default for GetRange {
	fn default() -> Self {
		GetRange::Entire
	}
}
impl From<std::ops::Range<u64>> for GetRange {
	fn from(r: std::ops::Range<u64>) -> Self {
		Self::Range(r.start, r.end)
	}
}

/// File reader returned by [`NodeTrait::get_file`], implements [`tokio::io::AsyncRead`].
///
/// Has some extra methods for fetching the metadata of the file.
pub type NodeReader = Pin<Box<dyn NodeReadTrait>>;

/// File writer returned by [`NodeTrait::push_file`], implements [`tokio::io::AsyncWrite`].
///
/// Write the data you want to store, then call [`finish`](`NodeWriteTrait::finish`) to save and get the calculated
/// [`FileID`] for the content. If dropped without calling `finish`, the file will not be created.
pub type NodeWriter = Pin<Box<dyn NodeWriteTrait>>;

/// Trait for [`NodeReader`].
///
/// Stores will want to implement this for their file readers. Users of the API should use `NodeReader`, which
/// is an alias for this trait object in a pinned box.
pub trait NodeReadTrait: tokio::io::AsyncRead + Send {
	/// ID of this file.
	fn id(&self) -> &FileID;
	/// Metadata of this file.
	fn metadata(&self) -> &Metadata;
	/// Total size of this file.
	///
	/// This is the entire file's size; i.e. reading and slicing does not affect the return size.
	fn size(&self) -> u64;
	/// Node path where the file is stored.
	///
	/// This should be the full path of the node that is actually responsible for storing the file,
	/// and so it may be different than the queried node if the queried node is an aggregator.
	fn node_path(&self) -> &NodePath;
	/// Range of the file this reader is reading over.
	///
	/// Corresponds to the clamped [`GetRange`] that was requested, or the entire range for [`GetRange::Entire`].
	/// As with [`Self::size`], reading does not alter this value.
	fn range(&self) -> std::ops::Range<u64>;
}
impl<T> NodeReadTrait for Box<T>
where
	T: NodeReadTrait + Unpin + ?Sized,
{
	fn id(&self) -> &FileID {
		(**self).id()
	}

	fn metadata(&self) -> &Metadata {
		(**self).metadata()
	}

	fn size(&self) -> u64 {
		(**self).size()
	}

	fn node_path(&self) -> &NodePath {
		(**self).node_path()
	}

	fn range(&self) -> std::ops::Range<u64> {
		(**self).range()
	}
}
impl<'a, T> NodeReadTrait for &'a mut T
where
	T: NodeReadTrait + Unpin + ?Sized,
{
	fn id(&self) -> &FileID {
		(**self).id()
	}

	fn metadata(&self) -> &Metadata {
		(**self).metadata()
	}

	fn size(&self) -> u64 {
		(**self).size()
	}

	fn node_path(&self) -> &NodePath {
		(**self).node_path()
	}

	fn range(&self) -> std::ops::Range<u64> {
		(**self).range()
	}
}
impl<P> NodeReadTrait for Pin<P>
where
	P: std::ops::DerefMut + Unpin + Send,
	P::Target: NodeReadTrait,
{
	fn id(&self) -> &FileID {
		(**self).id()
	}

	fn metadata(&self) -> &Metadata {
		(**self).metadata()
	}

	fn size(&self) -> u64 {
		(**self).size()
	}

	fn node_path(&self) -> &NodePath {
		(**self).node_path()
	}

	fn range(&self) -> std::ops::Range<u64> {
		(**self).range()
	}
}

/// Trait for [`NodeWriter`].
///
/// Stores will want to implement this for their file readers. Users of the API should use `NodeWriter`, which
/// is an alias for this trait object in a pinned box.
#[async_trait]
pub trait NodeWriteTrait: tokio::io::AsyncWrite + Send {
	/// Finishes and commits the file, returning its ID.
	///
	/// Users must call this to finish saving the file and returns its ID. If the object is dropped without
	/// calling `finish`, saving is aborted.
	///
	/// This implicitly shuts down the writer. Users may, but aren't required to, shut down the writer
	/// before finishing.
	async fn finish(self: Pin<Box<Self>>) -> anyhow::Result<FinishedFile>;
}

/// Result from [`NodeTrait::get_resolved_ref`].
pub enum ResolvedRef {
	/// Reference does not exist
	NoRef,
	/// Reference exists, but the pointed-to file does not.
	DanglingRef { ref_id: RefID, ref_: Ref },
	/// Reference and file does not exist.
	ResolvedRef { ref_: Ref, file: NodeReader },
}
impl ResolvedRef {
	/// Converts a resolved reference to an optional reader.
	///
	/// `NoRef` is mapped to `Ok(None)`.
	/// `ResolvedRef` is mapped to `Ok(Some(file))`.
	/// `DanglingRef` is mapped to `Err(crate::errors::DanglingRef)`.
	pub fn to_opt_reader(self) -> Result<Option<NodeReader>, crate::errors::DanglingRef> {
		match self {
			Self::NoRef => Ok(None),
			Self::DanglingRef { ref_id, ref_ } => Err(crate::errors::DanglingRef {
				ref_id,
				ref_node_path: ref_.node_path,
				target_id: ref_.target,
			}),
			Self::ResolvedRef { file, .. } => Ok(Some(file)),
		}
	}
}

/// Macro that implements a bunch of generic tests on a leaf storage node.
#[macro_export]
macro_rules! node_tests {
	(
		$name:ident, |$path:ident| $setup:expr
	) => {
		paste::paste!{
			async fn [<$name _setup>]($path : $crate::path::NodePathBuf) -> ($crate::nodes::Node, std::pin::Pin<Box<dyn std::future::Future<Output=()> + Send>>) {
				$setup
			}

			#[cfg(test)]
			#[tokio::test]
			async fn [<$name _empty_stored_ids>]() {
				let (node, end_fut) = [<$name _setup>]($crate::path::NodePathBuf::root()).await;
				use futures::stream::TryStreamExt;
				let ids = node.stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert!(ids.is_empty(), "{:?}", ids);
				end_fut.await;
			}

			#[cfg(test)]
			#[tokio::test]
			async fn [<$name _file_push_get_delete>]() {
				use futures::stream::TryStreamExt;
				use tokio::io::{AsyncReadExt, AsyncWriteExt};

				let path =  "/foo/bar/baz".parse::<$crate::path::NodePathBuf>().unwrap();
				let (node, end_fut) = [<$name _setup>](path.clone()).await;

				const TEST_STR: &[u8] = b"hello world!";
				let mut f = node.clone().push_file($crate::metadata::Metadata {
					links: std::collections::BTreeSet::new(),
				}).await.unwrap();
				f.write_all(TEST_STR).await.unwrap();
				let $crate::nodes::FinishedFile { id, node_path } = f.finish().await.unwrap();
				assert_eq!(id, "sha3:fa35d698efb6c2180698c4941c1dedf2c46bc8ddcc3bec00eb75dd6112194326".parse().unwrap());
				assert_eq!(node_path, path);

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Entire).await.unwrap().unwrap();
				assert_eq!(res.size(), TEST_STR.len() as u64);
				assert_eq!(res.range(), 0..12);
				assert!(res.metadata().links.is_empty());
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert_eq!(v.as_slice(), TEST_STR);

				let list = node.clone().stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert_eq!(list, vec![$crate::nodes::StoredID { id: id.clone().into(), node_path: path.clone() }]);

				node.delete(&id.clone().into()).await.expect("While deleting");
				assert!(node.clone().get_file(&id, &$crate::nodes::GetRange::Entire).await.unwrap().is_none());
				let list = node.clone().stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert_eq!(list, vec![]);

				end_fut.await;
			}

			#[cfg(test)]
			#[tokio::test]
			async fn [<$name _ref_set_get_delete>]() {
				use futures::stream::TryStreamExt;

				let path =  "/foo/bar/baz".parse::<$crate::path::NodePathBuf>().unwrap();
				let (node, end_fut) = [<$name _setup>](path.clone()).await;

				let ref_id = "ref:79e76960c57b11ecbf76e7d2e7f1a3f9".parse().unwrap();
				let target_id = "sha3:fa35d698efb6c2180698c4941c1dedf2c46bc8ddcc3bec00eb75dd6112194326".parse().unwrap();
				assert!(node.get_ref(&ref_id).await.unwrap().is_none());

				node.set_ref(&ref_id, &target_id, None).await.unwrap();
				let $crate::nodes::Ref{target, node_path} = node.get_ref(&ref_id).await.expect("While getting ref").expect("Expected a ref");
				assert_eq!(target, target_id);
				assert_eq!(node_path, path);
				let list = node.clone().stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert_eq!(list, vec![$crate::nodes::StoredID { id: ref_id.clone().into(), node_path: path.clone() }]);

				node.delete(&ref_id.clone().into()).await.expect("While deleting");
				let list = node.clone().stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert_eq!(list, vec![]);

				end_fut.await;
			}

			#[cfg(test)]
			#[tokio::test]
			async fn [<$name _ref_set_get_expected>]() {
				use futures::stream::TryStreamExt;

				let path =  "/foo/bar/baz".parse::<$crate::path::NodePathBuf>().unwrap();
				let (node, end_fut) = [<$name _setup>](path.clone()).await;

				let ref_id = "ref:79e76960c57b11ecbf76e7d2e7f1a3f9".parse().unwrap();
				let target_id_1 = "sha3:0000000000000000000000000000000000000000000000000000000000000001".parse().unwrap();
				let target_id_2 = "sha3:0000000000000000000000000000000000000000000000000000000000000002".parse().unwrap();
				let target_id_3 = "sha3:0000000000000000000000000000000000000000000000000000000000000003".parse().unwrap();

				node.set_ref(&ref_id, &target_id_1, None).await.unwrap();
				let $crate::nodes::Ref { target, node_path } = node.get_ref(&ref_id).await.unwrap().unwrap();
				assert_eq!(target, target_id_1);
				assert_eq!(node_path, path);

				assert!(matches!(node.set_ref(&ref_id, &target_id_3, Some(&target_id_2)).await, Err($crate::errors::SetReferenceError::DidNotMatch { .. })));
				let $crate::nodes::Ref { target, node_path } = node.get_ref(&ref_id).await.unwrap().unwrap();
				assert_eq!(target, target_id_1);
				assert_eq!(node_path, path);

				node.set_ref(&ref_id, &target_id_3, Some(&target_id_1)).await.expect("Expected to succeed on set_ref with matching target id");
				let $crate::nodes::Ref { target, node_path } = node.get_ref(&ref_id).await.unwrap().unwrap();
				assert_eq!(target, target_id_3);
				assert_eq!(node_path, path);

				let list = node.clone().stored_ids().await.unwrap().try_collect::<Vec<_>>().await.unwrap();
				assert_eq!(list, vec![$crate::nodes::StoredID { id: ref_id.clone().into(), node_path: path.clone() }]);

				end_fut.await;
			}

			#[cfg(test)]
			#[tokio::test]
			async fn [<$name _file_get_ranges>]() {
				use tokio::io::{AsyncReadExt, AsyncWriteExt};

				let path =  "/foo/bar/baz".parse::<$crate::path::NodePathBuf>().unwrap();
				let (node, end_fut) = [<$name _setup>](path.clone()).await;

				const TEST_STR: &[u8] = b"hello world!";
				let mut f = node.clone().push_file($crate::metadata::Metadata {
					links: std::collections::BTreeSet::new(),
				}).await.unwrap();
				f.write_all(TEST_STR).await.unwrap();
				let $crate::nodes::FinishedFile { id, .. } = f.finish().await.unwrap();

				fn test_meta(f: &$crate::nodes::NodeReader) {
					assert_eq!(f.size(), TEST_STR.len() as u64);
					assert!(f.metadata().links.is_empty());
				}

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Entire).await.unwrap().unwrap();
				test_meta(&res);
				assert_eq!(res.range(), 0..12, "Test entire range");
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert_eq!(v.as_slice(), TEST_STR);

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Range(2,12)).await.unwrap().unwrap();
				test_meta(&res);
				assert_eq!(res.range(), 2..12, "Test range 2..12");
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert_eq!(v.as_slice(), &TEST_STR[2..12]);

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Range(2,10)).await.unwrap().unwrap();
				test_meta(&res);
				assert_eq!(res.range(), 2..10, "Test range 2..10");
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert_eq!(v.as_slice(), &TEST_STR[2..10]);

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Range(2,999)).await.unwrap().unwrap();
				test_meta(&res);
				assert_eq!(res.range(), 2..12, "Test range 2..999");
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert_eq!(v.as_slice(), &TEST_STR[2..12]);

				let mut res = node.clone().get_file(&id, &$crate::nodes::GetRange::Range(5,5)).await.unwrap().unwrap();
				test_meta(&res);
				assert!(res.range().is_empty());
				let mut v = vec![];
				res.read_to_end(&mut v).await.unwrap();
				assert!(v.is_empty());

				end_fut.await;
			}
		}
	};
}
pub use node_tests;
