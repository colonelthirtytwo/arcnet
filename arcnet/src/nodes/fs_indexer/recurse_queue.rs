use std::sync::atomic::{
	AtomicUsize,
	Ordering,
};

use tokio::sync::Notify;

/// MPMC Work queue for recursive tree that doesn't become "empty" until all items are popped and processed.
///
/// Specifically, poping an item returns both the item and a [`RecurseQueueToken`]. The queue is not considered
/// empty until both the actual queue is empty and each returned [`RecurseQueueToken`] has been dropped. This
/// lets workers add more items to the queue as they are discovered.
#[derive(Debug)]
pub struct RecurseQueue<T> {
	send: crossbeam_channel::Sender<T>,
	recv: crossbeam_channel::Receiver<T>,
	num_pending_processing: AtomicUsize,
	wakeup: Notify,
}

impl<T> RecurseQueue<T> {
	pub fn new<I: IntoIterator<Item = T>>(initials: I) -> Self {
		let (send, recv) = crossbeam_channel::unbounded();
		let mut n = 0;
		for v in initials {
			n += 1;
			send.try_send(v).unwrap();
		}

		Self {
			send,
			recv,
			num_pending_processing: AtomicUsize::new(n),
			wakeup: Notify::new(),
		}
	}

	pub async fn pop<'a>(&'a self) -> Option<(T, RecurseQueueToken<'a, T>)> {
		loop {
			let notify = self.wakeup.notified();
			let c = self.num_pending_processing.load(Ordering::Acquire);
			if c == 0 || c > usize::MAX << 1 {
				self.num_pending_processing.store(0, Ordering::Relaxed);
				return None;
			}
			match self.recv.try_recv() {
				Ok(v) => return Some((v, RecurseQueueToken { parent: self })),
				Err(crossbeam_channel::TryRecvError::Empty) => {
					notify.await;
					continue;
				}
				Err(crossbeam_channel::TryRecvError::Disconnected) => unreachable!(),
			}
		}
	}
}

/// Token returned with the item by [`RecurseQueue::pop`].
pub struct RecurseQueueToken<'a, T> {
	parent: &'a RecurseQueue<T>,
}
impl<'a, T> RecurseQueueToken<'a, T> {
	pub fn push(&self, v: T) {
		let c = self
			.parent
			.num_pending_processing
			.fetch_add(1, Ordering::Acquire);
		if c >= usize::MAX << 1 {
			panic!("Overflow or misused");
		}
		self.parent.send.try_send(v).unwrap();
		self.parent.wakeup.notify_one();
	}
}
impl<'a, T> std::ops::Drop for RecurseQueueToken<'a, T> {
	fn drop(&mut self) {
		let c = self
			.parent
			.num_pending_processing
			.fetch_sub(1, Ordering::Release);
		if c == 1 {
			self.parent.wakeup.notify_waiters();
		}
		if c == 0 {
			unreachable!();
		}
	}
}

#[cfg(test)]
mod tests {
	use std::sync::Arc;

	use super::*;

	#[tokio::test]
	async fn test_queue() {
		let queue = Arc::new(RecurseQueue::new(vec![1]));

		while let Some((item, token)) = queue.pop().await {
			if item < 5 {
				token.push(item + 1);
			}
			assert!(item >= 1 && item <= 5, "{}", item);
		}
	}
}
