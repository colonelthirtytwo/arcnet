mod recurse_queue;

use std::{
	collections::HashMap,
	path::{
		Path,
		PathBuf,
	},
	pin::Pin,
	sync::Arc,
};

use anyhow::Context;
use async_trait::async_trait;
use bytes::Bytes;
use digest::Digest;
use futures::{
	stream::BoxStream,
	StreamExt,
};
use tokio::{
	io::AsyncReadExt,
	sync::RwLock,
};

use self::recurse_queue::{
	RecurseQueue,
	RecurseQueueToken,
};
use super::StoredID;
use crate::{
	cfg_loader::{
		NodeLoader,
		NodeTreeLoader,
	},
	digest::ArcnetDigest,
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::{
		util::BasicNodeReader,
		GetRange,
		Node,
		NodeReader,
		NodeWriter,
		Ref,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
	structures::fs::{
		FileList,
		FileListEntry,
	},
};

pub struct FsIndexerNode {
	node_path: NodePathBuf,
	_scan_path: PathBuf,
	index_ref_id: RefID,
	index: Arc<RwLock<Option<ScanResults>>>,
}

struct ScanResults {
	files_by_id: HashMap<FileID, IndexedFile>,
	index_id: FileID,
	index_meta: Metadata,
	index_bytes: Bytes,
}

struct IndexedFile {
	pub metadata: Metadata,
	pub size: u64,
	pub path: PathBuf,
}

impl FsIndexerNode {
	pub fn new(
		node_path: NodePathBuf,
		scan_path: PathBuf,
		scan_tasks: usize,
		index_ref_id: RefID,
	) -> Self {
		let index = Arc::new(RwLock::new(None));
		tokio::task::spawn(ScanResults::scan(
			index.clone(),
			scan_path.clone(),
			scan_tasks,
		));
		Self {
			node_path,
			_scan_path: scan_path,
			index_ref_id,
			index,
		}
	}
}

#[async_trait]
impl super::NodeTrait for FsIndexerNode {
	fn path(&self) -> &NodePath {
		&self.node_path
	}

	async fn sub_node(&self, _name: &str) -> anyhow::Result<Option<Node>> {
		Ok(None)
	}

	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		Ok(Box::pin(futures::stream::empty()))
	}

	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>> {
		let index = self.index.read().await;
		let index = match *index {
			Some(ref v) => v,
			None => return Ok(None),
		};

		if id == &index.index_id {
			let range = range.clamped_usize(index.index_bytes.len());
			let contents = index.index_bytes.slice(range.clone());

			return Ok(Some(Box::pin(BasicNodeReader {
				reader: std::io::Cursor::new(contents),
				id: index.index_id.clone(),
				metadata: index.index_meta.clone(),
				size: index.index_bytes.len() as u64,
				node_path: self.node_path.clone(),
				range: range.start as u64..range.end as u64,
			})));
		}
		if let Some(item) = index.files_by_id.get(id) {
			let fs_file = match tokio::fs::File::open(&item.path).await {
				Ok(f) => f,
				Err(e) if e.kind() == std::io::ErrorKind::NotFound => return Ok(None),
				Err(e) => {
					return Err(
						anyhow::anyhow!(e).context(format!("While opening file {:?}", item.path))
					)
				}
			};
			let reader = BasicNodeReader {
				reader: tokio::io::BufReader::new(fs_file),
				id: id.clone(),
				metadata: item.metadata.clone(),
				size: item.size,
				node_path: self.node_path.clone(),
				range: 0..0,
			}
			.apply_range(range)
			.await
			.with_context(|| format!("While seeking on file {:?}", item.path))?;
			return Ok(Some(Box::pin(reader)));
		}
		Ok(None)
	}

	async fn push_file(self: Pin<Arc<Self>>, _meta: Metadata) -> anyhow::Result<NodeWriter> {
		anyhow::bail!(crate::errors::Unsupported)
	}

	async fn delete(&self, _id: &ID) -> anyhow::Result<()> {
		anyhow::bail!(crate::errors::Unsupported)
	}

	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>> {
		let index = self.index.read().await;
		let index = match *index {
			Some(ref v) => v,
			None => return Ok(None),
		};

		if id == &self.index_ref_id {
			Ok(Some(Ref {
				target: index.index_id.clone(),
				node_path: self.node_path.clone(),
			}))
		} else {
			Ok(None)
		}
	}

	async fn set_ref(
		&self,
		_id: &RefID,
		_target: &FileID,
		_expected_existing: Option<&FileID>,
	) -> anyhow::Result<Ref, SetReferenceError> {
		Err(SetReferenceError::Error(anyhow::anyhow!(
			crate::errors::Unsupported
		)))
	}

	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		let path = self.node_path.clone();
		let index = self.index.read().await;
		let index = match *index {
			Some(ref v) => v,
			None => return Ok(Box::pin(futures::stream::empty())),
		};

		let s1 = futures::stream::iter([
			ID::from(index.index_id.clone()),
			ID::from(self.index_ref_id.clone()),
		]);

		// TODO: don't allocate a vec. Needs a self-referencial struct.
		let s2 = futures::stream::iter(index.files_by_id.keys().cloned().collect::<Vec<_>>());

		Ok(Box::pin(
			s1.chain(s2.map(ID::from))
				.map(move |id| StoredID {
					id,
					node_path: path.clone(),
				})
				.map(Ok),
		))
	}
}

impl ScanResults {
	async fn scan(dest: Arc<RwLock<Option<Self>>>, path: PathBuf, num_tasks: usize) {
		let queue = Arc::new(RecurseQueue::new(Some(PathBuf::from(""))));
		let files_by_id = Arc::new(RwLock::new(HashMap::new()));
		let index = Arc::new(RwLock::new(FileList::new()));

		let mut handles = (0..num_tasks)
			.map(|_| {
				let queue = Arc::clone(&queue);
				let files_by_id = Arc::clone(&files_by_id);
				let index = Arc::clone(&index);
				tokio::task::spawn(Self::scan_task(queue, path.clone(), files_by_id, index))
			})
			.collect::<futures::stream::FuturesUnordered<_>>();

		while let Some(res) = handles.next().await {
			if let Err(e) = res {
				warn!("Scanner thread panicked: {:#}", e);
			}
		}

		let files_by_id = Arc::try_unwrap(files_by_id)
			.map_err(|_| ())
			.unwrap()
			.into_inner();
		let index = Arc::try_unwrap(index).map_err(|_| ()).unwrap().into_inner();

		let (index_res, ids) =
			crate::identifiers::track_id_serializations(|| rmp_serde::to_vec_named(&index));
		let index_bytes = Bytes::from(index_res.unwrap());
		let index_meta = Metadata::new(ids);
		let index_id = ArcnetDigest::from_bytes(&index_bytes, &index_meta);

		let new = Self {
			files_by_id,
			index_bytes,
			index_meta,
			index_id,
		};
		info!(
			"Finished scanning {:?}, found {:?} files",
			path,
			new.files_by_id.len()
		);
		*dest.write().await = Some(new);
	}

	async fn scan_task(
		queue: Arc<RecurseQueue<PathBuf>>,
		base_path: PathBuf,
		files_by_id: Arc<RwLock<HashMap<FileID, IndexedFile>>>,
		index: Arc<RwLock<FileList>>,
	) {
		trace!("Scan task started");
		while let Some((rel_path, token)) = queue.pop().await {
			if let Err(e) =
				Self::scan_one(&rel_path, &token, &base_path, &files_by_id, &index).await
			{
				warn!(
					"Skipping path {:?} due to error: {:#}",
					base_path.join(rel_path),
					e
				);
			}
		}
		trace!("Scan task ended");
	}

	async fn scan_one(
		this_rel_path: &Path,
		token: &RecurseQueueToken<'_, PathBuf>,
		base_path: &Path,
		files_by_id: &RwLock<HashMap<FileID, IndexedFile>>,
		file_list: &RwLock<FileList>,
	) -> anyhow::Result<()> {
		let this_abs_path = base_path.join(this_rel_path);
		trace!("Scanning path: {:?}", this_abs_path);
		let path_str = this_rel_path
			.to_str()
			.ok_or_else(|| anyhow::anyhow!("Path is not UTF-8"))?;

		let meta = tokio::fs::metadata(&this_abs_path)
			.await
			.context("While getting file metadata")?;
		if meta.is_dir() {
			if this_rel_path != Path::new("") {
				file_list
					.write()
					.await
					.insert(path_str.into(), FileListEntry::Directory {});
			}

			let mut dir = tokio::fs::read_dir(&this_abs_path).await?;
			while let Some(entry) = dir.next_entry().await? {
				token.push(this_rel_path.join(entry.file_name()))
			}
			return Ok(());
		}
		if meta.is_file() {
			let mut f = tokio::fs::File::open(&this_abs_path)
				.await
				.context("While opening file")?;

			// Try to infer mime type, first from signature, then from extension.
			let mut header = vec![];
			AsyncReadExt::take(&mut f, 1024 * 8)
				.read_to_end(&mut header)
				.await
				.context("While reading")?;

			let metadata = Metadata::new_leaf();

			let mut hasher = ArcnetDigest::new();
			hasher.update(&header);
			tokio::io::copy(&mut f, &mut hasher)
				.await
				.context("While reading")?;

			let (id, size) = hasher.finish(&metadata);

			debug!("Scan path {:?}: file id {}", this_abs_path, id);

			files_by_id.write().await.insert(
				id.clone(),
				IndexedFile {
					metadata,
					size,
					path: this_abs_path,
				},
			);
			// TODO: properly find executable flag
			file_list.write().await.insert(
				path_str.into(),
				FileListEntry::File {
					id: id.clone().into(),
					executable: false,
				},
			);
			return Ok(());
		}
		Err(anyhow::anyhow!("Not a regular file or directory"))
	}
}

pub struct FSIndexerNodeLoader;
#[async_trait]
impl NodeLoader for FSIndexerNodeLoader {
	async fn create(_: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self)
	}

	fn name(&self) -> &str {
		"fs_indexer"
	}

	async fn load(
		&self,
		path: NodePathBuf,
		cfg: toml::value::Value,
		_: &NodeTreeLoader,
	) -> anyhow::Result<Node> {
		let cfg: FSIndexerNodeCfg = cfg
			.try_into()
			.with_context(|| format!("While parsing fs index node config for path {:?}", path))?;
		Ok(Arc::pin(FsIndexerNode::new(
			path,
			cfg.scan_dir,
			cfg.scan_tasks,
			cfg.index_ref_id,
		)))
	}
}

#[derive(serde::Deserialize, Clone, Debug)]
pub struct FSIndexerNodeCfg {
	pub scan_dir: PathBuf,
	pub index_ref_id: RefID,
	#[serde(default = "FSIndexerNodeCfg::scan_tasks_default")]
	pub scan_tasks: usize,
}
impl FSIndexerNodeCfg {
	fn scan_tasks_default() -> usize {
		16
	}
}
