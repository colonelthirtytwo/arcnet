use std::{
	io,
	path::{
		Path,
		PathBuf,
	},
	pin::Pin,
	task::Poll,
};

use tokio::io::{
	AsyncWrite,
	AsyncWriteExt,
};

pub(super) struct TempFile {
	file: Option<tokio::fs::File>,
	path: PathBuf,
}
impl TempFile {
	pub async fn new(dir: &Path) -> io::Result<Self> {
		loop {
			let mut path = dir.to_path_buf();
			path.push(&Self::rand_name());

			let file = match tokio::fs::OpenOptions::new()
				.write(true)
				.create_new(true)
				.open(&path)
				.await
			{
				Ok(f) => f,
				Err(e) if e.kind() == io::ErrorKind::AlreadyExists => continue,
				Err(e) => return Err(e),
			};

			return Ok(Self {
				file: Some(file),
				path,
			});
		}
	}

	pub async fn commit(mut self, to: &Path) -> io::Result<()> {
		let mut f = self.file.take().unwrap();
		f.shutdown().await?;
		std::mem::drop(f);
		tokio::fs::rename(&self.path, to).await
	}

	pub async fn delete(mut self) -> io::Result<()> {
		self.shutdown().await?;
		let f = self.file.take().unwrap();
		std::mem::drop(f);

		tokio::fs::remove_file(&self.path).await
	}

	fn rand_name() -> String {
		use rand::seq::SliceRandom;
		let mut rng = rand::thread_rng();
		let mut name = String::with_capacity(9);
		name.push('.');
		for _ in 0..8 {
			name.push(
				*b"0123456789abcdefghijklmnopqrstuvwxyz"
					.choose(&mut rng)
					.unwrap() as char,
			);
		}
		name
	}
}
impl std::ops::Drop for TempFile {
	fn drop(&mut self) {
		if let Some(f) = self.file.take() {
			std::mem::drop(f);
			let _ = std::fs::remove_file(&self.path);
		}
	}
}
impl AsyncWrite for TempFile {
	fn poll_write(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		buf: &[u8],
	) -> Poll<Result<usize, io::Error>> {
		Pin::new(self.file.as_mut().unwrap()).poll_write(cx, buf)
	}

	fn poll_flush(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> Poll<Result<(), io::Error>> {
		Pin::new(self.file.as_mut().unwrap()).poll_flush(cx)
	}

	fn poll_shutdown(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> Poll<Result<(), io::Error>> {
		Pin::new(self.file.as_mut().unwrap()).poll_shutdown(cx)
	}
}
