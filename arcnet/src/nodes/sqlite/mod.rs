//! Stores files and refs in a SQLite database.
//!
//! Requires the "node_sqlite" crate feature.

mod tempfile;

use std::{
	path::{
		Path,
		PathBuf,
	},
	pin::Pin,
	sync::Arc,
	task::{
		Context,
		Poll,
	},
};

use anyhow::Context as _;
use async_trait::async_trait;
use digest::Digest;
use futures::stream::BoxStream;
use rusqlite::{
	params,
	OptionalExtension,
};
use serde::Deserialize;
use tokio::{
	io::{
		AsyncWriteExt,
		BufWriter,
	},
	sync::{
		mpsc,
		oneshot,
	},
};

use super::{
	util::BasicNodeReader,
	StoredID,
};
use crate::{
	cfg_loader::{
		NodeLoader,
		NodeTreeLoader,
	},
	digest::ArcnetDigest,
	errors::SetReferenceError,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::{
		FinishedFile,
		GetRange,
		Node,
		NodeReader,
		NodeTrait,
		NodeWriteTrait,
		NodeWriter,
		Ref,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
	utils::infer::InferWrite,
};

const SQL_APP_ID: i32 = 0xede64;

/// Stores files and refs in a SQLite database.
///
/// Good for storing refs as well as small files (ex configuration and metadata),
/// but very bad for storing large files. Files are entirely buffered in memory
/// before writing.
pub struct SqliteNode {
	path: NodePathBuf,
	managed_files_path: Option<PathBuf>,
	db_send: mpsc::Sender<DbMessage>,
}
impl SqliteNode {
	pub async fn open(
		node_path: NodePathBuf,
		db_path: &Path,
		managed_files_path: Option<PathBuf>,
	) -> anyhow::Result<Self> {
		if let Some(parent) = db_path.parent() {
			tokio::fs::create_dir_all(parent).await.with_context(|| {
				format!("While creating directories leading up to {:?}", db_path)
			})?;
		}
		if let Some(managed_files_path) = managed_files_path.as_ref() {
			tokio::fs::create_dir_all(managed_files_path)
				.await
				.with_context(|| {
					format!(
						"While creating directories leading up to {:?}",
						managed_files_path
					)
				})?;
		}

		let mut db = rusqlite::Connection::open(db_path)
			.with_context(|| format!("While opening db at {:?}", db_path))?;

		match db.query_row("PRAGMA application_id", [], |row| row.get::<_, i32>(0)) {
			Ok(0) => {
				// new db
				db.execute_batch(
					r#"
					PRAGMA encoding = 'UTF-8';
					PRAGMA journal_mode = WAL;
				"#,
				)?;

				let tx = db.transaction()?;
				tx.execute_batch(
					r#"
					CREATE TABLE files(
						id TEXT PRIMARY KEY NOT NULL UNIQUE,
						links TEXT NOT NULL,

						contents BLOB DEFAULT NULL,
						ext TEXT DEFAULT NULL,

						CHECK((contents IS NULL) != (ext IS NULL))
					);
					CREATE TABLE refs(
						id TEXT PRIMARY KEY NOT NULL UNIQUE,
						target TEXT NOT NULL
					);
				"#,
				)
				.context("While setting up db")?;
				// Have to use string formatting cuz pragma values do not accept query parameters.
				// Safe because its a const.
				tx.execute(
					&format!("PRAGMA application_id = {}", SQL_APP_ID),
					rusqlite::params!(),
				)
				.context("While setting application ID")?;
				tx.commit().context("While committing transaction")?;
			}
			Ok(v) if v == SQL_APP_ID => {
				// existing db
			}
			Ok(_) => {
				return Err(anyhow::anyhow!(
					"File at {:?} does not look like an arcnet db",
					db_path
				))
			}
			Err(e) => return Err(anyhow::anyhow!(e).context("While querying db application id")),
		};

		db.execute_batch(
			r#"
			PRAGMA foreign_keys = 1;
		"#,
		)
		.context("While setting up database")?;

		let (db_send, db_recv) = mpsc::channel(16);

		tokio::task::spawn_blocking({
			let node_path = node_path.clone();
			move || db_task(db, db_recv, node_path)
		});

		Ok(Self {
			path: node_path,
			db_send,
			managed_files_path,
		})
	}
}

#[async_trait]
impl NodeTrait for SqliteNode {
	fn path(&self) -> &NodePath {
		&self.path
	}

	async fn sub_node(&self, _name: &str) -> anyhow::Result<Option<Node>> {
		Ok(None)
	}

	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		Ok(Box::pin(futures::stream::empty()))
	}

	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>> {
		let (send, recv) = oneshot::channel();
		self.db_send
			.send(DbMessage::GetFile(id.clone(), range.clone(), send))
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		let (metadata, src) = match recv.await.context("DB task exited unexpectedly")?? {
			Some(v) => v,
			None => return Ok(None),
		};
		match src {
			FileSrc::Blob {
				contents,
				size,
				range,
			} => Ok(Some(Box::pin(BasicNodeReader {
				reader: std::io::Cursor::new(contents),
				id: id.clone(),
				metadata,
				node_path: self.path.clone(),
				size: size as u64,
				range: range.start as u64..range.end as u64,
			}))),
			FileSrc::File { ext } => {
				let managed_files_path = self.managed_files_path.as_ref().ok_or_else(|| {
					anyhow::anyhow!("File ID {:?} stored in managed directory but node was not configured with it", id)
				})?;
				let path = path_for_file(&managed_files_path, &id, &ext);
				let f = tokio::fs::File::open(&path)
					.await
					.with_context(|| format!("While opening {:?}", path))?;
				let file_meta = f
					.metadata()
					.await
					.with_context(|| format!("While getting metadata for {:?}", path))?;

				let reader = BasicNodeReader {
					reader: tokio::io::BufReader::new(f),
					id: id.clone(),
					metadata,
					size: file_meta.len(),
					node_path: self.path.clone(),
					range: 0..0,
				}
				.apply_range(range)
				.await
				.with_context(|| format!("While seeking {:?}", path))?;

				Ok(Some(Box::pin(reader)))
			}
		}
	}

	async fn push_file(self: Pin<Arc<Self>>, meta: Metadata) -> anyhow::Result<NodeWriter> {
		if let Some(managed_files_path) = self.managed_files_path.as_ref() {
			let f = tempfile::TempFile::new(managed_files_path)
				.await
				.with_context(|| format!("While creating file at {:?}", managed_files_path))?;
			Ok(Box::pin(FileWriter {
				parent: self,
				metadata: meta,
				hasher: ArcnetDigest::new(),
				f: InferWrite::new(BufWriter::new(f)),
			}))
		} else {
			Ok(Box::pin(BlobWriter {
				parent: self,
				metadata: meta,
				f: vec![],
				hasher: ArcnetDigest::new(),
			}))
		}
	}

	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>> {
		let (send, recv) = oneshot::channel();
		self.db_send
			.send(DbMessage::GetRef(id.clone(), send))
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		recv.await
			.context("DB task exited unexpectedly")
			.and_then(|e| e)
	}

	async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		expected_existing: Option<&FileID>,
	) -> Result<Ref, SetReferenceError> {
		let (send, recv) = oneshot::channel();
		self.db_send
			.send(DbMessage::SetRef {
				id: id.clone(),
				target: target.clone(),
				expected_existing: expected_existing.cloned(),
				response: send,
			})
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		recv.await
			.context("DB task exited unexpectedly")
			.map_err(SetReferenceError::Error)
			.and_then(|e| e)
	}

	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		let (send, recv) = oneshot::channel();
		self.db_send
			.send(DbMessage::StoredIDs(send))
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		recv.await
			.context("DB task exited unexpectedly")
			.and_then(|e| e)
	}

	async fn delete(&self, id: &ID) -> anyhow::Result<()> {
		let (send, recv) = oneshot::channel();
		self.db_send
			.send(DbMessage::Delete(id.clone(), send))
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		let ext = recv
			.await
			.context("DB task exited unexpectedly")
			.and_then(|e| e)?;

		if let Some(ext) = ext {
			if let Some(ref managed_files_path) = self.managed_files_path {
				// TODO: there's a race condition if we delete and insert a file at the same time, where we set up the file, delete the file, delete the record, then
				// insert the record, so we end up with a dangling record.
				let id = match id {
					ID::File(id) => id,
					_ => unreachable!(),
				};
				let path = path_for_file(managed_files_path, id, &ext);
				tokio::fs::remove_file(&path)
					.await
					.with_context(|| format!("While deleting file {:?}", path))?;
			} else {
				warn!("Deleating file ID {} that was stored externally without the external directory set up.", id);
			}
		}

		Ok(())
	}
}

struct BlobWriter {
	parent: Pin<Arc<SqliteNode>>,
	metadata: Metadata,
	f: Vec<u8>,
	hasher: ArcnetDigest,
}
#[async_trait]
impl NodeWriteTrait for BlobWriter {
	async fn finish(self: Pin<Box<Self>>) -> anyhow::Result<FinishedFile> {
		let this = Pin::into_inner(self);
		let (id, _) = this.hasher.finish(&this.metadata);

		let (send, recv) = oneshot::channel();
		this.parent
			.db_send
			.send(DbMessage::PushFile {
				id,
				metadata: this.metadata,
				src: FileSrc::Blob {
					contents: this.f,
					size: 0,
					range: 0..0,
				},
				response: send,
			})
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		recv.await
			.context("DB task exited unexpectedly")
			.and_then(|e| e)
	}
}
impl tokio::io::AsyncWrite for BlobWriter {
	fn poll_write(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
		buf: &[u8],
	) -> Poll<Result<usize, std::io::Error>> {
		match Pin::new(&mut self.f).poll_write(cx, buf) {
			Poll::Ready(Ok(n)) => {
				self.hasher.update(&buf[0..n]);
				Poll::Ready(Ok(n))
			}
			e => e,
		}
	}

	fn poll_flush(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Pin::new(&mut self.f).poll_flush(cx)
	}

	fn poll_shutdown(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Pin::new(&mut self.f).poll_shutdown(cx)
	}
}

struct FileWriter {
	parent: Pin<Arc<SqliteNode>>,
	metadata: Metadata,
	hasher: ArcnetDigest,
	f: InferWrite<BufWriter<tempfile::TempFile>>,
}
#[async_trait]
impl NodeWriteTrait for FileWriter {
	async fn finish(self: Pin<Box<Self>>) -> anyhow::Result<FinishedFile> {
		let this = Pin::into_inner(self);
		let (id, _) = this.hasher.finish(&this.metadata);

		let inferred_type = this.f.infer();
		let mut f = this.f.into_inner();

		f.flush().await?;
		let f = f.into_inner();

		let ext = inferred_type.map(|v| v.extension()).unwrap_or("bin");

		let path = path_for_file(this.parent.managed_files_path.as_ref().unwrap(), &id, ext);

		if path.exists() {
			let _ = f.delete().await;
		} else {
			if let Some(parent) = path.parent() {
				tokio::fs::create_dir_all(parent)
					.await
					.with_context(|| format!("While creatind directories up to {:?}", path))?;
			}
			f.commit(&path)
				.await
				.with_context(|| format!("While moving finished file to {:?}", path))?;
		}

		let (send, recv) = oneshot::channel();
		this.parent
			.db_send
			.send(DbMessage::PushFile {
				id,
				metadata: this.metadata,
				src: FileSrc::File { ext: ext.into() },
				response: send,
			})
			.await
			.map_err(|_| anyhow::anyhow!("DB task exited unexpectedly"))?;
		recv.await
			.context("DB task exited unexpectedly")
			.and_then(|e| e)
	}
}
impl tokio::io::AsyncWrite for FileWriter {
	fn poll_write(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
		buf: &[u8],
	) -> Poll<Result<usize, std::io::Error>> {
		match Pin::new(&mut self.f).poll_write(cx, buf) {
			Poll::Ready(Ok(n)) => {
				self.hasher.update(&buf[0..n]);
				Poll::Ready(Ok(n))
			}
			e => e,
		}
	}

	fn poll_flush(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Pin::new(&mut self.f).poll_flush(cx)
	}

	fn poll_shutdown(
		mut self: Pin<&mut Self>,
		cx: &mut Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Pin::new(&mut self.f).poll_shutdown(cx)
	}
}

enum DbMessage {
	GetFile(
		FileID,
		GetRange,
		oneshot::Sender<anyhow::Result<Option<(Metadata, FileSrc)>>>,
	),
	PushFile {
		id: FileID,
		metadata: Metadata,
		src: FileSrc,
		response: oneshot::Sender<anyhow::Result<FinishedFile>>,
	},
	GetRef(RefID, oneshot::Sender<anyhow::Result<Option<Ref>>>),
	SetRef {
		id: RefID,
		target: FileID,
		expected_existing: Option<FileID>,
		response: oneshot::Sender<Result<Ref, SetReferenceError>>,
	},
	Delete(ID, oneshot::Sender<anyhow::Result<Option<String>>>),
	StoredIDs(oneshot::Sender<anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>>>),
}

enum FileSrc {
	Blob {
		contents: Vec<u8>,
		size: usize,
		range: std::ops::Range<usize>,
	},
	File {
		ext: String,
	},
}

fn db_task(
	mut db: rusqlite::Connection,
	mut queue: mpsc::Receiver<DbMessage>,
	node_path: NodePathBuf,
) {
	while let Some(msg) = queue.blocking_recv() {
		// TODO: refactor and_then chains when try{} is stable
		match msg {
			DbMessage::GetFile(id, range, response) => {
				let res = AnyhowErrOrNoRows::to_option(db.query_row_and_then(
					r"SELECT links, contents, ext FROM files WHERE id = ?",
					params![&id.to_string()],
					|r| -> Result<_, AnyhowErrOrNoRows> {
						let metadata = Metadata::new(
							Metadata::links_from_comma_separated_str(r.get_ref(0)?.as_str()?)
								.map_err(|e| anyhow::anyhow!(e))?,
						);

						let blob = r.get_ref(1)?.as_blob_or_null()?;
						let ext = r.get_ref(2)?.as_str_or_null()?;

						let src = match (blob, ext) {
							(Some(blob), None) => {
								let size = blob.len();
								let range = range.clamped_usize(blob.len());
								let contents = Vec::from(&blob[range.clone()]);
								FileSrc::Blob {
									contents,
									size,
									range,
								}
							}
							(None, Some(ext)) => FileSrc::File { ext: ext.into() },
							_ => {
								return Err(anyhow::anyhow!("Invalid files row in database").into())
							}
						};

						Ok((metadata, src))
					},
				))
				.with_context(|| format!("While getting file {} from db", id));
				let _ = response.send(res);
			}
			DbMessage::PushFile {
				id,
				metadata,
				src,
				response,
			} => {
				let links_str = Metadata::links_to_comma_separated_str(&metadata.links);
				let res = match src {
					FileSrc::Blob { contents, .. } => db.execute(
						"INSERT OR REPLACE INTO files(id, links, contents) VALUES (?,?,?)",
						params![&id.to_string(), &links_str, &contents],
					),
					FileSrc::File { ext } => db.execute(
						"INSERT OR REPLACE INTO files(id, links, ext) VALUES (?,?,?)",
						params![&id.to_string(), &links_str, &ext],
					),
				}
				.with_context(|| format!("While inserting file {} into the db", id));
				let _ = response.send(res.map(|_| FinishedFile {
					id,
					node_path: node_path.clone(),
				}));
			}
			DbMessage::GetRef(id, response) => {
				let res = db.query_row_and_then(
					"SELECT target FROM refs WHERE id = ?",
					params![&id.to_string()],
					|row| -> Result<_, AnyhowErrOrNoRows> {
						row.get_ref(0)?
							.as_str()?
							.parse::<FileID>()
							.map_err(|e| anyhow::anyhow!(e).context("While parsing file ID").into())
					},
				);
				let _ = response.send(match res {
					Ok(v) => Ok(Some(Ref {
						target: v,
						node_path: node_path.clone(),
					})),
					Err(AnyhowErrOrNoRows::NoRow) => Ok(None),
					Err(AnyhowErrOrNoRows::Error(e)) => {
						Err(e.context(format!("While getting ref {}", id)))
					}
				});
			}
			DbMessage::SetRef {
				id,
				target,
				expected_existing,
				response,
			} => {
				let id_txt = id.to_string();
				let target_txt = target.to_string();
				if let Some(expected_existing) = expected_existing {
					let res = db
						.transaction()
						.context("While starting DB transaction")
						.and_then(|tx| {
							tx.execute(
								"UPDATE refs SET target = ? WHERE id = ? and target = ?",
								params![&target_txt, &id_txt, &expected_existing.to_string()],
							)
							.with_context(|| format!("While updating ref {:?}", id))
							.map(|_| tx)
						})
						.and_then(|tx| {
							AnyhowErrOrNoRows::to_option(tx.query_row_and_then(
								"SELECT target FROM refs WHERE id = ?",
								params![&id_txt],
								|row| -> Result<_, AnyhowErrOrNoRows> {
									row.get_ref(0)?.as_str()?.parse::<FileID>().map_err(|e| {
										anyhow::anyhow!(e).context("While parsing file ID").into()
									})
								},
							))
							.map(move |new_target| (tx, new_target))
						})
						.and_then(|(tx, new_target)| {
							tx.commit()
								.context("While committing DB transaction")
								.map(move |()| new_target)
						})
						.map_err(SetReferenceError::Error)
						.and_then(|new_target| {
							if new_target.as_ref() == Some(&target) {
								Ok(Ref {
									target: new_target.unwrap(),
									node_path: node_path.clone(),
								})
							} else {
								Err(SetReferenceError::DidNotMatch {
									existing: new_target.map(|target| Ref {
										target,
										node_path: node_path.clone(),
									}),
								})
							}
						});
					let _ = response.send(res);
				} else {
					let res = db
						.execute(
							"INSERT OR REPLACE INTO refs (id, target) VALUES (?,?)",
							params![&id_txt, &target_txt],
						)
						.with_context(|| format!("While setting ref {:?}", id))
						.map(|_| Ref {
							target: target.clone(),
							node_path: node_path.clone(),
						})
						.map_err(SetReferenceError::Error);
					let _ = response.send(res);
				}
			}
			DbMessage::Delete(id, response) => {
				let res = match id {
					ID::File(id) => db
						.transaction()
						.map_err(|e| anyhow::anyhow!(e))
						.and_then(|tx| {
							tx.query_row(
								r"SELECT ext FROM files WHERE id = ?",
								params![&id.to_string()],
								|row| row.get::<_, Option<String>>(0),
							)
							.optional()
							.map(|ext| (tx, ext.flatten()))
							.map_err(Into::into)
						})
						.and_then(|(tx, ext)| {
							tx.execute(r"DELETE FROM files WHERE id = ?", params![&id.to_string()])
								.map(|_| (tx, ext))
								.map_err(Into::into)
						})
						.and_then(|(tx, ext)| tx.commit().map(|()| ext).map_err(Into::into)),
					ID::Ref(id) => db
						.execute("DELETE FROM refs WHERE id = ?", params![&id.to_string()])
						.map(|_| None)
						.map_err(Into::into),
				};
				let _ = response.send(res);
			}
			DbMessage::StoredIDs(response) => {
				let mut stmt = match db.prepare_cached(
					r"SELECT 'file', id FROM files UNION ALL SELECT 'ref', id FROM refs",
				) {
					Ok(s) => s,
					Err(e) => {
						let _ = response.send(Err(e.into()));
						continue;
					}
				};
				let mut query = match stmt.query(params![]) {
					Ok(s) => s,
					Err(e) => {
						let _ = response.send(Err(e.into()));
						continue;
					}
				};

				let (id_send, mut id_recv) = mpsc::channel(16);
				let _ = response.send(Ok(Box::pin(futures::stream::poll_fn(move |cx| {
					id_recv.poll_recv(cx)
				}))));

				loop {
					match query.next() {
						Ok(Some(row)) => {
							let res = row
								.get_ref(0)
								.map_err(|e| anyhow::anyhow!(e))
								.and_then(|v| v.as_str().map_err(Into::into))
								.and_then(|typ| match typ {
									"file" => row
										.get_ref(1)?
										.as_str()?
										.parse::<FileID>()
										.map(ID::from)
										.map_err(Into::into),
									"ref" => row
										.get_ref(1)?
										.as_str()?
										.parse::<RefID>()
										.map(ID::from)
										.map_err(Into::into),
									_ => unreachable!(),
								})
								.map(|id| StoredID {
									id,
									node_path: node_path.clone(),
								});
							if let Err(_) = id_send.blocking_send(res) {
								break;
							}
						}
						Ok(None) => {
							break;
						}
						Err(e) => {
							let _ = id_send.blocking_send(Err(anyhow::anyhow!(e)));
							break;
						}
					}
				}
			}
		}
	}
}

fn path_for_file(base_dir: &Path, id: &FileID, ext: &str) -> PathBuf {
	let a = hex::encode(&id[0..1]);
	let b = hex::encode(&id[1..2]);
	let full = hex::encode(&id[..]);
	let mut path = PathBuf::from(base_dir);
	path.push(a);
	path.push(b);
	path.push(full);
	path.set_extension(ext);
	path
}

pub struct SqliteNodeLoader;
#[async_trait]
impl NodeLoader for SqliteNodeLoader {
	async fn create(_: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self)
	}

	fn name(&self) -> &str {
		"sqlite"
	}

	async fn load(
		&self,
		path: NodePathBuf,
		cfg: toml::value::Value,
		_: &NodeTreeLoader,
	) -> anyhow::Result<Node> {
		let cfg: SqliteNodeCfg = cfg
			.try_into()
			.with_context(|| format!("While parsing sqlite node config for path {:?}", path))?;

		let db_path = cfg.db_path
			.or_else(|| cfg.managed_files_path.clone().map(|p| p.join("index.sqlite")))
			.ok_or_else(|| anyhow::anyhow!("Must specify one or both of `db_path` or `managed_files_path` for `sqlite` node config"))?;

		SqliteNode::open(path, &db_path, cfg.managed_files_path)
			.await
			.map(|v| Arc::pin(v) as _)
	}
}

/// One or both of `db_path` or `managed_files_path` must be specified.
///
/// If only `db_path` is specified, files are stored in the database. This works well for small files and refs, but will not scale to large files.
/// Files will be buffered in memory when read and written.
///
/// If `managed_files_path` is specified, files are instead placed in subdirectories of the specified directory, managed by the node.
/// This works well for larger files. Files are streamed to and from disk. If `db_path` is also specified, the index database will be
/// accessed from the specified path, otherwise it will default to a file in the `managed_files_path`.
#[derive(Deserialize, Clone, Debug)]
pub struct SqliteNodeCfg {
	#[serde(default)]
	pub db_path: Option<PathBuf>,
	#[serde(default)]
	pub managed_files_path: Option<PathBuf>,
}

#[cfg(test)]
mod tests {
	use super::*;
	crate::nodes::node_tests!(sqlite_internal, |path| {
		let tempdir = ::tempfile::tempdir().unwrap();
		let node = Arc::pin(
			SqliteNode::open(path, &tempdir.path().join("db.sqlite"), None)
				.await
				.unwrap(),
		);
		(
			node,
			Box::pin(async move {
				std::mem::drop(tempdir);
			}),
		)
	});

	crate::nodes::node_tests!(sqlite_external, |path| {
		let tempdir = ::tempfile::tempdir().unwrap();
		let node = Arc::pin(
			SqliteNode::open(
				path,
				&tempdir.path().join("db.sqlite"),
				Some(tempdir.path().into()),
			)
			.await
			.unwrap(),
		);
		(
			node,
			Box::pin(async move {
				std::mem::drop(tempdir);
			}),
		)
	});
}

enum AnyhowErrOrNoRows {
	NoRow,
	Error(anyhow::Error),
}
impl AnyhowErrOrNoRows {
	pub fn to_option<T>(res: Result<T, Self>) -> anyhow::Result<Option<T>> {
		match res {
			Ok(v) => Ok(Some(v)),
			Err(Self::NoRow) => Ok(None),
			Err(Self::Error(e)) => Err(e),
		}
	}
}
impl From<rusqlite::Error> for AnyhowErrOrNoRows {
	fn from(v: rusqlite::Error) -> Self {
		match v {
			rusqlite::Error::QueryReturnedNoRows => Self::NoRow,
			v => Self::Error(anyhow::anyhow!(v)),
		}
	}
}
impl From<anyhow::Error> for AnyhowErrOrNoRows {
	fn from(v: anyhow::Error) -> Self {
		Self::Error(v)
	}
}
impl From<rusqlite::types::FromSqlError> for AnyhowErrOrNoRows {
	fn from(v: rusqlite::types::FromSqlError) -> Self {
		Self::Error(anyhow::anyhow!(v))
	}
}
