//! In-memory storage

use std::{
	pin::Pin,
	sync::Arc,
	task::Poll,
};

use async_trait::async_trait;
use dashmap::DashMap;
use digest::Digest;
use futures::stream::BoxStream;

use super::util::BasicNodeReader;
use crate::{
	cfg_loader::{
		NodeLoader,
		NodeTreeLoader,
	},
	digest::ArcnetDigest,
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::{
		FinishedFile,
		GetRange,
		Node,
		NodeReader,
		NodeTrait,
		NodeWriteTrait,
		NodeWriter,
		Ref,
		SetReferenceError,
		StoredID,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
};

/// In-memory store.
///
/// Contents are gone once this is dropped. Contains no subnodes.
///
/// Mostly useful for testing and caching.
pub struct MemNode {
	path: NodePathBuf,
	files: DashMap<FileID, (bytes::Bytes, Metadata)>,
	refs: DashMap<RefID, FileID>,
}
impl MemNode {
	/// Creates a new empty memory store
	pub fn new(path: NodePathBuf) -> Self {
		Self {
			path,
			files: DashMap::new(),
			refs: DashMap::new(),
		}
	}
}
#[async_trait]
impl NodeTrait for MemNode {
	fn path(&self) -> &NodePath {
		&self.path
	}

	async fn sub_node(&self, _name: &str) -> anyhow::Result<Option<Node>> {
		Ok(None)
	}

	async fn sub_nodes(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		Ok(Box::pin(futures::stream::empty()))
	}

	async fn get_file(
		self: Pin<Arc<Self>>,
		id: &FileID,
		range: &GetRange,
	) -> anyhow::Result<Option<NodeReader>> {
		Ok(self
			.files
			.get(id)
			.map(|e| {
				let range = range.clamped_usize(e.0.len());
				let size = e.0.len();
				let buf = e.0.slice(range.clone());
				let md = e.1.clone();
				(buf, md, size, range)
			})
			.map(move |(buf, metadata, size, range)| {
				let f = BasicNodeReader {
					reader: std::io::Cursor::new(buf),
					id: id.clone(),
					metadata,
					node_path: self.path.clone(),
					range: range.start as u64..range.end as u64,
					size: size as u64,
				};
				Box::pin(f) as _
			}))
	}

	async fn push_file(self: Pin<Arc<Self>>, meta: Metadata) -> anyhow::Result<NodeWriter> {
		Ok(Box::pin(MemWriteFile {
			parent: self,
			dest: bytes::BytesMut::new(),
			hasher: ArcnetDigest::new(),
			meta,
		}) as _)
	}

	async fn delete(&self, id: &ID) -> anyhow::Result<()> {
		match id {
			ID::File(file_id) => {
				self.files.remove(file_id);
			}
			ID::Ref(ref_id) => {
				self.refs.remove(ref_id);
			}
		}
		Ok(())
	}

	async fn get_ref(&self, id: &RefID) -> anyhow::Result<Option<Ref>> {
		Ok(self.refs.get(id).map(|r| Ref {
			target: r.clone(),
			node_path: self.path.clone(),
		}))
	}

	async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		expected_existing: Option<&FileID>,
	) -> anyhow::Result<Ref, SetReferenceError> {
		match self.refs.entry(id.clone()) {
			dashmap::mapref::entry::Entry::Occupied(mut o) => {
				if expected_existing.map(|ee| o.get() == ee).unwrap_or(true) {
					*o.get_mut() = target.clone();
					Ok(Ref {
						target: target.clone(),
						node_path: self.path.clone(),
					})
				} else {
					Err(SetReferenceError::DidNotMatch {
						existing: Some(Ref {
							target: o.get().clone(),
							node_path: self.path.clone(),
						}),
					})
				}
			}
			dashmap::mapref::entry::Entry::Vacant(v) => {
				if expected_existing.is_some() {
					Err(SetReferenceError::DidNotMatch { existing: None })
				} else {
					v.insert(target.clone());
					Ok(Ref {
						target: target.clone(),
						node_path: self.path.clone(),
					})
				}
			}
		}
	}

	async fn stored_ids(
		self: Pin<Arc<Self>>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		// Copy keys to avoid deadlocking if accessed from the same thread.
		let path = self.path.clone();
		let v = self
			.files
			.iter()
			.map(|e| e.key().clone().into())
			.chain(self.refs.iter().map(|e| e.key().clone().into()))
			.map(|id| StoredID {
				id,
				node_path: path.clone(),
			})
			.collect::<Vec<_>>();
		Ok(Box::pin(futures::stream::iter(v.into_iter().map(Ok))))
	}
}

pub struct MemNodeLoader;
#[async_trait]
impl NodeLoader for MemNodeLoader {
	async fn create(_: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self)
	}

	fn name(&self) -> &str {
		"mem"
	}

	async fn load(
		&self,
		path: NodePathBuf,
		_: toml::value::Value,
		_: &NodeTreeLoader,
	) -> anyhow::Result<Node> {
		Ok(Arc::pin(MemNode::new(path)))
	}
}

struct MemWriteFile {
	parent: Pin<Arc<MemNode>>,
	dest: bytes::BytesMut,
	hasher: ArcnetDigest,
	meta: Metadata,
}
#[async_trait]
impl NodeWriteTrait for MemWriteFile {
	async fn finish(self: Pin<Box<Self>>) -> anyhow::Result<FinishedFile> {
		let MemWriteFile {
			parent,
			dest,
			hasher,
			meta,
		} = *Pin::into_inner(self);
		let (id, _) = hasher.finish(&meta);
		parent.files.entry(id.clone()).or_insert_with(|| {
			let v = dest.freeze();
			(v, meta)
		});
		Ok(FinishedFile {
			id,
			node_path: parent.path.clone(),
		})
	}
}
impl tokio::io::AsyncWrite for MemWriteFile {
	fn poll_write(
		self: std::pin::Pin<&mut Self>,
		_: &mut std::task::Context<'_>,
		buf: &[u8],
	) -> Poll<Result<usize, std::io::Error>> {
		let this = self.get_mut();
		this.dest.extend_from_slice(buf);
		this.hasher.update(buf);
		Poll::Ready(Ok(buf.len()))
	}

	fn poll_flush(
		self: std::pin::Pin<&mut Self>,
		_: &mut std::task::Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Poll::Ready(Ok(()))
	}

	fn poll_shutdown(
		self: std::pin::Pin<&mut Self>,
		_: &mut std::task::Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		Poll::Ready(Ok(()))
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	crate::nodes::node_tests!(mem, |path| (
		Arc::pin(MemNode::new(path)),
		Box::pin(async {})
	));
}
