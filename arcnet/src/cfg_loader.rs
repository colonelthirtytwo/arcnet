//! Loading a node tree from a configuration file.

use std::{
	collections::HashMap,
	path::Path,
};

use anyhow::Context;
use async_trait::async_trait;

use crate::{
	interface::StoreInterface,
	nodes::Node,
	path::NodePathBuf,
};

/// TOML configuration loader
///
/// Provides a registry of different types of storage nodes, and helps recursively parse the node tree.
///
/// If you just want to load a TOML, check out [`NodeTreeLoader::load_file`] and [`NodeTreeLoader::load_bytes`].
/// The more low-level functions are only needed if you plan to register your own or customize what types of
/// storage nodes are available.
///
/// If you just want to connect to a single remote node (ex. your root node is a quic_client or something), consider
/// using [`StoreInterface::new`](crate::interface::StoreInterface::new) instead.
pub struct NodeTreeLoader {
	cfg_root: toml::Value,
	loaders: HashMap<String, Box<dyn NodeLoader>>,
}
impl NodeTreeLoader {
	/// Prepares a configuration loader, without adding the built-in node types.
	pub fn prepare_no_builtins(cfg_root: toml::Value) -> Self {
		Self {
			loaders: HashMap::new(),
			cfg_root,
		}
	}

	/// Prepares a configuration loader with the built-in node types.
	///
	/// The built-in noded types are influenced by what features the library has enabled.
	pub async fn prepare(cfg_root: toml::Value) -> anyhow::Result<Self> {
		let mut this = Self::prepare_no_builtins(cfg_root);
		this.register::<crate::nodes::mem::MemNodeLoader>().await?;
		this.register::<crate::nodes::collection::CollectionNodeLoader>()
			.await?;
		this.register::<crate::nodes::fs_indexer::FSIndexerNodeLoader>()
			.await?;

		#[cfg(feature = "node_sqlite")]
		this.register::<crate::nodes::sqlite::SqliteNodeLoader>()
			.await?;
		#[cfg(feature = "node_quic_client")]
		this.register::<crate::nodes::quic_client::QuicClientNodeLoader>()
			.await?;

		Ok(this)
	}

	/// Registers a custom storage node loader.
	pub async fn register<L: NodeLoader + 'static>(&mut self) -> anyhow::Result<()> {
		let loader = L::create(&self.cfg_root)
			.await
			.with_context(|| format!("While creating loader {}", std::any::type_name::<L>()))?;
		let name = String::from(loader.name());
		let loader = Box::new(loader) as Box<dyn NodeLoader>;

		assert!(
			!self.loaders.contains_key(&name),
			"Node loader name {:?} already exists",
			name
		);
		self.loaders.insert(name, loader);
		Ok(())
	}

	/// Creates a subnode from a config value.
	///
	/// This should only be called by a node loader implementation. TODO: move to a new type to enforce this.
	pub async fn create_from_cfg_value(
		&self,
		path: NodePathBuf,
		cfg: toml::value::Value,
	) -> anyhow::Result<Node> {
		let mcfg = cfg
			.as_table()
			.ok_or_else(|| anyhow::anyhow!("Expected node definition table"))?;
		let typ = mcfg
			.get("type")
			.ok_or_else(|| anyhow::anyhow!("Node definition table does not have a 'type' enty."))?;
		let typ = typ.as_str().ok_or_else(|| {
			anyhow::anyhow!("Node definition table 'type' entry is not a string.")
		})?;
		let loader = self
			.loaders
			.get(typ)
			.ok_or_else(|| anyhow::anyhow!("Unrecognized node type: {:?}", typ))?;
		loader.load(path, cfg, self).await
	}

	/// Loads the node tree.
	pub async fn load(self) -> anyhow::Result<StoreInterface> {
		let root_cfg = self
			.cfg_root
			.get("root")
			.ok_or_else(|| anyhow::anyhow!("No `root` item in toml config"))?;
		let node = self
			.create_from_cfg_value(NodePathBuf::root(), root_cfg.clone())
			.await?;
		Ok(StoreInterface::new(node))
	}

	/// Loads a config file via the FS, using the built-in node loaders.
	///
	/// See [`Self::prepare`].
	pub async fn load_file(path: &Path) -> anyhow::Result<StoreInterface> {
		let bytes = tokio::fs::read(path)
			.await
			.with_context(|| format!("While reading config file {:?}", path))?;
		let cfg = toml::from_slice(&bytes)
			.with_context(|| format!("While parsing config file {:?}", path))?;
		Self::prepare(cfg).await?.load().await
	}

	/// Loads a config file from a byte slice.
	///
	/// See [`Self::prepare`].
	pub async fn load_bytes(bytes: &[u8]) -> anyhow::Result<StoreInterface> {
		let cfg = toml::from_slice(&bytes).context("While parsing config file")?;
		Self::prepare(cfg).await?.load().await
	}
}

/// Node loader interface, that parses and transforms global and node options to an actual node handle.
#[async_trait]
pub trait NodeLoader: Send + Sync {
	/// Creates the node loader. The node loader may fetch global options from the root of the config.
	async fn create(root_cfg: &toml::value::Value) -> anyhow::Result<Self>
	where
		Self: Sized;

	/// Gets the node type name. Should be snake_case.
	fn name(&self) -> &str;

	/// Loads a node from a node's config.
	async fn load(
		&self,
		path: NodePathBuf,
		cfg: toml::value::Value,
		tree_loader: &NodeTreeLoader,
	) -> anyhow::Result<Node>;
}
