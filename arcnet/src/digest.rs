use digest::Digest;

use crate::{
	identifiers::FileID,
	metadata::Metadata,
};

/// Hashes file contents, generating their [`FileID`].
///
/// Stores should use this to generate a file ID from the file's contents and metadata, to ensure
/// that the returned file ID is the same across backends.
///
/// This uses [`digest::Digest`] interface for compatibility with other digest interfaces, however, only link-less file IDs
/// can be generated with it. To generate IDs needed for files with non-default metadata, use [`ArcnetDigest::finish`].
///
/// This also implements [`std::io::Write`] and [`tokio::io::AsyncWrite`]: writes update the hash, and return `Ready` immediately.
#[derive(Clone)]
pub struct ArcnetDigest {
	hash: sha3::Sha3_256,
	size: u64,
}
impl digest::OutputSizeUser for ArcnetDigest {
	type OutputSize = <sha3::Sha3_256 as digest::OutputSizeUser>::OutputSize;
}
impl digest::Update for ArcnetDigest {
	fn update(&mut self, data: &[u8]) {
		digest::Digest::update(self, data)
	}
}
impl digest::FixedOutput for ArcnetDigest {
	fn finalize_into(self, out: &mut digest::Output<Self>) {
		digest::Digest::finalize_into(self, out)
	}
}
impl digest::Reset for ArcnetDigest {
	fn reset(&mut self) {
		digest::Digest::reset(self)
	}
}
impl digest::FixedOutputReset for ArcnetDigest {
	fn finalize_into_reset(&mut self, out: &mut digest::Output<Self>) {
		digest::Digest::finalize_into_reset(self, out)
	}
}

impl ArcnetDigest {
	/// Number of bytes that has been fed into the digest so far.
	pub fn num_bytes_consumed(&self) -> u64 {
		self.size
	}

	/// Finishes hashing, returning the generated `FileID` and the number of bytes hashed.
	pub fn finish(mut self, md: &Metadata) -> (FileID, u64) {
		let size = self.size;
		let mut genarr = digest::Output::<Self>::default();
		self.finalize_into_reset_with_md(&mut genarr, md);
		let mut arr = [0u8; 32];
		arr.copy_from_slice(&genarr);

		(arr.into(), size)
	}

	pub fn from_bytes(bytes: &[u8], md: &Metadata) -> FileID {
		Self::new_with_prefix(bytes).finish(md).0
	}

	fn finalize_into_reset_with_md(&mut self, out: &mut digest::Output<Self>, metadata: &Metadata) {
		// Include size as a protection against length extensison attacks.
		// Not a big problem for SHA3 ATM but may be in the future
		self.hash.update(&self.size.to_le_bytes());
		rmp_serde::encode::write(&mut self.hash, metadata).unwrap();

		// rust doesn't consider digest::Output<Sha3_256> and digest::Output<Self> as the same type
		let arr = self.hash.finalize_reset();
		out.copy_from_slice(&arr);
		self.size = 0;
	}
}

impl digest::Digest for ArcnetDigest {
	/// Createw an `ArcnetDigest`
	fn new() -> Self {
		Self {
			hash: sha3::Sha3_256::new(),
			size: 0,
		}
	}

	fn new_with_prefix(data: impl AsRef<[u8]>) -> Self {
		Self {
			size: data.as_ref().len() as u64,
			hash: sha3::Sha3_256::new_with_prefix(data),
		}
	}

	fn update(&mut self, data: impl AsRef<[u8]>) {
		self.size += data.as_ref().len() as u64;
		self.hash.update(data);
	}

	fn chain_update(mut self, data: impl AsRef<[u8]>) -> Self {
		self.update(data);
		self
	}

	fn finalize(mut self) -> digest::Output<Self> {
		self.finalize_reset()
	}

	fn finalize_into(mut self, out: &mut digest::Output<Self>) {
		self.finalize_into_reset(out);
	}

	fn finalize_reset(&mut self) -> digest::Output<Self>
	where
		Self: digest::FixedOutputReset,
	{
		let mut arr = digest::Output::<Self>::default();
		self.finalize_into_reset(&mut arr);
		arr
	}

	fn finalize_into_reset(&mut self, out: &mut digest::Output<Self>)
	where
		Self: digest::FixedOutputReset,
	{
		self.finalize_into_reset_with_md(out, &Metadata::new_leaf())
	}

	fn reset(&mut self)
	where
		Self: digest::Reset,
	{
		self.hash.reset();
		self.size = 0;
	}

	fn output_size() -> usize {
		sha3::Sha3_256::output_size()
	}

	fn digest(data: impl AsRef<[u8]>) -> digest::Output<Self> {
		Self::new_with_prefix(data).finalize()
	}
}

impl std::io::Write for ArcnetDigest {
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
		self.update(buf);
		Ok(buf.len())
	}

	fn flush(&mut self) -> std::io::Result<()> {
		Ok(())
	}
}

impl tokio::io::AsyncWrite for ArcnetDigest {
	fn poll_write(
		mut self: std::pin::Pin<&mut Self>,
		_cx: &mut std::task::Context<'_>,
		buf: &[u8],
	) -> std::task::Poll<Result<usize, std::io::Error>> {
		self.update(buf);
		std::task::Poll::Ready(Ok(buf.len()))
	}

	fn poll_flush(
		self: std::pin::Pin<&mut Self>,
		_cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), std::io::Error>> {
		std::task::Poll::Ready(Ok(()))
	}

	fn poll_shutdown(
		self: std::pin::Pin<&mut Self>,
		_cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), std::io::Error>> {
		std::task::Poll::Ready(Ok(()))
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn hash_hello_world() {
		let mut digest = ArcnetDigest::new();
		digest.update(b"hello world!");
		let (id, len) = digest.finish(&Metadata::new_leaf());
		assert_eq!(len, 12);
		assert_eq!(
			id,
			"sha3:fa35d698efb6c2180698c4941c1dedf2c46bc8ddcc3bec00eb75dd6112194326"
				.parse()
				.unwrap()
		);

		let mut digest = ArcnetDigest::new();
		digest.update(b"goodbye world!");
		let (id, len) = digest.finish(&Metadata::new_leaf());
		assert_eq!(len, 14);
		assert_eq!(
			id,
			"sha3:ab4f75ee40019867a70bf04ad52d82a4816412deacfe233838ca5c70b965bbae"
				.parse()
				.unwrap()
		);
	}
}
