//! High-level interface for accessing a storage node tree.
//!
//! See [`StoreInterface`].

use std::task::Poll;

use anyhow::Context;
use digest::Digest;
use futures::stream::BoxStream;
use hashlink::LruCache;
use tokio::{
	io::{
		AsyncReadExt,
		AsyncWriteExt,
	},
	sync::Mutex,
};

use crate::{
	digest::ArcnetDigest,
	errors::SetReferenceError,
	identifiers::{
		track_id_serializations,
		FileID,
		RefID,
		ID,
	},
	metadata::Metadata,
	nodes::{
		FinishedFile,
		GetRange,
		Node,
		NodeReadTrait,
		NodeReader,
		NodeWriter,
		Ref,
		ResolvedRef,
		StoredID,
	},
	path::{
		NodePath,
		NodePathBuf,
	},
};

/// High-level interface for accessing a storage node tree.
///
/// More convenient than using a [`Node`] on its own.
pub struct StoreInterface {
	root: Node,
	nodes_by_path: Mutex<LruCache<NodePathBuf, Node>>,
}
impl StoreInterface {
	pub fn new(root: Node) -> Self {
		assert_eq!(root.path(), NodePath::ROOT);
		Self {
			root,
			nodes_by_path: Mutex::new(LruCache::new(32)),
		}
	}

	/// Gets the root node.
	///
	/// There's less convenience and protections when using the underlying nodes, so be careful.
	pub fn root(&self) -> &Node {
		&self.root
	}

	/// Gets a node by a node path.
	///
	/// There's less convenience and protections when using the underlying nodes, so be careful.
	pub async fn node_by_path(&self, path: &NodePath) -> anyhow::Result<Option<Node>> {
		{
			let mut lock = self.nodes_by_path.lock().await;
			if let Some(v) = lock.get(path) {
				return Ok(Some(v.clone()));
			}
		}

		let mut node = self.root.clone();
		for component in path.components() {
			node = match node.sub_node(component).await.with_context(|| {
				format!(
					"While getting subnode {:?} from node {:?}",
					component,
					node.path()
				)
			})? {
				Some(v) => v,
				None => return Ok(None),
			};
		}

		{
			let mut lock = self.nodes_by_path.lock().await;
			lock.insert(path.into(), node.clone());
		}

		Ok(Some(node))
	}

	async fn node_by_path_or_err(&self, path: &NodePath) -> anyhow::Result<Node> {
		self.node_by_path(path)
			.await
			.with_context(|| format!("While getting node {:?}", path))?
			.ok_or_else(|| anyhow::anyhow!(crate::errors::NoSuchNode(path.into())))
	}

	/// Reads a file with a certain id.
	///
	/// If path is `None`, queries the root node, otherwise queries the specified node.
	pub async fn get_file(
		&self,
		id: &FileID,
		options: GetFileOptions<'_>,
	) -> anyhow::Result<Option<NodeReader>> {
		let path = options.node_path.unwrap_or(NodePath::ROOT);
		let node = self.node_by_path_or_err(path).await?;

		match node.get_file(id, &options.range).await {
			Ok(Some(f)) => {
				let f = Self::check_returned_file(path, id, f, &options)?;
				Ok(Some(f))
			}
			Ok(None) => Ok(None),
			Err(e) => Err(e.context(format!("While getting id {} from node {:?}", id, path))),
		}
	}

	/// Reads a file with a certain ID and deserializes it with messagepack.
	pub async fn get_msgpack<D: serde::de::DeserializeOwned, I: Into<ID> + Clone>(
		&self,
		id: &I,
		options: GetFileOptions<'_>,
	) -> anyhow::Result<Option<D>> {
		match self.get(id, options).await? {
			Some(mut f) => {
				let mut buf = vec![];
				f.read_to_end(&mut buf).await?;
				let v = rmp_serde::from_slice(&buf).context("While deserializing msgpack")?;
				Ok(Some(v))
			}
			None => Ok(None),
		}
	}

	/// Reads a reference.
	///
	/// If path is `None`, queries the root node, otherwise queries the specified node.
	pub async fn get_ref(
		&self,
		id: &RefID,
		path: Option<&NodePath>,
	) -> anyhow::Result<Option<Ref>> {
		let path = path.unwrap_or(NodePath::ROOT);
		let node = self.node_by_path_or_err(path).await?;

		node.get_ref(id)
			.await
			.with_context(|| format!("While getting id {} from node {:?}", id, path))
	}

	/// Reads a reference and reads its target.
	///
	/// If path is `None`, queries the root node, otherwise queries the specified node.
	pub async fn get_resolved_ref(
		&self,
		id: &RefID,
		options: GetFileOptions<'_>,
	) -> anyhow::Result<ResolvedRef> {
		let path = options.node_path.unwrap_or(NodePath::ROOT);
		let node = self.node_by_path_or_err(path).await?;

		match node.get_resolved_ref(id, &options.range).await {
			Ok(ResolvedRef::ResolvedRef { ref_, file }) => {
				let f = Self::check_returned_file(path, id, file, &options)?;
				Ok(ResolvedRef::ResolvedRef { ref_, file: f })
			}
			Err(e) => Err(e.context(format!("While getting id {} from node {:?}", id, path))),
			other => other,
		}
	}

	/// Reads a file from either type of ID.
	///
	/// For File IDs, calls [`StoreInterface::get_file`]. For Ref IDs, calls [`StoreInterface::get_resolved_ref`] and
	/// [`ResolvedRef::to_opt_reader`]
	pub async fn get<I: Clone + Into<ID>>(
		&self,
		id: &I,
		options: GetFileOptions<'_>,
	) -> anyhow::Result<Option<NodeReader>> {
		match id.clone().into() {
			ID::File(id) => self.get_file(&id, options).await,
			ID::Ref(id) => self
				.get_resolved_ref(&id, options)
				.await
				.and_then(|resolved| resolved.to_opt_reader().map_err(Into::into)),
		}
	}

	pub async fn push_file(
		&self,
		meta: Metadata,
		path: Option<&NodePath>,
	) -> anyhow::Result<NodeWriter> {
		let path = path.unwrap_or(NodePath::ROOT);
		let node = self.node_by_path_or_err(path).await?;
		node.push_file(meta).await
	}

	pub async fn push_file_from_read<R: tokio::io::AsyncRead + Unpin>(
		&self,
		meta: Metadata,
		path: Option<&NodePath>,
		mut reader: R,
	) -> anyhow::Result<FinishedFile> {
		let mut f = self.push_file(meta, path).await?;
		tokio::io::copy(&mut reader, &mut f)
			.await
			.context("While writing to file")?;
		f.finish().await
	}

	pub async fn push_msgpack<T: serde::Serialize>(
		&self,
		path: Option<&NodePath>,
		value: &T,
	) -> anyhow::Result<FinishedFile> {
		let (res, ids) = track_id_serializations(|| {
			rmp_serde::to_vec_named(value).context("While serializing to msgpack")
		});
		let buffer = res.context("While serializing msgpack")?;
		let meta = Metadata::new(ids);
		let mut f = self.push_file(meta, path).await?;
		f.write_all(&buffer).await?;
		f.finish().await
	}

	pub async fn stored_ids(
		&self,
		path: Option<&NodePath>,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<StoredID>>> {
		let node = self
			.node_by_path_or_err(path.unwrap_or(NodePath::ROOT))
			.await?;
		node.stored_ids().await
	}

	pub async fn sub_nodes(
		&self,
		path: &NodePath,
	) -> anyhow::Result<BoxStream<'static, anyhow::Result<String>>> {
		let node = self.node_by_path_or_err(path).await?;
		node.sub_nodes().await
	}

	pub async fn set_ref(
		&self,
		id: &RefID,
		target: &FileID,
		path: Option<&NodePath>,
		expected_existing: Option<&FileID>,
	) -> Result<Ref, SetReferenceError> {
		let node = self
			.node_by_path_or_err(path.unwrap_or(NodePath::ROOT))
			.await?;
		node.set_ref(id, target, expected_existing).await
	}

	pub async fn delete(&self, id: &ID, path: &NodePath) -> anyhow::Result<()> {
		let node = self.node_by_path_or_err(path).await?;
		node.delete(id).await
	}

	fn check_returned_file<I: std::fmt::Display>(
		path: &NodePath,
		id: &I,
		f: NodeReader,
		options: &GetFileOptions<'_>,
	) -> anyhow::Result<NodeReader> {
		let size = f.size();
		let range = f.range();

		if options.range.clamped(size) != range {
			anyhow::bail!("Node at path {} responded to get id {} with range {:?} inappropriately, sending range {}-{} of {} bytes",
				path,
				id,
				options.range,
				range.start,
				range.end,
				size,
			);
		}

		if range.start == 0 && range.end == size {
			Ok(Box::pin(HashCheckingReader {
				inner: f,
				num_read: 0,
				hasher: Some(ArcnetDigest::new()),
			}))
		} else {
			Ok(f)
		}
	}
}

/// [`NodeReader`] wrapper that provides checking on the data.
///
/// Checks that the wrapped [`NodeReader`] to ensure that the data it
/// provides matches the file ID and metadata.
///
/// Only works on full files.
struct HashCheckingReader {
	inner: NodeReader,
	num_read: u64,
	hasher: Option<ArcnetDigest>,
}
impl NodeReadTrait for HashCheckingReader {
	fn id(&self) -> &FileID {
		self.inner.id()
	}

	fn metadata(&self) -> &crate::metadata::Metadata {
		self.inner.metadata()
	}

	fn size(&self) -> u64 {
		self.inner.size()
	}

	fn node_path(&self) -> &NodePath {
		self.inner.node_path()
	}

	fn range(&self) -> std::ops::Range<u64> {
		let range = self.inner.range();
		assert_eq!(range.start, 0, "Range changed unexpectedly");
		assert_eq!(range.end, self.size(), "Range or size changed unexpectedly");
		range
	}
}
impl tokio::io::AsyncRead for HashCheckingReader {
	fn poll_read(
		mut self: std::pin::Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		buf: &mut tokio::io::ReadBuf<'_>,
	) -> Poll<std::io::Result<()>> {
		let prev = buf.filled().len();
		match self.inner.as_mut().poll_read(cx, buf) {
			Poll::Ready(Ok(())) => {
				let read = &buf.filled()[prev..];
				self.num_read += read.len() as u64;
				if self.num_read > self.size() {
					return Poll::Ready(Err(
						std::io::Error::new(
							std::io::ErrorKind::InvalidInput,
							anyhow::anyhow!("Node continued reading data beyond the file's size. Expected {} bytes.", self.size())
						)
					));
				}

				if read.len() != 0 {
					let hasher = match self.hasher.as_mut() {
						Some(v) => v,
						None => {
							return Poll::Ready(Err(std::io::Error::new(
								std::io::ErrorKind::InvalidInput,
								anyhow::anyhow!("Node read data after sending EOF"),
							)))
						}
					};
					hasher.update(read)
				} else if let Some(hasher) = self.hasher.take() {
					let (data_id, _) = hasher.finish(&self.inner.metadata());
					if data_id != *self.id() {
						return Poll::Ready(Err(std::io::Error::new(
							std::io::ErrorKind::InvalidInput,
							anyhow::anyhow!("Node returned corrupt contents. Expected ID {} but data's hash is {}",
								self.id(),
								data_id
							)
						)));
					}
				}
				Poll::Ready(Ok(()))
			}
			e @ Poll::Ready(Err(_)) | e @ Poll::Pending => e,
		}
	}
}

/// Options for [`StoreInterface::get_file`] and related functions.
#[derive(Default, Debug, Clone)]
#[non_exhaustive]
pub struct GetFileOptions<'a> {
	pub node_path: Option<&'a NodePath>,
	pub range: GetRange,
}
impl<'a> GetFileOptions<'a> {
	pub fn node_path<P: Into<Option<&'a NodePath>>>(mut self, path: P) -> Self {
		self.node_path = path.into();
		self
	}

	pub fn range<R: Into<GetRange>>(mut self, range: R) -> Self {
		self.range = range.into();
		self
	}
}
