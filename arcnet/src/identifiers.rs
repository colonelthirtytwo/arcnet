//! Identifier types

use std::{
	cell::RefCell,
	collections::BTreeSet,
	ops::Deref,
};

use thiserror::Error;

const PATH_ENCODE_SET: percent_encoding::AsciiSet = percent_encoding::CONTROLS
	.add(b' ')
	.add(b'"')
	.add(b'#')
	.add(b'<')
	.add(b'>')
	.add(b'?')
	.add(b'`')
	.add(b'{')
	.add(b'}')
	// Addition to spec path escaping, to make comma separation work
	.add(b',');

/// ID parse error
#[derive(Debug, Clone, Error)]
#[non_exhaustive]
pub enum FromStrError {
	#[error("no colon found in uri")]
	NoColon,
	#[error("unrecognized scheme")]
	UnrecognizedScheme,
	#[error("invalid uri data: {0}")]
	InvalidHexData(#[from] hex::FromHexError),
	#[error("invalid utf-8 data: {0}")]
	InvalidUtf8Data(#[from] std::str::Utf8Error),
}

/// ID of a file. Currently a SHA3_256 hash.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FileID([u8; 32]);
impl FileID {
	/// Formats the ID as a urlencoded string.
	pub fn to_url_string(&self) -> String {
		format!("sha3%3A{}", &hex::encode(&self.0))
	}

	/// Parses the ID from a urlencoded string.
	pub fn parse_url_string(s: &str) -> Result<Self, FromStrError> {
		let (scheme, data) = s
			.split_once("%3A")
			.or(s.split_once("%3a"))
			.ok_or(FromStrError::NoColon)?;
		if scheme != "sha3" {
			return Err(FromStrError::UnrecognizedScheme);
		}
		let mut buf = [0u8; 32];
		hex::decode_to_slice(data, &mut buf)?;
		Ok(Self(buf))
	}
}
impl std::fmt::Display for FileID {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str("sha3:")?;
		f.write_str(&hex::encode(&self.0))
	}
}
impl std::str::FromStr for FileID {
	type Err = FromStrError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (scheme, data) = s.split_once(':').ok_or(FromStrError::NoColon)?;
		if scheme != "sha3" {
			return Err(FromStrError::UnrecognizedScheme);
		}
		let mut buf = [0u8; 32];
		hex::decode_to_slice(data, &mut buf)?;
		Ok(Self(buf))
	}
}
impl From<[u8; 32]> for FileID {
	fn from(v: [u8; 32]) -> Self {
		Self(v)
	}
}
impl<'a> TryFrom<&'a [u8]> for FileID {
	type Error = <[u8; 32] as TryFrom<&'a [u8]>>::Error;

	fn try_from(value: &'a [u8]) -> Result<Self, Self::Error> {
		<[u8; 32]>::try_from(value).map(Self)
	}
}
impl Deref for FileID {
	type Target = [u8; 32];

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl<'de> serde::Deserialize<'de> for FileID {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error;
		let s = String::deserialize(deserializer)?;
		s.parse::<Self>().map_err(D::Error::custom)
	}
}
impl std::fmt::Debug for FileID {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let hex = hex::encode(&self.0);
		f.debug_tuple("FileID").field(&hex).finish()
	}
}

/// ID of a reference.
///
/// The reference itself is an arbitrary utf-8 string
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RefID(String);
impl RefID {
	/// Formats the ID as a urlencoded string.
	pub fn to_url_string(&self) -> String {
		format!(
			"ref%3A{}",
			percent_encoding::utf8_percent_encode(&self.0, &PATH_ENCODE_SET)
		)
	}

	/// Parses the ID from a urlencoded string.
	pub fn parse_url_string(s: &str) -> Result<Self, FromStrError> {
		let (scheme, data) = s
			.split_once("%3A")
			.or(s.split_once("%3a"))
			.ok_or(FromStrError::NoColon)?;
		if scheme != "ref" {
			return Err(FromStrError::UnrecognizedScheme);
		}
		let s = percent_encoding::percent_decode_str(data).decode_utf8()?;
		Ok(Self(s.into()))
	}
}
impl From<String> for RefID {
	fn from(v: String) -> Self {
		Self(v)
	}
}
impl<'a> From<&'a str> for RefID {
	fn from(v: &'a str) -> Self {
		Self(v.into())
	}
}
impl std::fmt::Display for RefID {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"ref:{}",
			percent_encoding::utf8_percent_encode(&self.0, &PATH_ENCODE_SET)
		)
	}
}
impl std::str::FromStr for RefID {
	type Err = FromStrError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (scheme, data) = s.split_once(':').ok_or(FromStrError::NoColon)?;
		if scheme != "ref" {
			return Err(FromStrError::UnrecognizedScheme);
		}
		let s = percent_encoding::percent_decode_str(data).decode_utf8()?;
		Ok(Self(s.into()))
	}
}
impl Deref for RefID {
	type Target = str;

	fn deref(&self) -> &Self::Target {
		self.0.as_str()
	}
}
impl<'de> serde::Deserialize<'de> for RefID {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error;
		let s = String::deserialize(deserializer)?;
		s.parse::<Self>().map_err(D::Error::custom)
	}
}

/// Generic ID pointing to either a `FileID` or `RefID`.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ID {
	File(FileID),
	Ref(RefID),
}
impl ID {
	/// Formats the ID as a urlencoded string.
	pub fn to_url_string(&self) -> String {
		match self {
			Self::File(v) => v.to_url_string(),
			Self::Ref(v) => v.to_url_string(),
		}
	}

	/// Parses the ID from a urlencoded string.
	pub fn parse_url_string(s: &str) -> Result<Self, FromStrError> {
		let (scheme, data) = s
			.split_once("%3A")
			.or(s.split_once("%3a"))
			.ok_or(FromStrError::NoColon)?;
		match scheme {
			"sha3" => {
				let mut buf = [0u8; 32];
				hex::decode_to_slice(data, &mut buf)?;
				Ok(Self::File(FileID(buf)))
			}
			"ref" => {
				let s = percent_encoding::percent_decode_str(data).decode_utf8()?;
				Ok(Self::Ref(RefID(s.into())))
			}
			_ => Err(FromStrError::UnrecognizedScheme),
		}
	}
}
impl std::fmt::Display for ID {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Self::File(v) => v.fmt(f),
			Self::Ref(v) => v.fmt(f),
		}
	}
}
impl std::str::FromStr for ID {
	type Err = FromStrError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (scheme, data) = s.split_once(':').ok_or(FromStrError::NoColon)?;
		match scheme {
			"sha3" => {
				let mut buf = [0u8; 32];
				hex::decode_to_slice(data, &mut buf)?;
				Ok(Self::File(FileID(buf)))
			}
			"ref" => {
				let s = percent_encoding::percent_decode_str(data).decode_utf8()?;
				Ok(Self::Ref(RefID(s.into())))
			}
			_ => Err(FromStrError::UnrecognizedScheme),
		}
	}
}
impl From<RefID> for ID {
	fn from(v: RefID) -> Self {
		Self::Ref(v)
	}
}
impl From<FileID> for ID {
	fn from(v: FileID) -> Self {
		Self::File(v)
	}
}
impl<'de> serde::Deserialize<'de> for ID {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error;
		let s = String::deserialize(deserializer)?;
		s.parse::<Self>().map_err(D::Error::custom)
	}
}

thread_local! {
	static SERIALIZED_IDS: RefCell<Option<BTreeSet<ID>>> = RefCell::new(None);
}

/// Runs a closure and gathers all IDs that are serialized in it.
///
/// In addition to the function's return value, also return a [`BTreeSet`] of all [`FileID`]s, [`RefID`]s, and [`ID`]s
/// whose `serialize` methods were called (directly or indirectly) by the closure.
///
/// This is useful for generating a set of links for [`crate::metadata::Metadata`].
///
/// Panics
/// ------
///
/// If called within another `track_id_serializations` closure (i.e. this function isn't re-entrant).
pub fn track_id_serializations<R, F: FnOnce() -> R>(cb: F) -> (R, BTreeSet<ID>) {
	SERIALIZED_IDS.with(|serialized_ids| {
		let mut serialized_ids = serialized_ids.borrow_mut();
		if serialized_ids.is_some() {
			panic!("Cannot use track_id_serializations recursively");
		}
		*serialized_ids = Some(BTreeSet::new());
	});
	let guard = TrackIdSerializationGuard;

	let r = (cb)();

	let ids = SERIALIZED_IDS
		.with(|serialized_ids| serialized_ids.borrow_mut().take())
		.unwrap();
	std::mem::forget(guard);
	return (r, ids);
}

struct TrackIdSerializationGuard;
impl std::ops::Drop for TrackIdSerializationGuard {
	fn drop(&mut self) {
		let _ = SERIALIZED_IDS.try_with(|serialized_ids| {
			*serialized_ids.borrow_mut() = None;
		});
	}
}

impl serde::Serialize for FileID {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		SERIALIZED_IDS.with(|serialized_ids| {
			let mut serialized_ids = serialized_ids.borrow_mut();
			if let Some(ref mut set) = *serialized_ids {
				set.insert(self.clone().into());
			}
		});
		self.to_string().serialize(serializer)
	}
}
impl serde::Serialize for RefID {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		SERIALIZED_IDS.with(|serialized_ids| {
			let mut serialized_ids = serialized_ids.borrow_mut();
			if let Some(ref mut set) = *serialized_ids {
				set.insert(self.clone().into());
			}
		});
		self.to_string().serialize(serializer)
	}
}
impl serde::Serialize for ID {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		SERIALIZED_IDS.with(|serialized_ids| {
			let mut serialized_ids = serialized_ids.borrow_mut();
			if let Some(ref mut set) = *serialized_ids {
				set.insert(self.clone());
			}
		});
		self.to_string().serialize(serializer)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	fn from_hex<const N: usize>(str: &str) -> [u8; N] {
		let mut buf: [u8; N] = [0; N];
		hex::decode_to_slice(str, &mut buf).unwrap();
		buf
	}

	#[test]
	fn test_parse() {
		assert_eq!(
			"sha3:020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5b"
				.parse::<ID>()
				.unwrap(),
			ID::File(
				from_hex("020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5b").into()
			)
		);
		assert_eq!(
			"ref:hello_world".parse::<ID>().unwrap(),
			ID::Ref("hello_world".into())
		);
		assert_eq!(
			"ref:413063006930405788cd35253f09326d"
				.parse::<ID>()
				.unwrap(),
			ID::Ref("413063006930405788cd35253f09326d".into())
		);
		assert!(matches!(
			"sha3:020f1959a2d1854".parse::<ID>(),
			Err(FromStrError::InvalidHexData(_))
		));
		assert!(matches!(
			"http://www.example.com".parse::<ID>(),
			Err(FromStrError::UnrecognizedScheme)
		));
		assert!(matches!(
			"sha1:0351adc07a2323a9bfc94e6020bf7a191052c605".parse::<ID>(),
			Err(FromStrError::UnrecognizedScheme)
		));
	}

	#[test]
	fn track_id_serializations() {
		let id1 = "sha3:020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5b"
			.parse::<ID>()
			.unwrap();
		let id2 = "ref:413063006930405788cd35253f09326d"
			.parse::<ID>()
			.unwrap();
		let (res, ids) = super::track_id_serializations(|| serde_json::to_string(&[&id1, &id2]));
		assert_eq!(
			res.unwrap().as_str(),
			r#"["sha3:020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5b","ref:413063006930405788cd35253f09326d"]"#
		);
		assert_eq!(ids.len(), 2);
		assert!(ids.contains(&id1));
		assert!(ids.contains(&id2));

		let id3 = "sha3:020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5c"
			.parse::<ID>()
			.unwrap();
		let (res, ids) = super::track_id_serializations(|| serde_json::to_string(&[&id3]));
		assert_eq!(
			res.unwrap().as_str(),
			r#"["sha3:020f1959a2d18544518c4a86bd631456f691ad9d96f4776ddc8b63699c99ce5c"]"#
		);
		assert_eq!(ids.len(), 1);
		assert!(ids.contains(&id3));
	}
}
