//! File metadata

use std::{
	collections::BTreeSet,
	fmt::Write,
};

use serde::{
	Deserialize,
	Serialize,
};

use crate::identifiers::ID;

/// File metadata
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Metadata {
	/// Other files that this object refers to, for garbage collection
	pub links: BTreeSet<ID>,
}
impl Metadata {
	/// Creates a new metadata object
	pub fn new(links: BTreeSet<ID>) -> Self {
		Self { links }
	}

	/// Creates a new metadata object without any links
	pub const fn new_leaf() -> Self {
		Self {
			links: BTreeSet::new(),
		}
	}

	/// Converts a list of IDs to a comma-separated string.
	pub fn links_to_comma_separated_str(links: &BTreeSet<ID>) -> String {
		let mut links_str = String::new();
		for link in links.iter() {
			if !links_str.is_empty() {
				links_str.push(',');
			}
			write!(&mut links_str, "{}", link).unwrap();
		}
		links_str
	}

	/// Parses a list of comma-separated IDs to a set.
	pub fn links_from_comma_separated_str(
		s: &str,
	) -> Result<BTreeSet<ID>, crate::identifiers::FromStrError> {
		s.split(",")
			.map(|s| s.trim())
			.filter(|s| !s.is_empty())
			.map(|s| s.parse::<ID>())
			.collect::<Result<BTreeSet<_>, _>>()
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::identifiers::RefID;

	#[test]
	fn links_to_comma_separated_str() {
		let mut set = BTreeSet::new();
		set.insert("ref:b5a5d6c6c56c11ec824d8b1d1574563b".parse().unwrap());
		set.insert(
			"sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f"
				.parse()
				.unwrap(),
		);
		let s = Metadata::links_to_comma_separated_str(&set);
		assert_eq!(s, "sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f,ref:b5a5d6c6c56c11ec824d8b1d1574563b");
	}

	#[test]
	fn links_from_comma_separated_str() {
		let mut expected = BTreeSet::new();
		expected.insert("ref:b5a5d6c6c56c11ec824d8b1d1574563b".parse().unwrap());
		expected.insert(
			"sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f"
				.parse()
				.unwrap(),
		);
		let parsed = Metadata::links_from_comma_separated_str("sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f,ref:b5a5d6c6c56c11ec824d8b1d1574563b").unwrap();
		assert_eq!(parsed, expected);
	}

	#[test]
	fn links_from_comma_separated_str_empty() {
		let expected = BTreeSet::new();
		let parsed = Metadata::links_from_comma_separated_str("").unwrap();
		assert_eq!(parsed, expected);
	}

	#[test]
	fn links_to_comma_separated_str_ref_with_comma() {
		let mut set = BTreeSet::new();
		set.insert(ID::Ref(RefID::from("hello, world!")));
		set.insert(
			"sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f"
				.parse()
				.unwrap(),
		);
		let s = Metadata::links_to_comma_separated_str(&set);
		assert_eq!(s, "sha3:ccb8f9235f4a932ca0abbb2c2436725e2e8dc75b99e7f6c4505b2a936eb63b3f,ref:hello%2C%20world!");
	}
}
