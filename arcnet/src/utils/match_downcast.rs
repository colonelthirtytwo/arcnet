/// Macro for easily downcasting.
macro_rules! match_downcast {
	($value:expr, {
		$binding:pat = $ty:ty => $body:expr,
		$($rest:tt)*
	}) => {{
		let v = $value;
		match v.downcast::<$ty>() {
			Ok($binding) => $body,
			Err(v) => match_downcast!(v, {$($rest)*}),
		}
	}};

	($(ref)? $value:expr, {
		$default:pat => $default_body:expr $(,)?
	}) => {{
		let $default = $value;
		$default_body
	}};

	(ref $value:expr, {
		$binding:pat = $ty:ty => $body:expr,
		$($rest:tt)*
	}) => {{
		let v = $value;
		match v.downcast_ref::<$ty>() {
			Some($binding) => $body,
			None => match_downcast!(ref v, {$($rest)*}),
		}
	}};
}
pub(crate) use match_downcast;

#[cfg(test)]
mod tests {
	#[test]
	fn match_downcast_test() {
		match_downcast!(anyhow::Error::msg(String::from("Oh no!")), {
			_ = i32 => panic!("match i32"),
			_ = bool => panic!("match bool"),
			e = String => assert_eq!(e, "Oh no!"),
			_ => panic!("match default"),
		})
	}

	#[test]
	fn match_downcast_default() {
		match_downcast!(anyhow::Error::msg(String::from("Oh no!")), {
			_ = i32 => panic!("match i32"),
			_ = bool => panic!("match bool"),
			e => assert_eq!(e.downcast::<String>().unwrap(), "Oh no!"),
		})
	}

	#[test]
	fn match_downcast_ref() {
		let v = anyhow::Error::msg(String::from("Oh no!"));
		match_downcast!(ref v, {
			_ = i32 => panic!("match i32"),
			_ = bool => panic!("match bool"),
			e = String => assert_eq!(e, "Oh no!"),
			_ => panic!("match default"),
		});
		let _ = v;
	}
}
