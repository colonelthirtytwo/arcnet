use std::{
	pin::Pin,
	task::{
		Context,
		Poll,
	},
};

use pin_project::pin_project;
use tokio::io::AsyncWrite;

#[pin_project]
pub struct InferWrite<W: AsyncWrite> {
	#[pin]
	inner: W,
	prefix: [u8; 8192],
	prefix_filled: usize,
}
impl<W: AsyncWrite> InferWrite<W> {
	pub fn new(inner: W) -> Self {
		Self {
			inner,
			prefix: [0; 8192],
			prefix_filled: 0,
		}
	}

	pub fn infer(&self) -> Option<infer::Type> {
		infer::get(&self.prefix[..self.prefix_filled])
	}

	pub fn into_inner(self) -> W {
		self.inner
	}
}
impl<W: AsyncWrite> AsyncWrite for InferWrite<W> {
	fn poll_write(
		self: Pin<&mut Self>,
		cx: &mut Context<'_>,
		buf: &[u8],
	) -> Poll<Result<usize, std::io::Error>> {
		let this = self.project();
		match this.inner.poll_write(cx, buf) {
			Poll::Ready(Ok(v)) => {
				let ncpy = (this.prefix.len() - *this.prefix_filled).min(buf.len());
				this.prefix[*this.prefix_filled..*this.prefix_filled + ncpy].copy_from_slice(buf);
				*this.prefix_filled += ncpy;
				Poll::Ready(Ok(v))
			}
			v => v,
		}
	}

	fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), std::io::Error>> {
		self.project().inner.poll_flush(cx)
	}

	fn poll_shutdown(
		self: Pin<&mut Self>,
		cx: &mut Context<'_>,
	) -> Poll<Result<(), std::io::Error>> {
		self.project().inner.poll_shutdown(cx)
	}
}
