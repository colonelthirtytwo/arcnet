//! File list structure

use std::collections::BTreeMap;

use serde::{
	Deserialize,
	Serialize,
};

use crate::identifiers::ID;

/// Media Type (MIME) for file lists.
///
/// Use this when storing file lists.
#[cfg(none)]
pub const FILE_LIST_MEDIA_TYPE: &str = "application/x-arcnet-file-list+msgpack";

/// List of files.
///
/// Used for storing a collection of files in Arcnet. This describes the filesystem layout and stores IDs
/// pointing to the file contents.
///
/// Keys are paths, relative to the archive root. For consistency, they should not start with a "/" or "./" segment,
/// though clients should handle it if needed.
pub type FileList = BTreeMap<String, FileListEntry>;

/// Entry in the file list.
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum FileListEntry {
	#[serde(rename = "f")]
	File {
		/// ID of file contents
		id: ID,
		/// Whether the file should be marked as executable on UNIX systems.
		#[serde(default, skip_serializing_if = "not")]
		executable: bool,
	},
	#[serde(rename = "d")]
	Directory {},
}

fn not(v: &bool) -> bool {
	!*v
}
