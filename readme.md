WIP content-addressible data storage with configurable backends

As a content-addressible data store, data that is inserted is keyed by the hash of their contents, which can
be used to retrieve the data. Content-addressbility has multiple advantages: the ID is the same everywhere,
since its based on the data; it simplifies synchronization, as you either have the entry or you do not; and
it helps verify the integrity of the data. Additionally, the system can also store "refs", simple mutable key/value
pairs where the values are data IDs, to help facilitate changing data.

Data storage is configurable with multiple backends for storing and retreiving the data, such as a directory of flat files,
a local SQLite database, or remotely on different servers. Backends can wrap and aggregate others, allowing for compression,
encryption, and distribution.

Arcnet is designed for multimedia storage, synchronization, and backup, where files change infrequently and are often organized
by metadata.
