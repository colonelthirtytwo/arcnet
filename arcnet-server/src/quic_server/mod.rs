use std::{
	net::SocketAddr,
	ops::Deref,
	path::PathBuf,
	sync::Arc,
};

use anyhow::Context;
use arcnet::{
	errors::SetReferenceError,
	interface::{
		GetFileOptions,
		StoreInterface,
	},
	messages,
	nodes::{
		FinishedFile,
		Ref,
		ResolvedRef,
	},
};
use futures::StreamExt;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct QuicServerSettings {
	pub address: String,
	pub certificate_path: PathBuf,
	pub key_path: PathBuf,
	// TODO: authenticate clients
}

pub async fn start_server(
	interface: Arc<StoreInterface>,
	cfg: QuicServerSettings,
	mut stop: tokio::sync::watch::Receiver<u32>,
) -> anyhow::Result<tokio::task::JoinHandle<anyhow::Result<()>>> {
	let certificate = tokio::fs::read(&cfg.certificate_path)
		.await
		.with_context(|| {
			format!(
				"While reading server certificate {:?}",
				cfg.certificate_path
			)
		})?;
	let key = tokio::fs::read(&cfg.key_path)
		.await
		.with_context(|| format!("While reading server key {:?}", cfg.key_path))?;

	let tls_cfg = rustls::ServerConfig::builder()
		.with_safe_defaults()
		.with_no_client_auth()
		.with_single_cert(
			vec![rustls::Certificate(certificate)],
			rustls::PrivateKey(key),
		)
		.context("While configuring TLS")?;

	let serv_cfg = quinn::ServerConfig::with_crypto(Arc::new(tls_cfg));

	let sock = std::net::UdpSocket::bind(&cfg.address).context("While creating UDP socket")?;

	let endpoint = quinn::Endpoint::new(
		Default::default(),
		Some(serv_cfg),
		sock,
		quinn::TokioRuntime,
	)
	.context("While creating quic server")?;

	trace!("Quic server initialized at {}", endpoint.local_addr()?);

	Ok(tokio::task::spawn(async move {
		loop {
			tokio::select!(
				conn = endpoint.accept() => {
					if let Some(conn) = conn {
						tokio::task::spawn(handle_connection(conn, interface.clone()));
					} else {
						return Err(anyhow::anyhow!("Quic acceptor ran out unexpectedly"));
					}
				}
				_ = stop.changed() => {
					info!("QUIC server shutting down");
					endpoint.close(2u32.into(), "Server shutting down".as_bytes());
					endpoint.wait_idle().await;
					return Ok(())
				}
			)
		}
	}))
}

async fn handle_connection(
	conn: quinn::Connecting,
	frontend: Arc<StoreInterface>,
) -> anyhow::Result<()> {
	let addr = conn.remote_address();
	info!("QUIC connection from {}", addr);

	let conn = conn.await.context("While connecting")?;

	loop {
		tokio::select! {
			stream = conn.accept_bi() => {
				let (send_stream, recv_stream) = stream.context("While accepting new stream")?;
				let frontend = frontend.clone();
				tokio::task::spawn(async move {
					let id = send_stream.id();
					if let Err(e) = handle_stream(addr, send_stream, recv_stream, frontend).await {
						error!("Client {} stream {}: {:#}", addr, id, e);
					}
				});
			}
			stream = conn.accept_uni() => {
				// Don't care
				let mut stream = stream?;
				let _ = stream.stop(1u32.into());
			}
			_ = conn.read_datagram() => {
				// Don't care
			}
			reason = conn.closed() => {
				info!("QUIC connection from {} closed: {}", addr, reason);
				break;
			}
		}
	}

	Ok(())
}

async fn handle_stream(
	client: SocketAddr,
	send_stream: quinn::SendStream,
	recv_stream: quinn::RecvStream,
	interface: Arc<StoreInterface>,
) -> anyhow::Result<()> {
	let mut send_stream = ResetOnDrop(Some(send_stream));
	let mut recv_stream = ResetOnDrop(Some(recv_stream));

	let mut req_len = [0u8; 4];
	match recv_stream.read_exact(&mut req_len).await {
		Ok(()) => {}
		Err(e) => {
			return Err(anyhow::anyhow!(e).context("While reading message length"));
		}
	};
	let req_len = u32::from_le_bytes(req_len) as usize;

	let mut msg_buf = vec![0u8; req_len];
	recv_stream
		.read_exact(&mut msg_buf)
		.await
		.context("While reading message")
		.map_err(|e| {
			let _ = send_stream.reset(2u32.into());
			let _ = recv_stream.stop(2u32.into());
			e
		})?;

	let msg = rmp_serde::from_slice::<messages::Request>(&msg_buf)
		.context("While deserializing message")
		.map_err(|e| {
			let _ = send_stream.reset(2u32.into());
			let _ = recv_stream.stop(2u32.into());
			e
		})?;

	debug!("Request from {}: {:?}", client, msg);

	match msg {
		messages::Request::GetRef { id, node_path } => {
			let _ = recv_stream.into_inner().stop(0u32.into());
			let resp = interface
				.get_ref(&id, Some(&node_path))
				.await
				.map(|o| {
					o.map(|Ref { target, node_path }| messages::GetRefResponseInner {
						target,
						node_path,
					})
				})
				.map_err(|e| handle_error(client, send_stream.id(), e));
			send_response(client, &mut send_stream, &resp).await?;
			let _ = send_stream.into_inner().finish();
			Ok(())
		}
		messages::Request::SetRef {
			id,
			target,
			node_path,
			expected_existing,
		} => {
			//let _ = recv_stream.into_inner().stop(0u32.into());
			let resp = interface
				.set_ref(&id, &target, Some(&*node_path), expected_existing.as_ref())
				.await;
			let resp = match resp {
				Ok(Ref { target, node_path }) => Ok(messages::SetRefResponse { target, node_path }),
				Err(SetReferenceError::DidNotMatch { existing }) => {
					Err(messages::Error::SetReferenceDoesNotMatch(existing))
				}
				Err(SetReferenceError::Error(e)) => Err(handle_error(client, send_stream.id(), e)),
			};

			send_response(client, &mut send_stream, &resp).await?;
			let _ = send_stream.into_inner().finish();
			Ok(())
		}
		messages::Request::Delete { id, node_path } => {
			let resp = interface
				.delete(&id, &node_path)
				.await
				.map_err(|e| handle_error(client, send_stream.id(), e));
			send_response(client, &mut send_stream, &resp).await?;
			let _ = send_stream.into_inner().finish();
			Ok(())
		}

		messages::Request::ListStoredIDs { node_path } => {
			//let _ = recv_stream.into_inner().stop(0u32.into());
			let res = interface.stored_ids(Some(&*node_path)).await;
			match res {
				Ok(mut stream) => {
					let resp: Result<messages::ListStoredIDsResponse, messages::Error> = Ok(());
					send_response(client, &mut send_stream, &resp).await?;

					let mut buf = vec![];
					while let Some(res) = stream.next().await {
						let id = res?;
						buf.clear();
						serde_json::to_writer(&mut buf, &id)
							.context("While serializing stored ID")?;
						send_stream.write_all(&buf).await?;
						send_stream.write_all(b"\n").await?;
					}
				}
				Err(e) => {
					let resp: Result<messages::ListStoredIDsResponse, messages::Error> =
						Err(handle_error(client, send_stream.id(), e));
					send_response(client, &mut send_stream, &resp).await?;
				}
			};
			let _ = send_stream.into_inner().finish();
			Ok(())
		}
		messages::Request::ListSubNodes { node_path } => {
			//let _ = recv_stream.into_inner().stop(0u32.into());
			let res = interface.sub_nodes(&*node_path).await;
			match res {
				Ok(mut stream) => {
					let resp: Result<messages::ListSubnodesResponse, messages::Error> = Ok(());
					send_response(client, &mut send_stream, &resp).await?;

					let mut buf = vec![];
					while let Some(res) = stream.next().await {
						let name = res?;
						buf.clear();
						serde_json::to_writer(&mut buf, &name)
							.context("While serializing subnode name")?;
						send_stream.write_all(&buf).await?;
						send_stream.write_all(b"\n").await?;
					}
				}
				Err(e) => {
					let resp: Result<messages::ListSubnodesResponse, messages::Error> =
						Err(handle_error(client, send_stream.id(), e));
					send_response(client, &mut send_stream, &resp).await?;
				}
			};
			let _ = send_stream.into_inner().finish();
			Ok(())
		}

		messages::Request::GetFile {
			id,
			node_path,
			range,
		} => {
			//let _ = recv_stream.into_inner().stop(0u32.into());
			let resp = interface
				.get_file(
					&id,
					GetFileOptions::default()
						.node_path(node_path.deref())
						.range(range),
				)
				.await;
			match resp {
				Ok(Some(mut f)) => {
					let resp: Result<_, messages::Error> =
						Ok(Some(messages::GetFileResponseInner {
							id: f.id().clone(),
							node_path: f.node_path().into(),
							metadata: f.metadata().clone(),
							size: f.size(),
							range: f.range(),
						}));
					send_response(client, &mut send_stream, &resp).await?;
					if let Err(e) = tokio::io::copy(&mut f, &mut *send_stream).await {
						let _ = send_stream.reset(1u32.into());
						return Err(anyhow::anyhow!(e));
					}
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Ok(None) => {
					let resp: Result<messages::GetFileResponse, messages::Error> = Ok(None);
					send_response(client, &mut send_stream, &resp).await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Err(e) => {
					let resp: Result<messages::GetFileResponse, messages::Error> =
						Err(handle_error(client, send_stream.id(), e));
					send_response(client, &mut send_stream, &resp).await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
			}
		}
		messages::Request::PushFile { meta, node_path } => {
			let res: anyhow::Result<()> = async {
				let mut f = interface.push_file(meta, Some(&*node_path)).await?;
				tokio::io::copy(&mut *recv_stream, &mut f).await?;

				let msg = f.finish().await.map(|FinishedFile { id, node_path }| {
					messages::PushFileResponse { id, node_path }
				})?;
				let resp: Result<messages::PushFileResponse, messages::Error> = Ok(msg);
				send_response(client, &mut send_stream, &resp).await?;
				Ok(())
			}
			.await;
			match res {
				Ok(()) => {
					let _ = recv_stream.into_inner().stop(0u32.into());
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Err(e) => {
					let resp: Result<messages::PushFileResponse, messages::Error> =
						Err(handle_error(client, send_stream.id(), e));
					send_response(client, &mut send_stream, &resp).await?;
					Ok(())
				}
			}
		}
		messages::Request::GetResolvedRef {
			id,
			node_path,
			range,
		} => {
			//let _ = recv_stream.into_inner().stop(0u32.into());
			match interface
				.get_resolved_ref(
					&id,
					GetFileOptions::default()
						.node_path(node_path.deref())
						.range(range),
				)
				.await
			{
				Ok(ResolvedRef::NoRef) => {
					let resp: Result<messages::GetResolvedRefResponse, messages::Error> =
						Ok(messages::GetResolvedRefResponse::NoRef);
					send_response(client, &mut send_stream, &resp).await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Ok(ResolvedRef::DanglingRef { ref_id: _, ref_ }) => {
					let resp: Result<messages::GetResolvedRefResponse, messages::Error> =
						Ok(messages::GetResolvedRefResponse::DanglingRef {
							target: ref_.target,
							ref_node_path: ref_.node_path,
						});
					send_response(client, &mut send_stream, &resp).await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Ok(ResolvedRef::ResolvedRef { ref_, mut file }) => {
					let resp: Result<messages::GetResolvedRefResponse, messages::Error> =
						Ok(messages::GetResolvedRefResponse::ResolvedRef {
							target: ref_.target,
							ref_node_path: ref_.node_path,
							file: messages::GetFileResponseInner {
								id: file.id().clone(),
								node_path: file.node_path().into(),
								metadata: file.metadata().clone(),
								size: file.size(),
								range: file.range(),
							},
						});
					send_response(client, &mut send_stream, &resp).await?;
					if let Err(e) = tokio::io::copy(&mut file, &mut *send_stream).await {
						let _ = send_stream.reset(1u32.into());
						return Err(anyhow::anyhow!(e));
					}
					send_stream.finish().await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
				Err(e) => {
					let resp: Result<messages::GetResolvedRefResponse, messages::Error> =
						Err(handle_error(client, send_stream.id(), e));
					send_response(client, &mut send_stream, &resp).await?;
					let _ = send_stream.into_inner().finish();
					Ok(())
				}
			}
		}
	}
}

fn handle_error(
	client: SocketAddr,
	streamid: quinn::StreamId,
	e: anyhow::Error,
) -> messages::Error {
	messages::Error::from_anyhow(e).unwrap_or_else(|e| {
		error!(
			"Client {} stream {} server error: {:#}",
			client,
			streamid,
			FormatAnyhowError(e)
		);
		messages::Error::Misc("Internal Server Error".into())
	})
}

async fn send_response<T: serde::Serialize>(
	_client: SocketAddr,
	send_stream: &mut quinn::SendStream,
	v: &T,
) -> anyhow::Result<()> {
	let msg = rmp_serde::to_vec_named(v).context("While serializing response")?;

	let len: u32 = msg
		.len()
		.try_into()
		.context("While getting response message length")?;
	let len = len.to_le_bytes();

	send_stream.write_all(&len).await.context("While writing")?;
	send_stream.write_all(&msg).await.context("While writing")?;
	Ok(())
}

trait QuinnStream {
	fn fail(&mut self);
}
impl QuinnStream for quinn::RecvStream {
	fn fail(&mut self) {
		let _ = self.stop(1u32.into());
	}
}
impl QuinnStream for quinn::SendStream {
	fn fail(&mut self) {
		let _ = self.reset(1u32.into());
	}
}

struct ResetOnDrop<T: QuinnStream>(Option<T>);
impl<T: QuinnStream> ResetOnDrop<T> {
	pub fn into_inner(mut self) -> T {
		self.0.take().unwrap()
	}
}
impl<T: QuinnStream> std::ops::Deref for ResetOnDrop<T> {
	type Target = T;

	fn deref(&self) -> &Self::Target {
		self.0.as_ref().unwrap()
	}
}
impl<T: QuinnStream> std::ops::DerefMut for ResetOnDrop<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		self.0.as_mut().unwrap()
	}
}
impl<T: QuinnStream> std::ops::Drop for ResetOnDrop<T> {
	fn drop(&mut self) {
		self.0.take().map(|mut s| s.fail());
	}
}

struct FormatAnyhowError(anyhow::Error);
impl std::fmt::Display for FormatAnyhowError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for (i, err) in self.0.chain().rev().enumerate() {
			if i != 0 {
				f.write_str("\n\t")?;
			}
			std::fmt::Display::fmt(err, f)?;
		}
		Ok(())
	}
}
