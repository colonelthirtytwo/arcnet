#[macro_use]
extern crate log;

pub mod http_server;
pub mod quic_server;

use std::{
	path::PathBuf,
	sync::Arc,
};

use anyhow::Context;
use arcnet::cfg_loader::NodeTreeLoader;
use clap::Parser;
use log::LevelFilter;
use time::OffsetDateTime;

/// HTTP and QUIC server for ArcNet.
#[derive(Parser)]
struct Args {
	/// Log less. Can be specified multiple times.
	#[clap(short = 'q', long, parse(from_occurrences))]
	quiet: i32,
	/// Log more. Can be specified multiple times.
	#[clap(short = 'v', long, parse(from_occurrences))]
	verbose: i32,

	/// Path to the configuration file to load.
	#[clap(parse(from_os_str))]
	store_cfg: PathBuf,
}

#[tokio::main]
async fn main() {
	if let Err(err) = real_main().await {
		eprintln!("{:#}", err);
		std::process::exit(1);
	}
}

async fn real_main() -> Result<(), anyhow::Error> {
	let args = Args::parse();
	setup_logging(args.quiet, args.verbose);

	let cfg_bytes = tokio::fs::read(&args.store_cfg)
		.await
		.with_context(|| format!("While reading config file {:?}", args.store_cfg))?;
	let cfg = toml::from_slice::<toml::Value>(&cfg_bytes)
		.with_context(|| format!("While parsing config file {:?}", args.store_cfg))?;

	let http_server_value = cfg.get("http_server").cloned();
	let quic_server_value = cfg.get("quic_server").cloned();

	if http_server_value.is_none() && quic_server_value.is_none() {
		return Err(anyhow::anyhow!("No servers configured"));
	}

	let interface = NodeTreeLoader::prepare(cfg).await?.load().await?;
	let interface = Arc::new(interface);

	let http_server_cfg = http_server_value
		.map(|cfg| cfg.try_into::<http_server::HttpServerSettings>())
		.transpose()
		.context("While parsing HTTP server config")?;
	let quic_server_cfg = quic_server_value
		.map(|cfg| cfg.try_into::<quic_server::QuicServerSettings>())
		.transpose()
		.context("While parsing QUIC server config")?;

	let (stop_send, stop_recv) = tokio::sync::watch::channel(0);
	tokio::task::spawn(async move {
		tokio::signal::ctrl_c().await.unwrap();
		info!("Ctrl+C detected, stopping server...");
		let _ = stop_send.send(1);
	});

	let http_task = {
		let frontend = interface.clone();
		let mut stop_recv = stop_recv.clone();

		if let Some(cfg) = http_server_cfg {
			http_server::start_server(frontend.clone(), cfg, stop_recv)
				.await
				.context("While starting HTTP server")?
		} else {
			tokio::task::spawn(async move {
				while *stop_recv.borrow() == 0 {
					if stop_recv.changed().await.is_err() {
						break;
					}
				}
				Ok(())
			})
		}
	};

	let quic_task = {
		let frontend = interface.clone();
		let mut stop_recv = stop_recv.clone();

		if let Some(cfg) = quic_server_cfg {
			quic_server::start_server(frontend.clone(), cfg, stop_recv)
				.await
				.context("While starting QUIC server")?
		} else {
			tokio::task::spawn(async move {
				while *stop_recv.borrow() == 0 {
					if stop_recv.changed().await.is_err() {
						break;
					}
				}
				Ok(())
			})
		}
	};

	let (http_res, quic_res) = futures::join!(http_task, quic_task);

	let mut ok = true;
	if let Err(e) = http_res.map_err(|e| anyhow::anyhow!(e)).and_then(|res| res) {
		error!("HTTP server error: {}", e);
		ok = false;
	}
	if let Err(e) = quic_res.map_err(|e| anyhow::anyhow!(e)).and_then(|res| res) {
		error!("QUIC server error: {}", e);
		ok = false;
	}

	if !ok {
		std::process::exit(1);
	}
	Ok(())
}

fn setup_logging(q: i32, v: i32) {
	let level = match v - q {
		n if n <= -1 => LevelFilter::Error,
		-1 => LevelFilter::Warn,
		0 => LevelFilter::Info,
		1 => LevelFilter::Debug,
		_ => LevelFilter::Trace,
	};

	fern::Dispatch::new()
		.format(|out, message, record| {
			out.finish(format_args!(
				"[{}][{}][{}] {}",
				OffsetDateTime::now_local()
					.ok()
					.unwrap_or_else(OffsetDateTime::now_utc)
					.format(&time::format_description::well_known::Rfc3339)
					.unwrap(),
				record.target(),
				record.level(),
				message
			))
		})
		.level(level)
		.level_for("quinn", LevelFilter::Warn)
		.chain(std::io::stderr())
		.apply()
		.unwrap();
}

#[cfg(test)]
mod tests {
	use arcnet::{
		interface::StoreInterface,
		path::NodePathBuf,
	};

	use super::*;

	static PORT: std::sync::atomic::AtomicU16 = std::sync::atomic::AtomicU16::new(5678);

	arcnet::node_tests!(quic_client_server, |path| {
		let port = PORT.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
		let server_node = Arc::pin(arcnet::nodes::mem::MemNode::new(NodePathBuf::root()));
		let server_interface = Arc::new(StoreInterface::new(server_node));
		let (stop_send, stop_recv) = tokio::sync::watch::channel(0);
		let serv_task = quic_server::start_server(
			server_interface,
			quic_server::QuicServerSettings {
				address: format!("127.0.0.1:{}", port),
				certificate_path: "../test-certs/server.crt".into(),
				key_path: "../test-certs/server.key".into(),
			},
			stop_recv,
		)
		.await
		.unwrap();

		let test_end = Box::pin(async move {
			let _ = stop_send.send(1);
			serv_task.await.unwrap().unwrap();
		});

		let client_node = arcnet::nodes::quic_client::QuicClientNode::connect(
			path,
			std::net::SocketAddrV4::new(std::net::Ipv4Addr::new(127, 0, 0, 1), port).into(),
			None,
			None,
		)
		.await
		.unwrap();

		(Arc::pin(client_node), test_end)
	});
}
