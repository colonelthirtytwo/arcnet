use std::{
	collections::BTreeSet,
	pin::Pin,
	sync::Arc,
	task::Poll,
};

use actix_web::{
	delete,
	get,
	http::header::{
		self,
		ByteRangeSpec,
	},
	post,
	put,
	web::{
		self,
		Bytes,
		BytesMut,
	},
	HttpRequest,
	HttpResponse,
};
use anyhow::Context;
use arcnet::{
	identifiers::{
		FileID,
		RefID,
		ID,
	},
	interface::{
		GetFileOptions,
		StoreInterface,
	},
	metadata::Metadata,
	nodes::{
		FinishedFile,
		GetRange,
		NodeReader,
		Ref,
		StoredID,
	},
	path::NodePathBuf,
};
use futures::{
	StreamExt,
	TryFutureExt,
	TryStreamExt,
};
use serde::Deserialize;
use tokio::io::{
	AsyncRead,
	AsyncReadExt,
	AsyncWriteExt,
};

#[derive(Deserialize)]
pub struct HttpServerSettings {
	pub address: String,

	pub hostname: String,

	#[serde(default)]
	pub workers: Option<usize>,

	#[serde(default)]
	pub base_path: String,
}

/// 400 error with a plaintext message
#[derive(Debug, Clone)]
struct ClientError(pub String);
impl std::fmt::Display for ClientError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(&self.0)
	}
}
impl std::error::Error for ClientError {}

/// Wrapper around [`anyhow::Error`] that provides an HTTP response.
///
/// Specializes a few errors in [`arcnet::errors`], as well as [`ClientError`].
///
/// Internal server errors should be logged and not published to the client.
#[derive(Debug)]
struct ErrorWrapper(anyhow::Error);
impl actix_web::error::ResponseError for ErrorWrapper {
	fn status_code(&self) -> actix_web::http::StatusCode {
		unreachable!()
	}

	fn error_response(&self) -> HttpResponse<actix_web::body::BoxBody> {
		if let Some(err) = self.0.downcast_ref::<ClientError>() {
			return HttpResponse::build(actix_web::http::StatusCode::BAD_REQUEST)
				.content_type("text/plain")
				.body(actix_web::body::BoxBody::new(err.0.clone()));
		}

		match arcnet::messages::Error::from_anyhow_ref(&self.0) {
			Some(err) => {
				let sc = match err {
					arcnet::messages::Error::Unsupported(_) => {
						actix_web::http::StatusCode::NOT_IMPLEMENTED
					}
					arcnet::messages::Error::Unauthorized(_) => {
						actix_web::http::StatusCode::FORBIDDEN
					}
					arcnet::messages::Error::NoSuchNode(_) => {
						actix_web::http::StatusCode::NOT_FOUND
					}
					arcnet::messages::Error::SetReferenceDoesNotMatch(_) => unreachable!(),
					arcnet::messages::Error::Misc(_) => unreachable!(),
				};

				HttpResponse::build(sc)
					.content_type("application/json")
					.body(actix_web::body::BoxBody::new(
						serde_json::to_vec(&err).unwrap(),
					))
			}
			None => {
				error!("{:#}", self.0);
				HttpResponse::build(actix_web::http::StatusCode::INTERNAL_SERVER_ERROR)
					.content_type("text/plain")
					.body(actix_web::body::BoxBody::new("Internal server error"))
			}
		}
	}
}
impl std::fmt::Display for ErrorWrapper {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str("An internal error has occured")
	}
}
impl<T> From<T> for ErrorWrapper
where
	T: Into<anyhow::Error>,
{
	fn from(v: T) -> Self {
		Self(v.into())
	}
}

pub async fn start_server(
	interface: Arc<StoreInterface>,
	cfg: HttpServerSettings,
	mut stop: tokio::sync::watch::Receiver<u32>,
) -> anyhow::Result<tokio::task::JoinHandle<anyhow::Result<()>>> {
	let base_path = cfg.base_path.clone();
	let mut s = actix_web::HttpServer::new(move || {
		actix_web::App::new()
			.app_data(web::Data::new(interface.clone()))
			.service(
				web::scope(&base_path)
					.service(post_file)
					.service(get_file)
					.service(post_file)
					.service(put_ref)
					.service(delete)
					.service(list_stored_ids)
					.service(list_sub_nodes),
			)
	})
	.server_hostname(&cfg.hostname)
	.disable_signals();
	if let Some(workers) = cfg.workers {
		s = s.workers(workers);
	}
	let s = s.bind(&cfg.address)?.run();

	let h = s.handle();
	tokio::task::spawn(async move {
		while *stop.borrow() == 0 {
			if stop.changed().await.is_err() {
				break;
			}
		}
		h.stop(true).await;
	});
	Ok(tokio::task::spawn(s.map_err(|e| anyhow::anyhow!(e))))
}

/// Adapter from [`NodeReader`] to [`actix_web::body::MessageBody`]
struct NodeReaderBody {
	reader: NodeReader,
	preread: Option<Bytes>,
	// Keep a buffer on us. Otherwise poll_next would have to allocate it and throw it away if
	// the reader returned Pending.
	buf: BytesMut,
}
impl actix_web::body::MessageBody for NodeReaderBody {
	type Error = std::io::Error;

	fn size(&self) -> actix_web::body::BodySize {
		actix_web::body::BodySize::Sized(self.reader.size())
	}

	fn poll_next(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> Poll<Option<Result<web::Bytes, Self::Error>>> {
		let this = Pin::into_inner(self);

		if let Some(preread) = this.preread.take() {
			return Poll::Ready(Some(Ok(preread)));
		}

		if this.buf.capacity() == 0 {
			this.buf = BytesMut::with_capacity(8 * 1024);
		}
		// TODO: convert BytesMut to a tokio ReadBuf to skip initialization
		this.buf.resize(8 * 1024, 0);
		let mut rb = tokio::io::ReadBuf::new(&mut *this.buf);
		match Pin::new(&mut this.reader).poll_read(cx, &mut rb) {
			Poll::Ready(Ok(())) => {
				let len = rb.filled().len();
				if len == 0 {
					Poll::Ready(None)
				} else {
					this.buf.resize(len, 0);
					let out_buf = std::mem::replace(&mut this.buf, BytesMut::new());
					Poll::Ready(Some(Ok(out_buf.freeze())))
				}
			}
			Poll::Ready(Err(e)) => Poll::Ready(Some(Err(e))),
			Poll::Pending => Poll::Pending,
		}
	}
}

#[get("/files/{id}/{node_path:.*}")]
async fn get_file(
	request: HttpRequest,
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(ID, NodePathBuf)>,
	range: Option<actix_web::web::Header<header::Range>>,
	if_none_match: Option<actix_web::web::Header<header::IfNoneMatch>>,
) -> Result<HttpResponse, ErrorWrapper> {
	let (id, node_path) = path.into_inner();

	match id {
		ID::File(id) => {
			let range = match range {
				Some(actix_web::web::Header(header::Range::Bytes(range))) if range.len() == 1 => {
					match range[0] {
						ByteRangeSpec::From(v) => GetRange::Range(v, u64::MAX),
						ByteRangeSpec::FromTo(a, b) => GetRange::Range(a, b),
						ByteRangeSpec::Last(_) => {
							return Ok(HttpResponse::BadRequest()
								.content_type("text/plain")
								.body("Last byte range is not yet supported"))
						}
					}
				}
				Some(_) => {
					return Ok(HttpResponse::BadRequest()
						.content_type("text/plain")
						.body("Only single byte range requests are supported"))
				}
				None => GetRange::Entire,
			};

			match interface
				.get_file(
					&id,
					GetFileOptions::default()
						.node_path(&*node_path)
						.range(range),
				)
				.await?
			{
				Some(mut f) => {
					let (inferred_type, first_bytes) = infer(Pin::new(&mut f)).await?;

					let meta = f.metadata();
					let file_node_path = f.node_path();
					let size = f.size();
					let range = f.range();

					let etag = id.to_string();
					let etag_matches = match if_none_match.as_deref() {
						Some(header::IfNoneMatch::Any) => true,
						Some(header::IfNoneMatch::Items(items)) => {
							items.iter().any(|t| t.tag() == etag)
						}
						None => false,
					};

					let links = Metadata::links_to_comma_separated_str(&meta.links);
					let mut resp = if etag_matches {
						HttpResponse::NotModified()
					} else if range == (0u64..size) {
						let mut resp = HttpResponse::Ok();
						resp.insert_header(("Content-Length", size));
						resp
					} else {
						let mut resp = HttpResponse::PartialContent();
						resp.insert_header((
							"Content-Range",
							format!("bytes {}-{}/{}", range.start, range.end, size),
						));
						resp
					};

					resp.content_type(
						inferred_type
							.map(|t| t.mime_type())
							.unwrap_or("application/octet-stream"),
					)
					.insert_header(("ETag", format!("\"{}\"", id)))
					.insert_header(("X-Links", links))
					.insert_header(("X-Node-Path", file_node_path.as_str()));

					if !etag_matches {
						Ok(resp.body(NodeReaderBody {
							preread: Some(first_bytes),
							reader: f,
							buf: BytesMut::new(),
						}))
					} else {
						Ok(resp.finish())
					}
				}
				None => Ok(HttpResponse::NotFound().body(format!("File with ID {} not found", id))),
			}
		}
		ID::Ref(id) => match (**interface).get_ref(&id, Some(&*node_path)).await? {
			Some(Ref { target, node_path }) => Ok(HttpResponse::TemporaryRedirect()
				.insert_header((
					"Location",
					request
						.url_for("get_file", &[target.to_string(), "".into()])
						.unwrap()
						.to_string(),
				))
				.insert_header(("X-Node-Path", node_path.as_str()))
				.content_type("text/plain")
				.body(target.to_string())),
			None => Ok(HttpResponse::NotFound()
				.content_type("text/plain")
				.body("Reference not found")),
		},
	}
}

#[derive(Deserialize)]
struct PushFileQuery {
	#[serde(deserialize_with = "PushFileQuery::deser_links", default)]
	links: BTreeSet<ID>,
}
impl PushFileQuery {
	fn deser_links<'de, D: serde::Deserializer<'de>>(de: D) -> Result<BTreeSet<ID>, D::Error> {
		use serde::de::Error;
		let s = String::deserialize(de)?;
		Metadata::links_from_comma_separated_str(&s).map_err(|e| D::Error::custom(e))
	}
}

#[post("/files/{node_path:.*}")]
async fn post_file(
	request: HttpRequest,
	payload: web::Payload,
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(NodePathBuf,)>,
) -> Result<HttpResponse, ErrorWrapper> {
	let qp = web::Query::<PushFileQuery>::from_query(request.query_string())
		.map_err(|e| ClientError(format!("Could not parse links: {}", e)))?
		.into_inner();

	let metadata = Metadata::new(qp.links);

	let mut file = interface
		.push_file(metadata.clone(), Some(&*path.0))
		.await?;
	let mut payload = payload.into_inner();
	while let Some(chunk) = payload.next().await {
		let chunk = chunk.context("While reading body")?;
		file.write_all(&chunk).await.context("While writing file")?;
	}
	let FinishedFile { id, node_path } = file.finish().await.context("While finishing file")?;

	info!("Pushed file {} ({} links)", id, metadata.links.len());

	Ok(HttpResponse::TemporaryRedirect()
		.insert_header(("Location", id.to_string()))
		.insert_header(("X-Node-Path", node_path.as_str().to_string()))
		.content_type("text/plain")
		.body(id.to_string()))
}

#[derive(Deserialize)]
struct PutRefQuery {
	#[serde(default)]
	expected_existing: Option<FileID>,
}

#[put("/files/{id}/{node_path:.*}")]
async fn put_ref(
	request: HttpRequest,
	body: web::Bytes,
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(RefID, NodePathBuf)>,
) -> Result<HttpResponse, ErrorWrapper> {
	let (id, node_path) = path.into_inner();

	let target = std::str::from_utf8(&body)
		.map_err(|e| e.to_string())
		.and_then(|s| s.parse::<FileID>().map_err(|e| e.to_string()))
		.map_err(|e| ClientError(format!("Could not parse file ID from body: {}", e)))?;
	let qp = web::Query::<PutRefQuery>::from_query(request.query_string())
		.map_err(|e| ClientError(format!("Could not parse query parameters: {}", e)))?;

	match interface
		.set_ref(
			&id,
			&target,
			Some(&*node_path),
			qp.expected_existing.as_ref(),
		)
		.await
	{
		Ok(Ref {
			target: _,
			node_path,
		}) => Ok(HttpResponse::NoContent()
			.insert_header(("X-Node-Path", node_path.as_str()))
			.finish()),
		Err(arcnet::errors::SetReferenceError::DidNotMatch { .. }) => {
			Ok(HttpResponse::PreconditionFailed()
				.content_type("text/plain")
				.body("expected_existing failed, query and try again"))
		}
		Err(arcnet::errors::SetReferenceError::Error(err)) => Err(err.into()),
	}
}

#[delete("/files/{id}/{node_path:.*}")]
async fn delete(
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(ID, NodePathBuf)>,
) -> Result<HttpResponse, ErrorWrapper> {
	let (id, node_path) = path.into_inner();
	interface.delete(&id, &*node_path).await?;
	Ok(HttpResponse::NoContent().finish())
}

#[get("/files_list/{node_path:.*}")]
async fn list_stored_ids(
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(NodePathBuf,)>,
) -> Result<HttpResponse, ErrorWrapper> {
	let (node_path,) = path.into_inner();

	// TODO: stream this
	let stream = interface.stored_ids(Some(&*node_path)).await?;
	let ids = stream.try_collect::<Vec<StoredID>>().await?;
	Ok(HttpResponse::Ok()
		.content_type("application/json")
		.json(&ids))
}

#[get("/sub_nodes_list/{node_path:.*}")]
async fn list_sub_nodes(
	interface: web::Data<Arc<StoreInterface>>,
	path: web::Path<(NodePathBuf,)>,
) -> Result<HttpResponse, ErrorWrapper> {
	let (node_path,) = path.into_inner();

	// TODO: stream this
	let stream = interface.sub_nodes(&*node_path).await?;
	let names = stream.try_collect::<Vec<String>>().await?;
	Ok(HttpResponse::Ok()
		.content_type("application/json")
		.json(&names))
}

async fn infer<R: AsyncRead>(
	mut r: Pin<&mut R>,
) -> Result<(Option<infer::Type>, Bytes), std::io::Error> {
	let mut buf = BytesMut::new();
	buf.resize(8 * 1024, 0);
	let mut i = 0;
	while i < buf.len() {
		let n = r.read(&mut buf[i..]).await?;
		if n == 0 {
			buf.resize(i, 0);
			break;
		}
		i += n;
	}

	let ty = infer::get(&buf);
	Ok((ty, buf.freeze()))
}
